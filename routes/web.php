<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'App\Http\Controllers\HomeController@home')->name('home');

Route::get('loginJLB', 'App\Http\Controllers\Admin\LoginController@showLoginForm')->name('login_admin');
Route::post('loginJLB', 'App\Http\Controllers\Admin\LoginController@login');

Route::get('login_frontend', 'App\Http\Controllers\LoginController@login_frontend')->name('login_frontend');
Route::post('postLogin', 'App\Http\Controllers\LoginController@postLogin')->name('login_frontend.post');

Route::get('tags/{slug}', 'App\Http\Controllers\HomeController@tags')->name('tags');

Route::post('add-to-cart/{id}/{group?}', 'App\Http\Controllers\CartController@addCart')->name('add_to_cart');
Route::get('buy-now/{id}/{group?}', 'App\Http\Controllers\CartController@buyNow')->name('buy_now');

Route::get('car/{slug}', 'App\Http\Controllers\CarController@show')->name('cars.show');

Route::get('get_car/{id}', 'App\Http\Controllers\CarController@get_car')->name('cars.get_car');

Route::get('cart', 'App\Http\Controllers\CartController@index')->name('cart.index');
Route::post('cart/coupon', 'App\Http\Controllers\CartController@addCoupon')->name('cart.coupon');

Route::get('cart/delete/{id}', 'App\Http\Controllers\CartController@remove')->name('cart.items.remove');
Route::post('cart/update', 'App\Http\Controllers\CartController@update')->name('cart.update');

Route::get('checkout', 'App\Http\Controllers\CartController@checkout')->name('checkout');
Route::post('checkout', 'App\Http\Controllers\CartController@checkout')->name('checkout.post')->middleware('checkout.login');
Route::get('thank-you', 'App\Http\Controllers\CartController@thankyou')->name('thankyou');

Route::get('register', 'App\Http\Controllers\LoginController@register')->name('register');
Route::post('register/post', 'App\Http\Controllers\LoginController@postRegister')->name('post.register');

// customer
Route::get('account', 'App\Http\Controllers\AccountController@account')->name('account')->middleware('account');
Route::get('account/detail', 'App\Http\Controllers\AccountController@detail')->name('account.detail')->middleware('account');
Route::post('account/update', 'App\Http\Controllers\AccountController@update')->name('account.update')->middleware('account');
Route::get('account/orders/detail/{id}', 'App\Http\Controllers\AccountController@order_detail')->name('account.orders.detail')->middleware('account');
Route::get('account/orders', 'App\Http\Controllers\AccountController@orders')->name('account.orders')->middleware('account');
Route::get('account/json_detail', 'App\Http\Controllers\AccountController@json_detail')->name('account.json_detail')->middleware('account');
Route::get('account/json_address', 'App\Http\Controllers\AccountController@json_address')->name('account.json_address');
Route::get('account/json_getdistricts/{value}', 'App\Http\Controllers\AccountController@json_getdistricts')->name('account.json_getdistricts');

Route::get('search', 'App\Http\Controllers\HomeController@search')->name('search');

//ajax
Route::get('ajax/getdistrict', 'App\Http\Controllers\AccountController@getdistricts')->name('ajax.getdistricts');

Route::post('logout_frontend', 'App\Http\Controllers\Auth\LoginController@logout')->name('logout_frontend');
if (str_contains(Request::path(), '@dminJLB')) {
    require base_path('routes/admin.php');
}
