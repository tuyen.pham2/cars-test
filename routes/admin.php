<?php

Route::group(['prefix' => '@dminJLB', 'middleware' => ['auth'], 'namespace' => 'App\Http\Controllers\Admin'], function () {
    Route::post('logout', 'App\Http\Controllers\LoginController@logout')->name('logout_admin');

    Route::get('/', function () {
        return view('admin.dashboard');
    });
    // Route::resource('cars', 'App\Http\Controllers\CarController');
    // Route::resource('category', 'App\Http\Controllers\CategoryController');

    Route::get('user_detail/{id}', 'UserController@user_detail')->name('users.user_detail');
    Route::get('users', 'UserController@index')->name('users.index');
    Route::get('users/edit/{id}', 'UserController@edit')->name('users.edit');
    Route::put('users/update/{id}', 'UserController@update')->name('users.update');
    Route::post('users/create', 'UserController@create')->name('users.create');

    Route::get('cars', 'CarController@index')->name('cars.index');
    Route::get('cars/create', 'CarController@create')->name('cars.create');

    Route::get('cars/edit/{id}', 'CarController@edit')->name('cars.edit');
    Route::put('cars/update/{id}', 'CarController@update')->name('cars.update');

    Route::post('cars/store', 'CarController@store')->name('cars.store');

    Route::post('resources/store', 'ResourceController@store')->name('resources.store');
    Route::delete('resources/destroy/{id}', 'ResourceController@destroy')->name('resources.destroy');

    Route::get('tags', 'CategoryController@index')->name('tags.index');
    Route::get('tags/create', 'CategoryController@create')->name('tags.create');
    Route::post('tags/store', 'CategoryController@store')->name('tags.store');
    Route::get('tags/edit/{id}', 'CategoryController@edit')->name('tags.edit');
    Route::put('tags/update/{id}', 'CategoryController@update')->name('tags.update');

    Route::get('attributes', 'AttributeController@index')->name('attributes.index');
    Route::get('attributes/create', 'AttributeController@create')->name('attributes.create');
    Route::post('attributes/store', 'AttributeController@store')->name('attributes.store');
    Route::get('attributes/edit/{id}', 'AttributeController@edit')->name('attributes.edit');
    Route::put('attributes/update/{id}', 'AttributeController@update')->name('attributes.update');
    Route::get('first_attribute_values', 'AttributeValueController@first_attribute_values')->name('attributes.first_attribute_values');
    Route::get('list_attributes', 'AttributeController@list_attributes')->name('attributes.list_attributes');
    Route::get('list_attribute_values', 'AttributeValueController@list_attribute_values')->name('attributes.list_attribute_value');
    Route::get('product_attributes/{id}', 'CarController@product_attributes')->name('cars.product_attributes');

    Route::get('attribute_values/create', 'AttributeValueController@create')->name('attribute.values.create');
    Route::post('attribute_values/store', 'AttributeValueController@store')->name('attribute.values.store');
    Route::get('attribute_values/{id}', 'AttributeValueController@index')->name('attribute.values.index');
    Route::get('attribute_values/edit/{id}', 'AttributeValueController@edit')->name('attribute.values.edit');
    Route::put('attribute_values/update/{id}', 'AttributeValueController@update')->name('attribute.values.update');

    Route::get('order_status', 'OrderStatusController@index')->name('order_status.index');
    Route::get('order_status/create', 'OrderStatusController@create')->name('order_status.create');
    Route::post('order_status/store', 'OrderStatusController@store')->name('order_status.store');
    Route::get('order_status/edit/{id}', 'OrderStatusController@edit')->name('order_status.edit');
    Route::put('order_status/update/{id}', 'OrderStatusController@update')->name('order_status.update');

    Route::get('coupons', 'CouponController@index')->name('coupons.index');
    Route::get('coupons/create', 'CouponController@create')->name('coupons.create');
    Route::post('coupons/store', 'CouponController@store')->name('coupons.store');
    Route::get('coupons/edit/{id}', 'CouponController@edit')->name('coupons.edit');
    Route::put('coupons/update/{id}', 'CouponController@update')->name('coupons.update');

    Route::get('settings', 'SettingController@index')->name('settings.index');
    Route::get('settings/send_mail', 'SettingController@send_mail')->name('settings.send_mail');
    // Route::get('settings/create', 'SettingController@create')->name('settings.create');
    Route::post('settings/store', 'SettingController@store')->name('settings.store');
    // Route::get('settings/edit/{id}', 'SettingController@edit')->name('settings.edit');
    // Route::put('settings/update/{id}', 'SettingController@update')->name('settings.update');

    Route::get('orders', 'OrderController@index')->name('orders.index');
    Route::get('orders/{id}', 'OrderController@edit')->name('orders.edit');
    Route::put('orders/update/{id}', 'OrderController@update')->name('orders.update');

    // Route::get('users/create', 'UserController@create')->name('users.create.get');
    // Route::post('users/create', 'UserController@create')->name('users.create.post');
    // Route::get('users', 'UserController@index')->name('users.index');

    // Route::resource('user', 'UserController');
});
