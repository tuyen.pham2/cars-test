<?php

namespace App;

use DB;
use Cache;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'first_name',
        'last_name',
        'phone',
        'address',
        'password',
        'city',
        'district',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            Cache::forget("user_{$model->id}.userById");
            Cache::forget("user_{$model->id}.userByIdAndToken");
        });

        static::deleted(function ($model) {
            Cache::forget("user_{$model->id}.userById");
            Cache::forget("user_{$model->id}.userByIdAndToken");
        });
    }

    public function getCreatedAtAttribute($date)
    {
        if (! empty($date)) {
            return Carbon::parse($date)->format('d/m/Y');
        }

        return '';
    }

    // public function getNameROle($id){
    //     return DB::table('user_role')->where('id',$id)->first();
    // }

    public function generateToken()
    {
        $this->api_token = str_random(60);
        $this->save();

        return $this->api_token;
    }
}
