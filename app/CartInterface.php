<?php

namespace App;

interface CartInterface
{
    public function add($id, $price, $name, $image, $qty = null, $group_id = null);

    public function content();

    public function get($id);

    public function update($id, int $qty);

    public function subTotal();

    public function total();

    public function destroy();

    public function remove($id);

    public function getContent();

    public function coupons();

    public function addCoupon($code);

    public function updateMulti($id, $qty);
}
