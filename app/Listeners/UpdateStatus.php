<?php

namespace App\Listeners;

use App\Models\Setting;
use App\Events\OrderEvent;

class UpdateStatus
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(OrderEvent $event)
    {
        $status = 2;
        $setting_status = Setting::where('key', 'order_compledted')->select('value')->first();
        if ($setting_status) {
            $status = $setting_status->value;
        }
        $event->order->update(['status' => $status]);
    }
}
