<?php

namespace App\Listeners;

use Carbon\Carbon;
use App\Models\Order;
use App\Events\OrderEvent;

class SaveOrder
{
    public $delay = 10;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\OrderEvent  $event
     * @return void
     */
    public function handle(OrderEvent $event)
    {
        $data = $event->order;

        unset($data['_token']);

        $now = Carbon::now();

        $data['total'] = Cart::total();
        $data['total_price'] = Cart::totalPrice();
        $data['created_at'] = $data['updated_at'] = $now;
        $id = Order::insertGetId($data);

        $items = [];
        foreach (Cart::content() as $item) {
            $data = [
                'order_id' => $id,
                'car_id' => $item['item_id'],
                'unit_price' => $item['price'],
                'qty' => $item['quantity'],
                'group_id' => $item['group'],
                'created_at' => $now,
                'updated_at' => $now,
            ];
            $items[] = $data;
        }

        \DB::table('order_items')->insert($items);

        // return $next($event);
    }
}
