<?php

namespace App\Listeners;

use App\Facades\Cart;
use App\Models\CarGroup;
use App\Events\OrderEvent;

class UpdateQty
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\OrderEvent  $event
     * @return void
     */
    public function handle(OrderEvent $event)
    {
        $cart = Cart::content();

        foreach ($cart as $item) {
            if (! is_null($item['group_id'])) {
                $group = CarGroup::where('id', $item['group_id'])->first();
                if ($group) {
                    $group->qty = $group->qty - $item['qty'];
                    $group->save();
                }
            }
        }
    }
}
