<?php

namespace App;

use Session;
use App\Models\Coupon;
use App\Models\CarGroup;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Collection;
use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Events\Dispatcher;

class CartSession implements CartInterface
{
    /**
     * Instance of the session manager.
     *
     * @var \Illuminate\Session\SessionManager
     */
    private $session;

    /**
     * Instance of the event dispatcher.
     *
     * @var \Illuminate\Contracts\Events\Dispatcher
     */
    private $events;

    /**
     * Holds the current cart instance.
     *
     * @var string
     */
    private $instance;

    private $cart;

    /**
     * Cart constructor.
     *
     * @param \Illuminate\Session\SessionManager      $session
     * @param \Illuminate\Contracts\Events\Dispatcher $events
     */
    public function __construct(SessionManager $session, Dispatcher $events)
    {
        $this->session = $session;
        $this->events = $events;

        $this->cart = $this->getCart();
        $this->instance('default');
    }

    public function instance($instance = null)
    {
        $instance = $instance ?: 'default';

        $this->instance = sprintf('%s.%s', 'cart', $instance);

        return $this;
    }

    public function currentInstance()
    {
        return str_replace('cart.', '', $this->instance);
    }

    /**
     * ADD TO CART
    */
    public function add($id, $price, $name, $image, $qty = null, $group_id = null)
    {
        $cart = $this->getContent();
        if ($qty === null) {
            $qty = 1;
        }

        $group_detail = null;
        if (! is_null($group_id)) {
            $group_detail = CarGroup::where('id', $group_id)->first();
        }
        $idcart = $this->idItem($id, $group_id);

        if (isset($cart['products'])) { // check product exist in cart

            $products = $cart['products'];

            if (isset($products[$idcart])) {
                $product = $products[$idcart];
                $access = $this->checkQty($group_id, $product['qty'] + (int) $qty);

                if ($access == true) {
                    $product['qty'] += (int) $qty;

                    $products[$idcart] = $product;
                    $cart['products'] = $products;

                // $cart->put('products', $products);
                } else {
                    Session::flash('response', ['status' => 'error', 'message' => 'Maximum!']);

                    return back();
                }
            } else {
                $products[$idcart] = [
                    'id' => $idcart,
                    'cartname_id' => $idcart,
                    'item_id' => $id,
                    'price' => $price,
                    'qty' => $qty,
                    'name' => $name,
                    'image' => $image,
                    'group_id' => $group_id,
                    'group_detail' => $group_detail,
                ];
                $cart['products'] = $products;
            }
        } else {
            $access = $this->checkQty($group_id, $qty);
            if ($access == true) {
                $products[$idcart] = [
                    'id' => $idcart,
                    'cartname_id' => $idcart,
                    'item_id' => $id,
                    'price' => $price,
                    'qty' => $qty,
                    'name' => $name,
                    'image' => $image,
                    'group_id' => $group_id,
                    'group_detail' => $group_detail,
                ];

                $cart['products'] = $products;
            } else {
                Session::flash('response', ['status' => 'error', 'message' => 'Maximum!']);

                return back();
            }
        }

        $cart['coupons'] = [];
        Session::put('cart', $cart);

        return $idcart;
    }

    public function getContent()
    {
        $content = $this->session->has('cart')
            ? $this->session->get('cart')
            : new Collection;

        return $content;
    }

    public function content()
    {
        if (isset($this->cart['products'])) {
            return $this->cart['products'];
        }

        return new Collection([]);
    }

    private function getCart()
    {
        if (is_null(Session::get('cart'))) {
            return new Collection([]);
        }

        return $this->session->get('cart');
    }

    public function get($rowId)
    {
        $content = $this->getContent();

        if (! $content->has($rowId)) {
            throw new InvalidRowIDException("The cart does not contain rowId {$rowId}.");
        }

        return $content->get($rowId);
    }

    public function subTotal()
    {
        $price = 0;
        if (! Session::has('cart')) {
            return $price;
        }
        if (isset($this->cart['products'])) {
            foreach ($this->cart['products'] as $id => $value) {
                $price += $value['qty'] * $value['price'];
            }
        }

        return $price;
    }

    public function isEmpty()
    {
        if (! Session::has('cart')) {
            return true;
        }

        if (count(Session::get('cart')) <= 0) {
            return true;
        }

        return false;
    }

    public function destroy()
    {
        Session::pull('cart');
    }

    public function remove($id)
    {
        // $cart = Session::pull('cart', []);
        $cart = $this->cart;

        $cart = $cart->map(function ($value, $index) use ($id) {
            if ($index == 'products') {
                unset($value[$id]);
            }

            return $value;
        });

        session()->put('cart', $cart);
    }

    public function update($id, $qty)
    {
        $cart = $this->getContent();

        if ($cart) {
            if (isset($cart[$id])) {
                $item = $cart[$id];
                $item['qty'] = $qty;
                $cart->put($id, $item);
            }
        }
        // Session::put('cart', $cart);
    }

    public function total()
    {
        $total = 0;

        foreach ($this->content() as $id => $value) {
            $total += $value['qty'];
        }

        return $total;
    }

    public function totalPrice()
    {
        $total = $this->subTotal();
        $total = $this->plusCoupon($total);

        return $total;
    }

    public function put($key, $value)
    {
        $this->offsetSet($key, $value);

        return $this;
    }

    private function idItem($id, $group_id = null)
    {
        $name = 'product_' . $id;

        if (! is_null($group_id)) {
            $name = $name . '.group_' . $group_id;
        }

        return $name;
    }

    private function checkQty($groupid, $qty)
    {
        if (is_null($groupid)) {
            return true;
        }
        $g = CarGroup::where('id', $groupid)->first();
        if ($g) {
            if ($g->qty >= $qty) {
                return true;
            }
        }

        return false;
    }

    public function coupons()
    {
        if (isset($this->cart['coupons'])) {
            return $this->cart['coupons'];
        }

        return new Collection([]);
    }

    public function hasCoupon()
    {
        if (isset($this->cart['coupons']) && count($this->cart['coupons']) > 0) {
            return true;
        }

        return false;
    }

    public function addCoupon($code)
    {
        $coupon = Coupon::where('code', $code)->first();
        if (! $coupon) {
            throw new CouponNotExistException;
        }

        $pipes = [
            // \App\Pipes\HasCoupon::class,
            \App\Pipes\CouponDate::class,
            \App\Pipes\CouponWithItem::class,
            // Check already added (later)
            // Check number usage (later)
            // Check limit for user (later)
        ];

        $response = app(Pipeline::class)
            ->send($coupon)
            ->through($pipes)
            ->then(function ($content) use ($coupon) {
                $cart = $this->cart;

                $arr = [
                    'coupon_code' => $coupon->code,
                    'coupon_value' => $coupon->value,
                    'coupon_is_percent' => $coupon->is_percent,
                    'coupon_id' => $coupon->id,
                ];

                $cart = $cart->map(function ($value, $index) use ($arr) {
                    if ($index == 'coupons') {
                        $value[] = $arr;
                    }

                    return $value;
                });

                Session::put('cart', $cart);

                return true;
            });

        return $response;
    }

    public function updateMulti($id, $qty)
    {
        $products = $this->content();

        if ($products) {
            if (isset($products[$id])) {
                $item = $products[$id];
                $item['qty'] = $qty;
                $products[$id] = $item;
                $cart['products'] = $products;
                Session::put('cart', $cart);
            }
        }
    }

    public function updateCache()
    {
    }

    public function getCoupons()
    {
    }

    private function plusCoupon($price)
    {
        if (! $this->hasCoupon()) {
            return $price;
        }

        foreach ($this->cart['coupons'] as $coupon) {
            if ($coupon['coupon_is_percent'] == 1) {
                $price = $price - (($price * $coupon['coupon_value']) / 100);
            } else {
                $price = $price - $coupon['coupon_value'];
            }
        }

        if ($price <= 0) {
            $price = 0;
        }

        return $price;
    }
}
