<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class OrderItem extends Model
{
    use HasFactory;

    protected $table = 'order_items';

    protected $with = ['car'];

    protected $fillable = [];

    public function car()
    {
        return $this->belongsTo(Car::class);
    }

    public function group()
    {
        return $this->belongsTo(CarGroup::class);
    }
}
