<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    protected $fillable = [
        'cart_id',
        'item_id',
        'cartname_id',
        'session_id',
        'qty',
        'price',
        'name',
        'image',
        'group_id',
        'group_detail',
    ];
}
