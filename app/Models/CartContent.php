<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartContent extends Model
{
    protected $table = 'cart';

    protected $with = ['items', 'coupons'];

    protected $fillable = [
        'session_id',
    ];

    public function items()
    {
        return $this->hasMany(CartItem::class, 'cart_id');
    }

    public function coupons()
    {
        return $this->hasMany(CartCoupon::class, 'cart_id');
    }
}
