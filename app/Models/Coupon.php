<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable = [
        'name',
        'code',
        'value',
        'is_percent',
        'active',
        'start_date',
        'end_date',
    ];

    protected $hidden = [];

    public static function boot()
    {
        parent::boot();
    }
}
