<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartCoupon extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'cart_id',
        'coupon_id',
        'coupon_code',
        'coupon_value',
        'coupon_is_percent',
    ];
}
