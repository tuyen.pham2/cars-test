<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarGroup extends Model
{
    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['attributes'];

    public $timestamps = false;

    protected $table = 'car_groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['car_id', 'price', 'qty'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [];

    public function attributes()
    {
        return $this->hasMany(CarGroupAttribute::class, 'group_id');
    }
}
