<?php

namespace App\Models;

use DB;
use App\Traits\HasCategory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasCategory;

    protected $fillable = [
        'name', 'description', 'slug', 'image', 'model', 'engine_size', 'make', 'registration', 'price', 'active',
    ];

    protected $hidden = [];

    public function image_url()
    {
        return $this->hasOne(Resource::class, 'id', 'image');
    }

    public static function booted()
    {
        parent::boot();

        static::saved(function ($entity) {
            if (! empty(request()->all())) {
                $entity->saveRelations(request()->all());
                $entity->saveGroup(request()->all());

                // $entity->saveAttributes(request()->all());
            }
        });
    }

    public function saveRelations($attributes = [])
    {
        // $this->attrs()->sync($attributes['attributes']);
    }

    public function attrs()
    {
        // return $this->belongsToMany(Attribute::class, 'car_attributes');
        return $this->hasMany(CarAttribute::class);
    }

    public function groups()
    {
        return $this->hasMany(CarGroup::class);
    }

    private function saveAttributes($attributes = [])
    {
        DB::table('car_attributes')->where('car_id', $this->id)->delete();
        foreach ($attributes['attributes'] as $attribute) {
            $car_attr = DB::table('car_attributes')->insertGetId(['car_id' => $this->id, 'attribute_id' => $attribute['attribute_id']]);

            if ($car_attr) {
                $arr = [];
                foreach ($attribute['values'] as $value) {
                    $ar = [
                        'car_attribute_id' => $car_attr,
                        'attribute_value_id' => $value,
                    ];
                    $arr[] = $ar;
                }
                DB::table('car_attribute_values')->insert($arr);
            }
        }
    }

    private function saveGroup($attributes = [])
    {
        CarGroup::where('car_id', $this->id)->delete();
        if ($attributes['groups']) {
            foreach ($attributes['groups'] as $group) {
                $group_id = CarGroup::insertGetId(
                    [
                        'car_id' => $this->id,
                        'price' => $group['price'],
                        'qty' => $group['qty'] ?? 1,
                    ]
                );
                foreach ($group['values'] as $attribute_id => $value_id) {
                    DB::table('group_attributes')->insert([
                        'group_id' => $group_id,
                        'attribute_id' => $attribute_id,
                        'attribute_value_id' => $value_id,
                    ]);
                }
            }
        }
    }
}
