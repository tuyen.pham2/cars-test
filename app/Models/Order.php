<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Order extends Model
{
    use HasFactory;

    protected $with = ['status'];

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'city',
        'district',
        'address',
        'total',
        'total_price',
        'payment_method',
        'notes',
        'user_id',
        'order_status_id',
        'order_id',
        'sub_total',
        'discount',
        'coupon_id',
    ];

    public function items()
    {
        return $this->hasmany(OrderItem::class);
    }

    public function get_status()
    {
        if (isset(OrderStatus::list()[$this->status])) {
            return OrderStatus::list()[$this->status];
        }

        return 'Pending';
    }

    public function status()
    {
        return $this->belongsTo(OrderStatus::class, 'order_status_id');
    }

    public function get_info()
    {
        return [
            'ID' => $this->order_id,
            'First Name' => $this->first_name,
            'Last Name' => $this->last_name,
            'Email' => $this->email,
            'Phone' => $this->phone,
            'Address' => $this->address,
            'City' => $this->city,
            'District' => $this->district,
            'Notes' => $this->notes,
        ];
    }
}
