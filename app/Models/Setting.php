<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
        'key',
        'value',
    ];

    protected $hidden = [];

    public static function boot()
    {
        parent::boot();
    }

    public function settings_save($attributes = [])
    {
        foreach ($attributes['values'] as $key => $value) {
            $setting = Setting::firstOrNew(['key' => $key]);
            $setting->value = $value;
            $setting->save();
        }
    }

    public function get_values()
    {
        $arr = [];
        $settings = $this->all();
        if ($settings) {
            foreach ($settings as $setting) {
                $arr[$setting->key] = $setting->value;
            }
        }

        return $arr;
    }
}
