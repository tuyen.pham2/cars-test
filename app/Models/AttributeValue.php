<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttributeValue extends Model
{
    protected $fillable = [
        'name', 'attribute_id',
    ];

    protected $hidden = [];

    public static function boot()
    {
        parent::boot();
    }

    public function values()
    {
        return $this->belongsTo(Attribute::class);
    }
}
