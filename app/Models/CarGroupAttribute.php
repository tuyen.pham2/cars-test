<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarGroupAttribute extends Model
{
    protected $table = 'group_attributes';

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['attribute', 'attribute_value'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['group_id', 'attribute_id'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['value'];

    public function attribute()
    {
        return $this->belongsTo(Attribute::class);
    }

    public function attribute_value()
    {
        return $this->belongsTo(AttributeValue::class, 'attribute_value_id');
    }

    public function getNameAttribute()
    {
        return $this->attribute->name;
    }

    public function getAttributeSetAttribute()
    {
        return $this->attribute->attributeSet->name;
    }

    public function getIdAttribute()
    {
        return $this->attribute_value->id;
    }

    public function getValueAttribute()
    {
        return $this->attribute_value->value;
    }
}
