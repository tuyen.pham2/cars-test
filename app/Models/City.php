<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function list()
    {
        $cities = [
            1 => 'Hồ Chí Minh',
            2 => 'Hà Nội',
            3 => 'Hải Phòng',
            4 => 'Đà Nẵng',
            5 => 'Bình Thuận',
        ];

        return $cities;
    }
}
