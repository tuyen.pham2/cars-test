<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    public function list()
    {
        $districts = [
            'Hồ Chí Minh' => [
                'Quận 1',
                'Quận 2',
                'Quận 3',
                'Quận 4',
                'Quận 5',
                'Quận 6',
                'Quận 7',
            ],
            'Hà Nội' => [
                'Hà Nội 1',
                'Hà Nội 2',
                'Hà Nội 3',
                'Hà Nội 4',
            ],
            'Hải Phòng' => [

            ],
            'Đà Nẵng' => [

            ],

        ];

        return $districts;
    }
}
