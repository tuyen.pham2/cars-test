<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Category extends Model
{

    protected $fillable = ['name', 'slug', 'description', 'resource_id'];

    protected $multilingual = [];

    protected $hidden = [];

    public function cars() {
    	return $this->belongsToMany('App\Models\Car')->orderBy('cars.id', 'desc');
    }

    public function getParent()
    {
        return $this->belongsTo('App\Models\Category', 'parent_id');
    }

    public function getParentsTitle() {
        if($this->parent) {
            return $this->parent->getParentsTitle();
        } else {
            return $this->title;
        }
    }

    public function getParentId(){
        return $this->parent_id;
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Category', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Category', 'parent_id');
    }

}
