<?php

namespace App\Providers;

use App\Gateway\COD;
use App\Models\Setting;
use App\Facades\Gateway;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton(
            \App\Repositories\Car\CarRepositoryInterface::class,
            \App\Repositories\Car\CarRepository::class,
        );
        $this->app->singleton(
            \App\Repositories\Category\CategoryRepositoryInterface::class,
            \App\Repositories\Category\CategoryRepository::class
        );
        $this->app->singleton(
            \App\Repositories\Attribute\AttributeRepositoryInterface::class,
            \App\Repositories\Attribute\AttributeRepository::class
        );

        $this->app->singleton(
            \App\Repositories\AttributeValue\AttributeValueRepositoryInterface::class,
            \App\Repositories\AttributeValue\AttributeValueRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Order\OrderRepositoryInterface::class,
            \App\Repositories\Order\OrderRepository::class
        );

        $this->app->singleton(
            \App\Repositories\User\UserRepositoryInterface::class,
            \App\Repositories\User\UserRepository::class
        );

        $this->app->singleton(
            \App\Repositories\OrderStatus\OrderStatusRepositoryInterface::class,
            \App\Repositories\OrderStatus\OrderStatusRepository::class
        );
        $this->app->singleton(
            \App\Repositories\Setting\SettingRepositoryInterface::class,
            \App\Repositories\Setting\SettingRepository::class
        );
        $this->app->singleton(
            \App\Repositories\Coupon\CouponRepositoryInterface::class,
            \App\Repositories\Coupon\CouponRepository::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerCOD();
        $this->setEmail();
    }

    private function registerCOD()
    {
        Gateway::register('cod', new COD);
    }

    private function setEmail()
    {
        $settings = new Setting();

        $arr = $settings->get_values();

        // app('config')->set('mail.default', 'smtp');
        // app('config')->set('mail.from.address', $arr['mail_from_address']);
        // app('config')->set('mail.from.name', $arr['mail_from_name']);
        app('config')->set('mail.mailers.smtp.host', $arr['email_host']);
        app('config')->set('mail.mailers.smtp.port', $arr['email_port']);
        app('config')->set('mail.mailers.smtp.username', $arr['email_username']);
        app('config')->set('mail.mailers.smtp.password', $arr['email_password']);
        // app('config')->set('mail.mailers.smtp.encryption', $arr['mail_encryption']);
    }
}
