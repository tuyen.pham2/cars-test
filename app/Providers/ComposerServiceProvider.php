<?php

namespace App\Providers;

use File;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['layouts.app', 'site.*', 'checkout'], function ($view) {
            $currentUser = auth()->guard('web')->user();
            if ($currentUser != null) {
                $first_name = '';
                $last_name = '';
                $parts = explode(' ', $currentUser->name);
                $last = array_pop($parts);
                $parts = [implode(' ', $parts), $last];
                if (count($parts) > 0) {
                    $first_name = isset($parts[1]) ? $parts[1] : '';
                    $last_name = isset($parts[0]) ? $parts[0] : '';
                }
                $currentUser->first_name = $first_name;
                $currentUser->last_name = $last_name;
            }
            $data['currentUser'] = auth()->guard('web')->user();
            if (! \App::runningInConsole()) {
                $data['bodyClass'] = \Route::currentRouteName() !== null
                    ? \Route::currentRouteName()
                    : strtolower(str_replace('Controller@', '-', array_last(explode('\\', \Route::currentRouteAction()))));
            }
            // $data['cart'] = $this->getCart();
            $data['menu'] = $this->getMenu();
            $view->with($data);
        });

        View::composer('admin.page.form', function ($view) {
            $data['pages'] = Page::orderBy('id', 'desc')->get()->pluck('title', 'id')->all();
            $data['templates'] = array_reduce(File::files(resource_path('views/site/page')), function ($carry, $item) {
                $name = str_replace('.blade.php', '', basename($item));
                $carry[$name] = ucfirst($name);

                return $carry;
            });
            $view->with($data);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    private function getMenu()
    {
        return \App\Models\Category::all();
    }
}
