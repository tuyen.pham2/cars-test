<?php

namespace App\Exceptions;

use Exception;

class CouponNotExistException extends Exception
{
    /**
     * Render the exception into an HTTP response.
     *
     * @return \Illuminate\Http\Response
     */
    public function render()
    {
        // return response()->json([
        //     'message' => trans('coupon::messages.not_exists'),
        // ], 404);
        return back()->withErrors(['msg' => 'Coupon Not Exist!']);
    }
}
