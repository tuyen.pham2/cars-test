<?php

namespace App\Http\Controllers;

use App\searchEs;
use App\Models\Car;
use App\Models\Category;
use Illuminate\Http\Request;

use Illuminate\Routing\Controller as BaseController;

class HomeController extends BaseController
{
    public function home()
    {
        $cars = Car::with('image_url')->where('active', 1)->get();
        $tags = Category::all();

        return view('home', compact('cars', 'tags'));
    }

    public function search(Request $request)
    {
        $cars = [];
        if ($request->has('keyword') && $request->keyword != '') {
            $es = new searchEs('cars', ['name', 'engine_size'], $request->keyword);

            $results = $es->search();

            $keyword = $request->keyword;

            $cars = $results['hits']['hits'];
        }

        return view('search', compact('cars', 'keyword'));
    }

    public function tags($slug)
    {
        $tag = Category::where('slug', $slug)->firstOrFail();

        $cars = $tag->cars()->where('active', 1)->get();

        return view('tags', compact('tag', 'cars'));
    }
}
