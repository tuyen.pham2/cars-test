<?php

namespace App\Http\Controllers\Admin;

use DB;

use Auth;

use Session;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\User\UserRepositoryInterface;

class UserController extends Controller
{
    protected $entityRepo;

    private $path = 'admin.user';

    public function __construct(UserRepositoryInterface $entityRepo)
    {
        $this->entityRepo = $entityRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entities = $this->entityRepo->getAll();

        return view($this->path . '.index', compact('entities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $user = new User();
        // $roles = DB::table('user_role')->groupBy('role')->get();

        // return view('admin.user.create', compact('user', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request, [
        //     'name' => 'required|max:255',
        //     'email' => 'required|email|max:255|unique:users',
        //     'password' => 'required|min:6|confirmed',
        //     // 'level' => 'required|in:editor,admin'
        // ]);

        // $request->merge(['password' => bcrypt($request->password)]);

        // $new = User::create($request->all());
        // Session::flash('response', ['status' => 'success', 'message' => trans('admin.message.create')]);

        // return redirect()->action('Admin\UserController@edit', $new->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $entity = $this->entityRepo->edit($id);

        // $this->authorize('edit-user', $user);
        // $roles = DB::table('user_role')->groupBy('role')->get();

        return view($this->path . '.edit', compact('entity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email,' . $id,
            // 'password' => 'min:6|confirmed',
            // 'level' => 'in:editor,admin'
        ]);

        $user = User::findOrFail($id);
        // $this->authorize('edit-user', $user);

        $data = $request->only(['first_name', 'last_name', 'email', 'phone', 'address', 'city', 'district']);
        $data['name'] = $request->first_name . ' ' . $request->last_name;
        if ($request->has('password')) {
            // $user->password = bcrypt($request->password);
            $data['password'] = bcrypt($request->password);
        }
        // if (Auth::user()->can('manage-user', User::class)) {
        //     $user->level = $request->level;
        // }
        $user->update($data);
        // $user->save();

        Session::flash('response', ['status' => 'success', 'message' => trans('admin.message.update')]);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = User::findOrFail($id);
        $this->authorize('delete-user', $model);
        $model->delete();
        Session::flash('response', ['status' => 'success', 'message' => trans('admin.message.delete')]);

        return redirect()->action('Admin\UserController@index');
    }

    public function permissionCustom()
    {
        $this->checkPermission('permission-view');
        $roles = DB::table('user_role')->get();

        return view('admin.user.permission', compact('roles'));
    }

    public function permissionCreate()
    {
        $this->checkPermission('permission-create');
        include '../permission.php';

        return view('admin.user.permission-create', compact('permission'));
    }

    public function storePermission(Request $request)
    {
        $this->checkPermission('permission-create');
        if ($request->permission) {
            $serial = serialize($request->permission);
        } else {
            $serial = null;
        }

        DB::table('user_role')->insert(
            ['role' => $request->role, 'permission' => $serial]
        );

        return redirect()->action('Admin\UserController@permissionCustom');
    }

    public function permissionEdit(Request $request, $id)
    {
        $this->checkPermission('permission-edit');

        $role = DB::table('user_role')->where('id', $id)->first();
        include '../permission.php';
        $perOfRole = [];
        if ($role->permission != null || $role->permission != '') {
            $perOfRole = unserialize($role->permission);
        }

        return view('admin.user.permission-edit', compact('role', 'permission', 'perOfRole'));
    }

    public function updatePermission(Request $request, $id)
    {
        $this->checkPermission('permission-edit');
        // var_dump($request->all());die;
        if ($request->permission) {
            $permission = serialize($request->permission);
        } else {
            $permission = null;
        }

        $role = DB::table('user_role')->where('id', $id)->update(['role' => $request->role, 'permission' => $permission]);

        return redirect()->action('Admin\UserController@permissionEdit', $id);
    }

    public function permissionDestroyer(Request $request)
    {
        // var_dump($request->all());die;
        $this->checkPermission('permission-delete');
        $role = DB::table('user_role')->where('id', $request->id)->delete();

        Session::flash('response', ['status' => 'success', 'message' => trans('admin.message.delete')]);

        return redirect()->action('Admin\UserController@permissionCustom');
    }

    public function user_detail($id)
    {
        $cities = \App\Models\City::list();

        $arr['profile'] = User::where('id', $id)->first();
        $arr['cities'] = $cities;

        // dd($arr['cities']);
        // $districts = $ds[array_shift(array_values($arr['profile']['city']))];
        $districts = \App\Models\District::list()[array_values($cities)[0]];

        if (! is_null($arr['profile']['city'])) {
            $districts = \App\Models\District::list()[$arr['profile']['city']];
        }
        $arr['districts'] = $districts;

        return $arr;
    }
}
