<?php

namespace App\Http\Controllers\Admin;

use Session;
use Illuminate\Http\Request;
use App\Models\AttributeValue;
use App\Http\Controllers\Controller;
use App\Repositories\Attribute\AttributeRepositoryInterface;

class AttributeController extends Controller
{
    /**
     * @var PostRepositoryInterface|\App\Repositories\Repository
     */
    protected $entityRepo;

    private $path = 'admin.attribute';

    public function __construct(AttributeRepositoryInterface $entityRepo)
    {
        $this->entityRepo = $entityRepo;
    }

    public function index()
    {
        $entities = $this->entityRepo->getAll();

        return view($this->path . '.index', ['entities' => $entities]);
    }

    public function create()
    {
        $entity = $this->entityRepo->create();

        return view($this->path . '.create', compact('entity'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $new = $this->entityRepo->store($data);

        if ($request->has('attribute_values')) {
            $this->update_values($data['attribute_values'], $new->id);
        }

        Session::flash('response', ['status' => 'success', 'message' => 'Success!!']);

        return redirect()->route('attributes.edit', $new->id);
    }

    public function edit($id)
    {
        $entity = $this->entityRepo->edit($id);

        return view($this->path . '.edit', ['entity' => $entity]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        if ($request->has('attribute_values')) {
            $this->update_values($data['attribute_values'], $id);
        }

        $entity = $this->entityRepo->update($id, $data);

        Session::flash('response', ['status' => 'success', 'message' => 'Update success!']);

        return back();
    }

    public function destroy($id)
    {
        $this->entityRepo->delete($id);

        return view($this->path . '.index');
    }

    private function update_values($attr_values, $attribute_id)
    {
        AttributeValue::where('attribute_id', $attribute_id)->delete();
        $arr = [];
        foreach ($attr_values as $value) {
            $arr[] = [
                'name' => $value,
                'attribute_id' => $attribute_id,
            ];
        }
        AttributeValue::insert($arr);
    }

    public function list_attributes()
    {
        $attributes = $this->entityRepo->getAll()->toArray();
        $return = [];
        foreach ($attributes as $attribute) {
            $ar = $attribute;
            $attr_values = AttributeValue::where('attribute_id', $attribute['id'])->get();
            $ar['values'] = $attr_values->toArray();
            $return[$attribute['id']] = $ar;
        }

        return $return;
    }
}
