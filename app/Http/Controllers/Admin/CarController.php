<?php

namespace App\Http\Controllers\Admin;

use File;
use Image;
use Session;
use App\Models\Car;
use App\Models\Category;
use App\Models\Resource;
use App\Models\Attribute;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Car\CarRepositoryInterface;

class CarController extends Controller
{
    /**
     * @var PostRepositoryInterface|\App\Repositories\Repository
     */
    protected $carRepo;

    public function __construct(CarRepositoryInterface $carRepo)
    {
        $this->carRepo = $carRepo;
    }

    public function index()
    {
        $cars = $this->carRepo->getAll();

        return view('admin.car.index', ['cars' => $cars]);
    }

    public function create()
    {
        $car = $this->carRepo->create();
        $tags = Category::all();

        return view('admin.car.create', compact('car', 'tags'));
    }

    public function store(Request $request)
    {
        $data = $request->all();

        if ($request->hasFile('upload')) {
            $data['image'] = $this->image_upload($request);
        }

        $new = $this->carRepo->store($data);

        // es
        $arr = $new->toArray();
        $params = [
            'index' => 'car',
            'id' => $arr['id'],
            'body' => $arr,
        ];
        $client = es_connect();
        $response = $client->index($params);
        // end es

        Session::flash('response', ['status' => 'success', 'message' => 'Success!!']);

        return redirect()->route('cars.edit', $new->id);
    }

    public function edit($id)
    {
        $car = $this->carRepo->edit($id);
        $tags = Category::all();
        $attributes = $this->get_attributes();

        return view('admin.car.edit', ['car' => $car, 'tags' => $tags, 'attributes' => $attributes]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        // dd($data);
        if ($request->hasFile('upload')) {
            $data['image'] = $this->image_upload($request);
        }

        $car = $this->carRepo->update($id, $data);

        Session::flash('response', ['status' => 'success', 'message' => 'Update success!']);

        return back();
    }

    public function destroy($id)
    {
        $this->productRepo->delete($id);

        return view('admin.cars.index');
    }

    public function image_upload($request)
    {
        $type = 'images';
        $file = $request->upload;

        $newfilename = $this->rename($file->getClientOriginalName());
        $dir_path = public_path('uploads/car');
        $image_path = public_path("uploads/car/$newfilename");

        $image = Image::make($file);

        if (! File::exists($dir_path)) {
            File::makeDirectory($dir_path, 0777, true, true);
        }

        $file->move($dir_path, $newfilename);

        $resource = Resource::create([
            'type' => $type,
            'name' => $newfilename,
        ]);

        return $resource->id;
    }

    private function rename($file)
    {
        $name = pathinfo($file, PATHINFO_FILENAME);
        $ext = pathinfo($file, PATHINFO_EXTENSION);

        return bin2hex(openssl_random_pseudo_bytes(7)) . '-' . strtolower(preg_replace('/\W/', '', $name) . '.' . $ext);
    }

    private function get_attributes()
    {
        return Attribute::all();
    }

    public function product_attributes($id)
    {
        $product = Car::where('id', $id)->first();
        $groups = $product->groups()->with('attributes')->get();

        $groups_return = [];
        foreach ($groups as $index => $group) {
            $ar = [
                'price' => $group->price,
                'qty' => $group->qty,
            ];
            foreach ($group->attributes as $attribute) {
                $ar['values'][] = [
                    'attribute_id' => $attribute->attribute_id,
                    'value_id' => $attribute->attribute_value_id,
                ];
                $ar['attrs'][] = 1;
            }

            $groups_return[] = $ar;
        }

        return $groups_return;
    }
}
