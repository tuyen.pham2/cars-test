<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Resource;
use File;
use Illuminate\Http\Request;
use Image;
use Validator;

class ResourceController extends Controller
{
    public function store(Request $request) {
        $is_cke = $request->has('CKEditor');

        $validator = Validator::make($request->all(), [
            'type'   => (!$is_cke ? 'required|' : '') . 'alpha',
            'resize' => 'boolean',
            'upload' => 'required|image',
        ]);

        if ($validator->fails()) return 123;

    	$type = $request->has('type') ? strtolower(trim($request->type)) : 'images';
    	$file = $request->file('upload');
        $newfilename = $this->rename($file->getClientOriginalName());
        $dir_path = public_path("uploads/$type");
        $image_path = public_path("uploads/$type/$newfilename");

        $image = Image::make($file);

        if (!File::exists($dir_path)) File::makeDirectory($dir_path, 0777, true, true);

        if ($request->has('resize') and $image->width() > config('image.max_size')) {
            $image->resize(config('image.max_size'), null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $image->save($image_path);
        } else {
            $file->move($dir_path, $newfilename);
        }

        $resource = Resource::create([
			'type' => $type,
			'name' => $newfilename,
        ]);

        if ($is_cke) return '<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction("' . $request->CKEditorFuncNum . '", "' . $resource->large . '", "");</script>';

        return response()->json([
			'id'   => $resource->id,
            'square' => $resource->square,
            /*'thumbnail' => $resource->thumbnail,
            'large' => $resource->large,*/
            'full' => $resource->full,
        ]);
    }

    public function destroy($id) {
        $resource = Resource::where('id',$id)->first();
        if(!$resource)
            return response()->json(['status' => 'success', 'message' => trans('admin.message.delete')]);
        $image_path = public_path("uploads/$resource->type/$resource->name");
        File::delete($image_path);
        $resource->delete();
        return response()->json(['status' => 'success', 'message' => trans('admin.message.delete')]);
    }

    private function rename($file) {
        $name = pathinfo($file, PATHINFO_FILENAME);
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        return bin2hex(openssl_random_pseudo_bytes(7)) . '-' . strtolower(preg_replace('/\W/', '', $name) . '.' . $ext);
    }
}
