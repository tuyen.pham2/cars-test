<?php

namespace App\Http\Controllers\Admin;

use Mail;
use Session;
use App\Mail\SendHog;
use App\Models\Setting;
use App\Models\OrderStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Setting\SettingRepositoryInterface;

class SettingController extends Controller
{
    /**
     * @var PostRepositoryInterface|\App\Repositories\Repository
     */
    protected $entityRepo;

    private $path = 'admin.settings';

    public function __construct(SettingRepositoryInterface $entityRepo)
    {
        $this->entityRepo = $entityRepo;
    }

    /**
     * Only use inddex and store here
    */
    public function index()
    {
        $order_statuses = OrderStatus::all();

        $arr = Setting::all();
        $settings = [];

        if ($arr) {
            $arr = $arr->toArray();

            foreach ($arr as $setting) {
                $settings[$setting['key']] = $setting['value'];
            }
        }

        return view($this->path . '.edit', compact('settings', 'order_statuses'));
    }

    public function create()
    {
        return abort(404);
        // $entity = $this->entityRepo->create();

        // return view($this->path . '.create', compact('entity'));
    }

    public function store(Request $request)
    {
        $data = $request->all();

        // Setting Save
        $settings = new Setting();
        $settings->settings_save($request->all());

        Session::flash('response', ['status' => 'success', 'message' => 'Success!!']);

        return redirect()->route('settings.index');
    }

    public function edit($id)
    {
        return abort(404);
        // $entity = $this->entityRepo->edit($id);

        // return view($this->path . '.edit', ['entity' => $entity]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        $entity = $this->entityRepo->update($id, $data);

        Session::flash('response', ['status' => 'success', 'message' => 'Update success!']);

        return back();
    }

    public function destroy($id)
    {
        $this->entityRepo->delete($id);

        return view($this->path . '.index');
    }

    public function send_mail()
    {
        try {

            // Mail::to('test@gmail.com')->send(new SendHog([]));

            $data = ['content' => 'Here goes description consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco'];
            $mail = new SendHog($data);
            $mail->sendMail('test@gmail.com', null, null, 'test');
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return 'Send mail success!';
    }
}
