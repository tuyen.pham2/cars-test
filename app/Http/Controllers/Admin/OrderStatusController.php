<?php

namespace App\Http\Controllers\Admin;

use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\OrderStatus\OrderStatusRepositoryInterface;

class OrderStatusController extends Controller
{
    /**
     * @var PostRepositoryInterface|\App\Repositories\Repository
     */
    protected $entityRepo;

    private $path = 'admin.order_status';

    public function __construct(OrderStatusRepositoryInterface $entityRepo)
    {
        $this->entityRepo = $entityRepo;
    }

    public function index()
    {
        $entities = $this->entityRepo->getAll();

        return view($this->path . '.index', ['entities' => $entities]);
    }

    public function create()
    {
        $entity = $this->entityRepo->create();

        return view($this->path . '.create', compact('entity'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $new = $this->entityRepo->store($data);

        Session::flash('response', ['status' => 'success', 'message' => 'Success!!']);

        return redirect()->route('order_status.edit', $new->id);
    }

    public function edit($id)
    {
        $entity = $this->entityRepo->edit($id);

        return view($this->path . '.edit', ['entity' => $entity]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        $entity = $this->entityRepo->update($id, $data);

        Session::flash('response', ['status' => 'success', 'message' => 'Update success!']);

        return back();
    }

    public function destroy($id)
    {
        $this->entityRepo->delete($id);

        return view($this->path . '.index');
    }
}
