<?php

namespace App\Http\Controllers\Admin;

use Session;
use App\Models\OrderStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Order\OrderRepositoryInterface;

class OrderController extends Controller
{
    /**
     * @var PostRepositoryInterface|\App\Repositories\Repository
     */
    protected $entityRepo;

    private $path = 'admin.order';

    public function __construct(OrderRepositoryInterface $entityRepo)
    {
        $this->entityRepo = $entityRepo;
    }

    public function index()
    {
        $entities = $this->entityRepo->getAll();

        return view($this->path . '.index', ['entities' => $entities]);
    }

    public function edit($id)
    {
        $entity = $this->entityRepo->edit($id);

        $order_statuses = OrderStatus::all();

        return view($this->path . '.edit', ['entity' => $entity, 'order_statuses' => $order_statuses]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->only('order_status_id');

        $entity = $this->entityRepo->update($id, $data);
        $order_statuses = OrderStatus::all();

        Session::flash('response', ['status' => 'success', 'message' => 'Success!!']);

        return view($this->path . '.edit', ['entity' => $entity, 'order_statuses' => $order_statuses]);
    }

    // public function store(Request $request, $id)
    // {
    //     $entity = $this->entityRepo->edit($id);

    //     return view($this->path . '.edit', ['entity' => $entity]);
    // }

    public function destroy($id)
    {
        $this->entityRepo->delete($id);

        return view($this->path . '.index');
    }
}
