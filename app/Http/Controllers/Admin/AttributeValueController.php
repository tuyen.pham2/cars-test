<?php

namespace App\Http\Controllers\Admin;

use Session;
use Illuminate\Http\Request;
use App\Models\AttributeValue;
use App\Http\Controllers\Controller;

// use App\Repositories\AttributeValue\AttributeValueRepositoryInterface;

class AttributeValueController extends Controller
{
    protected $entityRepo;

    private $path = 'admin.attributevalue';

    // public function __construct(AttributeValueRepositoryInterface $entityRepo)
    // {
    //     $this->entityRepo = $entityRepo;
    // }

    public function index($id)
    {
        $entities = AttributeValue::where('attribute_id', $id)->get();

        return $entities->toArray();

        // return view($this->path . '.index', ['entities' => $entities]);
    }

    public function create()
    {
        $entity = $this->entityRepo->create();

        return view($this->path . '.create', compact('entity'));
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $new = $this->entityRepo->store($data);

        Session::flash('response', ['status' => 'success', 'message' => 'Success!!']);

        return redirect()->route('attributes.edit', $new->id);
    }

    public function edit($id)
    {
        $entity = $this->entityRepo->edit($id);

        return view($this->path . '.edit', ['entity' => $entity]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        if ($request->hasFile('upload')) {
            $data['image'] = $this->image_upload($request);
        }

        $entity = $this->entityRepo->update($id, $data);

        Session::flash('response', ['status' => 'success', 'message' => 'Update success!']);

        return back();
    }

    public function destroy($id)
    {
        $this->entityRepo->delete($id);

        return view($this->path . '.index');
    }

    public function first_attribute_values()
    {
        $firstAttribute = \App\Models\Attribute::orderBy('id', 'ASC')->first();
        $attr_values = \App\Models\AttributeValue::where('attribute_id', $firstAttribute->id)->get();

        return $attr_values->toArray();
    }

    public function list_attribute_values()
    {
        $values = \App\Models\AttributeValue::all()->toArray();
        $arr = [];
        foreach ($values as $v) {
            $arr[$v['attribute_id']][] = $v;
        }

        return $arr;
    }
}
