<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

// use App\Http\Requests\SaveCategory;
use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Repositories\Category\CategoryRepositoryInterface;
use Session;

class CategoryController extends Controller
{

    protected $categoryRepo;

    public function __construct(CategoryRepositoryInterface $categoryRepo)
    {
        $this->categoryRepo = $categoryRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tags = $this->categoryRepo->getAll();

        return view('admin.tags.index', compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $tag = $this->categoryRepo->create();
        return view('admin.tags.create', compact('tag'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datas = $request->all();
        $datas['description'] = '';

        $new = $this->categoryRepo->store($datas);

        Session::flash('response', ['status' => 'success', 'message' => 'Success!!']);

        return redirect()->route('tags.edit', $new->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $tag = $this->categoryRepo->edit($id);

        $cates = Category::where('id','<>',$id)->orderBy('name', 'asc')->get()->pluck('name', 'id'); 
        $cates = $cates->toArray();

        return view('admin.tags.edit', compact('tag', 'cates'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SaveCategory $request, $id)
    {
    
        $data = $request->all();
        $category = $this->categoryRepo->update($id,$data);
        Session::flash('response', ['status' => 'success', 'message' => 'Update Success!']);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->categoryRepo->delete($id);
        return back();
    }
}
