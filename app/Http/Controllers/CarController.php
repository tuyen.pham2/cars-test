<?php

namespace App\Http\Controllers;

use App\Models\Car;

class CarController extends Controller
{
    public function show($slug)
    {
        $car = Car::where('slug', $slug)->firstOrFail();

        $cats = $car->categories->toArray();

        $arr = [];
        if (count($cats) > 0) {
            $arr = array_map(function ($cat) {
                return $cat['id'];
            }, $cats);
        }

        $query = Car::with('image_url')->where('id', '<>', $car->id)->where('active', 1);

        if (count($arr) > 0) {
            $query->whereHas('categories', function ($q) use ($arr) {
                $q->whereIn('categories.id', $arr);
            });
        }

        $related = $query->limit(3)->get();

        return view('cars.show', compact('car', 'related'));
    }

    public function get_car($id)
    {
        $car = Car::with('groups')->where('id', $id)->first();

        if ($car) {
            return $car->toArray();
        }

        return '';
    }
}
