<?php

namespace App\Http\Controllers;

use Mail;
use Session;
use Carbon\Carbon;
use App\Models\Car;
use App\Facades\Cart;
use App\Mail\SendHog;
use App\Models\Order;
use App\Models\Setting;
use App\Facades\Gateway;
use App\Models\CarGroup;
use App\Events\OrderEvent;

use Illuminate\Http\Request;

class CartController extends Controller
{
    public function addCart(Request $request, $id, $group = null)
    {
        $qty = 1;
        if ($request->has('qty')) {
            $qty = $request->qty;
        }
        $car = Car::where('id', $id)->first();
        if ($car) {
            $image = '/images/no-image.png';
            if ($car->image_url) {
                $image = '/uploads/car/' . $car->image_url->name;
            }

            if (! is_null($group)) {
                $g = CarGroup::where('id', $group)->first();
                $price = $g->price;
                if (is_null($g->price)) {
                    $price = $car->price;
                }

                if ($g) {
                    Cart::add($car->id, $price, $car->name, $image, $qty, $g->id);
                } else {
                    Cart::add($car->id, $price, $car->name, $image, $qty);
                }
            } else {
                Cart::add($car->id, $car->price, $car->name, $image, $qty);
            }
        }

        if ($request->submit == 'quick') {
            return redirect()->route('cart.index')->with('success', 'Add to cart');
        }

        return redirect()->back()->with('success', 'Add to cart');
    }

    public function index()
    {
        $cart = Cart::content();

        return view('cart', compact('cart'));
    }

    public function remove($id)
    {
        $return = Cart::remove($id);

        return redirect()->back()->with('success', $return);
    }

    public function update(Request $request)
    {
        if ($request->has('product')) {
            foreach ($request->product as $id => $qty) {
                Cart::updateMulti($id, $qty);
            }
            Cart::updateCache();
        }

        // return redirect()->back()->with('errors', 'Error!');

        return redirect()->back()->with('success', 'Cart Updated!');
    }

    public function checkout(Request $request)
    {
        if (Cart::isEmpty()) {
            return redirect()->route('home');
        }

        if ($request->isMethod('post')) {
            $data = $request->all();

            //Test EMAIL
            // Mail::to('test@gmail.com')->send(new SendHog([]));

            unset($data['_token']);

            $now = Carbon::now();

            $data['total'] = $data['sub_total'] = Cart::total();
            $data['total_price'] = Cart::totalPrice();
            $data['created_at'] = $data['updated_at'] = $now;
            $data['order_id'] = 'order_' . $data['phone'] . Carbon::now()->format('Ymd');
            $user = auth()->user();
            $data['user_id'] = $user->id;

            if (Cart::hasCoupon()) {
                $coupon = Cart::coupons()[0];

                $data['coupon_id'] = $coupon['coupon_id'];
                $data['sub_total'] = Cart::subTotal();
                $data['discount'] = $coupon['coupon_value'];
            }

            $status = 1;
            $setting_order = Setting::select('value')->where('key', 'order_processing')->first();
            if ($setting_order) {
                $status = $setting_order->value;
            }
            $data['order_status_id'] = $status;

            //save Order
            $order = Order::create($data);

            $items = [];
            foreach (Cart::content() as $item) {
                $data = [
                    'order_id' => $order->id,
                    'car_id' => $item['item_id'],
                    'unit_price' => $item['price'] ?? 0,
                    'qty' => $item['quantity'] ?? $item['qty'],
                    'group_id' => $item['group_id'] ?? null,
                    'created_at' => $now,
                    'updated_at' => $now,
                ];
                $items[] = $data;
            }

            \DB::table('order_items')->insert($items);

            // checkout payment
            $gateway = Gateway::get($request->payment_method);

            try {
                $response = $gateway->purchase($order, $request);
            } catch (Exception $e) {
                Order::where('id', $order->id)->delete();

                Session::flash('response', ['status' => 'errors', 'message' => $e->getMessage()]);

                return back();
            }

            if ($response->isSuccess()) {
                event(new OrderEvent($order));

                Cart::destroy();

                return redirect()->route('thankyou');
            }
        }

        $cart = Session::get('cart');

        $cities = \App\Models\City::list();

        $user = auth()->user();

        $districts = \App\Models\District::list()[array_values($cities)[0]];
        if ($user) {
            if ($user->city) {
                $districts = \App\Models\District::list()[$user->city];
            }
        }
        $gates = Gateway::all();

        return view('checkout', compact('cart', 'cities', 'districts', 'gates'));
    }

    public function thankyou()
    {
        return view('thankyou');
    }

    public function buyNow($id, $group = null)
    {
        $car = Car::where('id', $id)->first();
        if ($car) {
            $image = '/images/no-image.png';
            if ($car->image_url) {
                $image = $car->image_url->name;
            }

            if (! is_null($group)) {
                $g = CarGroup::where('id', $group)->first();
                if ($g) {
                    Cart::add($car->id, $g->price, $car->name, $image, 1, $g->id);
                } else {
                    Cart::add($car->id, $car->price, $car->name, $image, 1);
                }
            } else {
                Cart::add($car->id, $car->price, $car->name, $image, 1);
            }
        }

        return redirect()->route('cart.index')->with('success', 'Add to cart');
    }

    public function addCoupon(Request $request)
    {
        $response = false;
        if ($request->has('code')) {
            $response = Cart::addCoupon($request->code);
        }
        if ($response == true) {
            return back()->withSuccess('Success!');
        }

        return back()->withErrors($response);
    }
}
