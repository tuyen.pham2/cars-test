<?php

namespace App\Http\Controllers\Auth;

use Socialite;
use Session;
use App\Customer;
use App\Social;
use App\Page;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Lang;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('auth.login');
    }

    protected function sendLoginResponse(Request $request) {
        $request->session()->regenerate();
        $this->clearLoginAttempts($request);
        $customer = Auth::guard('web')->user();
        $old='';
        
        if($request->has('to')){
            $old = $request->to;
        }
        // var_dump($old);die;
        // var_dump(Lang::locale());die;
        if($old != ''){
            return redirect()->route($old);
        }else{
            $lang = \Config::get('app.locale');
            if($lang == 'en'){
                return redirect('/en');
            }
            return redirect('/');
        }
        
    }

    /**
     *  add function credentials and username
     */
    protected function credentials(Request $request)
    {
        $field = filter_var($request->input($this->username()), FILTER_VALIDATE_EMAIL) ? 'email' : 'membership_id';

        $request->merge([$field => $request->input($this->username())]);
        $credentials = $request->only($field, 'password');
        $credentials['status'] = 1;
        return $credentials;
    }

    /** 这个是声明账户登录的name值 */
    public function username()
    {
        return 'login';
    }

    /**
     * Log the user out of the application.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request) {
        $this->guard()->logout();
        return redirect('/');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard() {
        return Auth::guard('web');
    }

    /**
     * Redirect the user to the Facebook, Google authentication page.
     *
     * @return Response
     */
    public function redirectToProvider(Request $request, $provider) {
        if ($request->has('thuc-don')) Session::put('getShareLink', $request->get('thuc-don'));
        else {
            Session::put('beforeLogin', redirect()->back()->getTargetUrl());

        }
//        var_dump( Socialite::driver($provider)); die;
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from Facebook, Google.
     *
     * @return Response
     */
    public function handleProviderCallback($provider) {
        try {
            $customer_info = Socialite::driver($provider)->user();
        } catch (\Exception $ex) {
            return redirect(Session::pull('beforeLogin', '/'));
        }
        $customer = Customer::where('email', '=', $customer_info->getEmail())->first();

        if (!$customer) {
            $customer = Customer::create([
                'name' => $customer_info->getName(),
                'email' => $customer_info->getEmail(),
                'password' => bcrypt($customer_info->getEmail()),
                'avatar' => $customer_info->getAvatar()
            ]);
        }

        $social = $customer->socials()->where('provider', $provider)->where('social_id', $customer_info->getId())->first();
        if (!$social) {
            $customer->socials()->save(new Social([
                'provider'  => $provider,
                'social_id' => $customer_info->getId()
            ]));
        }

        $this->guard()->login($customer, true);

        if (Session::has('getShareLink')) return redirect()->action('GroupController@getShareLink', Session::pull('getShareLink'));

        return redirect(Session::pull('beforeLogin', '/'));
    }

}
