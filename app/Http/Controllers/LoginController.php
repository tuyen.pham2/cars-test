<?php

namespace App\Http\Controllers;

use Hash;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login_frontend()
    {
        return view('auth.login');
    }

    public function postLogin(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            if ($request->has('redirect')) {
                return redirect()->route($request->redirect)
                        ->withSuccess('Signed in');
            }

            return redirect()->route('home')
                        ->withSuccess('Signed in');
        }

        return redirect()->back()->withError('Login Failed!');
    }

    public function register()
    {
        return view('auth.register');
    }

    public function postRegister(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
        ]);

        $data = $request->only(['first_name', 'last_name', 'email', 'password', 'phone', 'address', 'city', 'district']);
        $data['name'] = $request->first_name . ' ' . $request->last_name;

        try {
            $check = $this->create($data);

            $credentials = $request->only('email', 'password');
            Auth::attempt($credentials);

            if ($request->has('redirect') && $request->redirect != '') {
                return redirect()->route($request->redirect)->withSuccess('You have signed-in');
            }

            return redirect()->route('home')->withSuccess('You have signed-in');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['msg' => $e->getMessage()]);
        }
    }

    public function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'phone' => $data['phone'],
            'address' => $data['address'],
            'city' => $data['city'],
            'district' => $data['district'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
