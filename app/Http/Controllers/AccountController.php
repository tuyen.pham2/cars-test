<?php

namespace App\Http\Controllers;

use Session;
use App\Models\Order;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function account()
    {
        $user = $this->user();

        $recent_orders = Order::with('items')->where('user_id', $user->id)->limit(5)->get();

        return view('accounts.dashboard', compact('user', 'recent_orders'));
    }

    public function orders()
    {
        $user = $this->user();
        $orders = Order::with('items')->where('user_id', $user->id)->get();

        return view('accounts.orders', compact('user', 'orders'));
    }

    public function detail()
    {
        $user = $this->user();

        return view('accounts.detail', compact('user'));
    }

    private function user()
    {
        return auth()->user();
    }

    public function json_detail()
    {
        $cities = \App\Models\City::list();

        $arr['profile'] = $this->user();
        $arr['cities'] = $cities;

        // dd($arr['cities']);
        // $districts = $ds[array_shift(array_values($arr['profile']['city']))];
        $districts = \App\Models\District::list()[array_values($cities)[0]];

        if (! is_null($arr['profile']['city'])) {
            $districts = \App\Models\District::list()[$arr['profile']['city']];
        }
        $arr['districts'] = $districts;

        return $arr;
    }

    public function json_getdistricts($value)
    {
        $districts = \App\Models\District::list();
        if (isset($districts[$value])) {
            return \App\Models\District::list()[$value];
        }
    }

    public function update(Request $request)
    {
        $data = $request->only(['first_name', 'last_name', 'phone', 'address', 'city', 'district']);
        $user = $this->user();

        if ($request->has('password')) {
            $data['password'] = bcrypt($request->password);
        }

        $user->update($data);

        Session::flash('response', ['status' => 'success', 'message' => 'Success!!']);

        return back();
    }

    public function json_address()
    {
        $cities = \App\Models\City::list();
        $arr['cities'] = $cities;
        $districts = \App\Models\District::list()[array_values($cities)[0]];
        $arr['districts'] = $districts;

        return $arr;
    }

    public function getdistricts(Request $request)
    {
        if ($request->has('city')) {
            return \App\Models\District::list()[$request->city];
        }
    }

    public function order_detail($id)
    {
        $user = auth()->user();
        $order = Order::with('items')->where('id', $id)->where('user_id', $user->id)->firstOrFail();

        return view('accounts.order_detail', compact('user', 'order'));
    }
}
