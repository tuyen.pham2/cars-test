<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Redirect;

class checkoutAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check() == false) {
            $login = route('login_frontend');
            return redirect()->back()->with('error', 'You have to log in to checkout, <a href="/login_frontend?redirect=checkout">Click here to Login</a> or <a href="/register?redirect=checkout">Register</a>');   
        }
        return $next($request);
    }
}
