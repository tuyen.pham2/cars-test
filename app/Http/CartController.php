<?php

namespace App\Http\Controllers;

use Agent;
use App\City;
use App\Dish;
use App\Page;
use App\District;
use Cache;
use Carbon\Carbon;
use App\Order;
use App\Customer;
use App\SaleOrder;
use App\SaleOrderDetail;
use Cart;
use DB;
use Auth;
use Illuminate\Http\Request;
use Lang;
use Mail;
use Session;
use DateTime;
use App\Mail\EmailReply;
use App\Email;

class CartController extends Controller
{
    use EmailReply;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $rows = [];

        foreach (Cart::content()->reverse() as $item) {

            $dish = Dish::find($item->id);
            $options = [];
            //
            if(isset($dish ) ){

                if($dish->type == Dish::TYPE_COMBO){
                    foreach ($item->options as $side) {
                        if(isset($side) && isset($side['ids'])){
                            $d_op = Dish::find($side['ids']);

                            if(isset($d_op)){
                                foreach ($d_op as $opt ) {

                                    $options['extra'][$opt->id] =
                                        [
                                            'id' => $opt->id,
                                            'title' => $opt->title,
                                            'qty' => 1,
                                            'price' => $opt->price
                                        ];
                                }
                            }
                        }

                    }
                    if(isset($item->options["notes"]["size"])) {
                        $sizeInfo = json_decode($item->options["notes"]["size"], true);
                        $options['notes'] =
                            [
                                'size' => [
                                    'title' => $sizeInfo["title"]  ,
                                    'price' => $sizeInfo["price"]
                                ],
                                'notes' => $item->options["notes"]["orderNotes"]
                            ];
                    }
                }

                $rows[] = [
                    'id' => $item->id,
                    'rowId' => $item->rowId,
                    'title' => $item->name,
                    'qty' => $item->qty,
                    'price' => $item->price,
                    'image' => $dish->getImage('full'),
                    'options' => $options
                ];
            }
        }

        return response()->json([
            'data' => $rows,
            'total' => Cart::total()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $id = (int) $request->id;
        $dish = Dish::findOrFail($id);

        $side_dishes["discount"] = $dish->discount;

        //Dữ liệu nhận thêm của request
        $values = $request->all();
        $values['title'] = $dish->title;
        $values['code'] = $dish->product_id;
        $values['type'] = $dish->type;

        //nếu có các thuộc tính của sản phẩm
        $propId = (int) $request->productSizeId;
        if ($propId > 0)
        {
            $arrProp = json_decode($dish->props,true);

            $dish->price = isset($arrProp[$propId]['price']) ? $arrProp[$propId]['price'] : $arrProp[$propId];
            if(isset($arrProp[$propId]['code'])){
                $values['code'] = $arrProp[$propId]['code'] ;
            }
        }

        //Nếu có giảm giá
        if(isset($dish->discount) && $dish->discount){
            $values['realPrice'] = $dish->price;
            $dish->price = $dish->price * (100 - $dish->discount) / 100;
            $values['discount'] = $dish->discount;
        }
        
        # Nếu là combo
        if ($request->has('ids')) {
            $side_dishes = [];

            foreach ($request->ids as $value) {

                $side_dish = 'side_dish_' . $value;

                $$side_dish = Cache::remember("dish.$value", 1440, function() use($value) {
                    return Dish::findOrFail($value);
                });

                $$side_dish->name = $$side_dish->getOriginal('title');
                $dish->price += $$side_dish->price;
                $side_dishes[] = $$side_dish;
            }
            
            //Gán thêm tên cho extra
            foreach ($side_dishes as $key => $side_dish) {
                $side_dish->name = $side_dish->title;
                $side_dishes[$key] = $side_dish;
            }
            Cart::add($dish, (int) $request->qty, ['notes' => $values, 'extra' => $side_dishes])->associate('App\Dish');
        }
        else {
            Cart::add($dish, (int) $request->qty, [
                'notes' => $values
            ])->associate('App\Dish');
        }

        return response()->json(['status' => 'success']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        Cart::update($id, (int) $request->qty);
        return response()->json(['status' => 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Cart::remove($id);
        return response()->json(['status' => 'success']);
    }

    /**
     * Lưu order vào db và gửi thông tin theo yêu cầu (gửi mail hoặc gửi qua api)
     */
    public function checkout(Request $request) {

        // NGHỈ TẾT
        // Session::flash('response', ['status' => 'error', 'message' => Lang::get('cart.stop_order')]);
        // return redirect()->action('SiteController@index');

        // TẮT ĐẶT HÀNG
        $setting_get = \DB::table('settings')->where('name','sale_order_settings')->first();
        if ( $setting_get ) {

            $setting = unserialize($setting_get->value);
            // dd($setting);
            $setting = $setting['setting'];

            if ($setting['turn_of_order']) { // nếu tắt đặt hàng checked

                $message = Lang::get('cart.stop_order');
                if (isset($setting['turn_of_mess']['vi']) && $setting['turn_of_mess']['vi'] != '') {
                    $message = $setting['turn_of_mess']['vi'];

                    if (Lang::locale() == 'en' && $setting['turn_of_mess']['en'] != '') {
                        $message = $setting['turn_of_mess']['en'];
                    }

                }


                Session::flash('response', ['status' => 'error', 'message' => $message ]);
                return redirect()->action('SiteController@index');
            }
            
        }
        // Session::flash('response', ['status' => 'error', 'message' => Lang::get('cart.stop_order')]);
        // return redirect()->action('SiteController@index');

        // KHÔNG CHO ĐẶT HÀNG VÀO CN
        // if($this->isWeekend()){
        //     Session::flash('response', ['status' => 'error', 'message' => Lang::get('cart.out_of_time')]);
        //     return redirect()->action('SiteController@index');
        // }

        // if($this->isWeekend()){
        //     Session::flash('response', ['status' => 'error', 'message' => Lang::get('cart.out_of_time')]);
        //     return redirect()->action('SiteController@index');
        // }

        // if (!$this->checkTimeServe()) {
        //     Session::flash('response', ['status' => 'error', 'message' => trans('user.message.time')]);
        //     return redirect()->action('SiteController@index');
        // }

        // MODAL LOGIN
        // if (auth()->guard('customer')->user() == null || auth()->guard('customer')->user()->id <= 0) {
        //     Session::flash('response', [
        //         'status' => 'success',
        //         'message' => '',
        //         'showModal' => 'PleaseLogin',
        //     ]);
        // }

        // CHECK TIME
        // dd($request->all());
        if (!checkTimeServe()){
            // QUÁ THỜI GIAN THANH TOÁN
            Session::flash('response', ['status' => 'error', 'message' => Lang::get('cart.out_of_time')]);
            return redirect()->action('SiteController@index');
        }

        if ($request->method() === 'POST') {
            // var_dumsp($request->all());die;

            // KIỂM TRA ĐĂNG NHẬP HAY KHÔNG ĐỂ LƯU COOKIE
            if(auth()->guard('customer')->user() == null || auth()->guard('customer')->user()->id <= 0){
                $values = serialize($request->all());
                $minutes = 527040;
                // \Cookie::make('checkoutdata', $values, $minutes);
                \Cookie::queue(\Cookie::forever('checkoutdata', $values));
            }

            $request->merge([
                'city' => (int) filter_var($request->city, FILTER_SANITIZE_NUMBER_INT),
                'district' => (int) filter_var($request->district, FILTER_SANITIZE_NUMBER_INT)
            ]);

            $messages = array(
                'firstname.required' => trans('cart.checkoutform.firstname_required'),
                'address.required' => trans('cart.checkoutform.address_required'),
                'address.min' => trans('cart.checkoutform.address_min_6'),
                'address.max' => trans('cart.checkoutform.address_max_255'),
                'phone.digits_between' => trans('cart.checkoutform.phone_between_10_25'),
                'phone.required' => trans('cart.checkoutform.phone_required'),
                'city.required' => trans('cart.checkoutform.city_required'),
                'district.required' => trans('cart.checkoutform.district_required'),
            );
            // $messages = array(
            //     'firstname.required' => 'Họ tên bắt buộc',
            //     'address.required' => 'Vui lòng điền địa chỉ',
            //     'address.min' => 'Địa chỉ tối thiểu 6 ký tự',
            //     'address.max' => 'Địa chỉ tối đa 255 ký tự',
            //     'phone.digits_between' => 'Số điện thoại từ 10 tới 15 số',
            //     'phone.required' => 'Vui lòng điền số điện thoại',
            //     'city.required' => 'Vui lòng chọn thành phố',
            //     'district.required' => 'Vui lòng chọn quận huyện',
            // );
            //Bắt check validate
            $this->validate($request, [
                'firstname'     => 'required|min:2|max:255',
                // 'lastname'      => 'required|min:2|max:255',
                'phone'         => 'required|digits_between:10,15',
                'city'          => 'required|exists:cities,id',
                'district'      => 'required|exists:districts,id',
                'address'       => 'required|min:6|max:255',
                'gartId'        => 'alpha_num|size:16'
            ],$messages);
            Session::put('customerFirstname', $request->firstname);
            // Session::put('customerLastname', $request->lastname);
            Session::put('customerPhone', $request->phone);
            Session::put('customerAddress', $request->address);
            $value = $request->all();
            
            $errorCode = '';
            
                //session sản phẩm
                $dishes = Cart::content()->reverse();
                // dd($dishes);
                $productList = []; //Mảng dữ liệu sản phẩm
                $pricesell = 0;
                $priceshipping = 0;
                $pricetax = 0;
                $pricediscount = 0;
                $pricefinal = 0;
                $quantity = 0;
                //Duyệt mảng sản phẩm để lưu lai các thông tin
                foreach($dishes as $key => $dish){
                    $ids = '';
                    $i = 1;
                    if(isset($dish->options->notes['ids'])){
                        if(is_array($dish->options->notes['ids']) && count($dish->options->notes['ids'])>0){
                            foreach($dish->options->notes['ids'] as $idextra){
                                if($i > 1){
                                   $ids .= ','; 
                                }
                                $ids .= $idextra;
                                $i++;
                            }
                        }
                    }
                    
                    
                    $pricefinal += $dish->qty * $dish->price;
                    $pricesell += $dish->qty * $dish->price;
                    $productList[$key]['product_id']      = $dish->id;
                    $productList[$key]['product_code']      = $dish->product_id;
                    $productList[$key]['itemprice']      = $dish->price;
                    $productList[$key]['itemname']      = $dish->options->notes['title'];
                    $productList[$key]['quantity']        = $dish->qty;
                    $productList[$key]['itemfinalprice']  = $dish->price;
                    $productList[$key]['subtotal']       = $dish->price * $dish->qty;
                    $productList[$key]['note']       = isset($dish->options->notes['orderNotes']) ? $dish->options->notes['orderNotes'] : '';
                    $productList[$key]['itemunit']       = $ids;
                    $productList[$key]['itemsize']       = $dish->options->notes['size'] ?? '';
                    $quantity += $dish->qty;
                }
                // kiểm tra số lượng sản phẩm của đơn hàng
                if ( $setting_get ) {
                    $setting = unserialize($setting_get->value);
                    $setting = $setting['setting'];
                    $min = $setting['min_order_item'];
                    $max = $setting['max_order_item'];
                } else {
                    $min = 4;
                    $max = 6;
                }
                
                if ( $quantity >= $min && $quantity <= $max ) {
                    # Kiểm tra địa chỉ
                    $city = City::findOrFail($request->city);
                    $district = District::findOrFail($request->district);
                    $customer_id = 0;
                    if (auth()->guard('customer')->user() != null) {
                        $customer_id = auth()->guard('customer')->user()->id;
                    }
                    //$customer = Customer::findOrFail($customer_id);
                    //Create sale order
                    $mySaleOrder = new SaleOrder();
                    $mySaleOrder->user_id = 0;
                    $mySaleOrder->customer_id = (int)$customer_id;
                    $mySaleOrder->ordercode = (string) SaleOrder::getLastOrderCode();
                    $mySaleOrder->pricesell = $pricesell;
                    $mySaleOrder->priceshipping = $priceshipping;
                    $mySaleOrder->pricetax = $pricetax;
                    $mySaleOrder->pricediscount = $pricediscount;
                    $mySaleOrder->pricefinal = $pricefinal;
                    $mySaleOrder->billinggender = 0;
                    $mySaleOrder->billingfullname = $request->firstname . ' ' . $request->lastname;
                    $mySaleOrder->billingphone = $request->phone;
                    $mySaleOrder->billingaddress = $request->address;
                    $mySaleOrder->billingcityid = $city->id;
                    $mySaleOrder->billingdistrictid = $district->id;
                    $mySaleOrder->billingward = $request->ward;
                    $mySaleOrder->shippingfullname = $request->firstname . ' ' . $request->lastname;
                    $mySaleOrder->shippingphone = $request->phone;
                    $mySaleOrder->shippingaddress = $request->address;
                    $mySaleOrder->shippingcityid = $city->id;
                    $mySaleOrder->shippingdistrictid = $district->id;
                    $mySaleOrder->shippingward = $request->ward;
                    $mySaleOrder->quantity = $quantity;
                    $mySaleOrder->note = $request->note ?? 'n/a';
                    $mySaleOrder->paymentmethod = SaleOrder::PAYMENT_METHOD;
                    $mySaleOrder->isdeleted = 0;
                    $mySaleOrder->isdeletedby = 0;
                    $mySaleOrder->status = SaleOrder::STATUS_NEW;
                    $mySaleOrder->cancelreason = '';
                    if ($mySaleOrder->save()) {
                        //Insert order detail
                        foreach ($productList as $myProduct) {
                            $mySaleOrderDetail = new SaleOrderDetail();
                            $mySaleOrderDetail->sale_order_id = $mySaleOrder->id;
                            $mySaleOrderDetail->product_id = $myProduct['product_id'];
                            $mySaleOrderDetail->product_code = $myProduct['product_code'];
                            $mySaleOrderDetail->customer_id = $customer_id;
                            $mySaleOrderDetail->itemname = $myProduct['itemname'];
                            $mySaleOrderDetail->itemcategoryid = 0;
                            $mySaleOrderDetail->quantity = $myProduct['quantity'];
                            $mySaleOrderDetail->itemprice = $myProduct['itemprice'];
                            $mySaleOrderDetail->tax = 0;
                            $mySaleOrderDetail->itemfinalprice = $myProduct['itemfinalprice'];
                            $mySaleOrderDetail->subtotal = $myProduct['subtotal'];
                            $mySaleOrderDetail->note = $myProduct['note'];
                            $mySaleOrderDetail->itemunit = $myProduct['itemunit'];
                            $mySaleOrderDetail->itemsize = $myProduct['itemsize'];
                            if ($mySaleOrderDetail->save()) {
                                //Loghere
                            }
                            
                        }
                        $email = $request->email;
                        $mailsend = '';
                        if (Auth::guard('customer')->check()) {
                            $email = auth()->guard('customer')->user()->email;
                            // dd($email); 
                        }

                        // test template mail
                        // return view('mail.complete',compact('mySaleOrder','dishes'));

                        try{
                            // $this->sendMailafterComplete($mySaleOrder,$email);
                            $this->sendMailafterComplete2($mySaleOrder,$email,$dishes);
                        }catch(\Exception $e){
                            $mailsend = $e->getMessage();
                        }
                        
                        Cart::destroy();
                    } else {
                        $errorCode = 1;
                    }
                } else {
                    $message = Lang::get('cart.not_enough_number_product');

                    if ( $setting_get ) {
                        $setting = unserialize($setting_get->value);
                        $setting = $setting['setting'];

                        if (isset($setting['order_item_mess']['vi']) && $setting['order_item_mess']['vi'] != '') {
                            $message = $setting['order_item_mess']['vi'];

                            if (Lang::locale() == 'en' && $setting['order_item_mess']['en'] != '') {
                                $message = $setting['order_item_mess']['en'];
                            }

                        }
                    }

                    return response()->json(['status' => 'error', 'message' => $message]);
                }

            if ($errorCode === '') {
                Session::put('orderCode', $mySaleOrder->ordercode);
                return response()->json(['status' => 'success', 'message' => action('SiteController@thank'),'mail' => $mailsend]);
            } else {
                return response()->json(['status' => 'error', 'message' => trans('cart.error.' . $errorCode),'mail'=>$mailsend]);
            }
        }
        # Kiểm tra đơn hàng tối thiểu
        $cookie_city = (int) $request->cookie('customerCity');
        $cookie_district = (int) $request->cookie('customerDistrict');
        $clastname = '';
        $cfirstname = '';
        $cphone = '';
        $cemail = '';
        $caddress = '';
        $ccity = '';
        $cdistrict = '';
        $cward = '';
        // var_dump(\Cookie::get('checkoutdata'));die;
        if(\Cookie::get('checkoutdata') !== null){
            $values = unserialize(\Cookie::get('checkoutdata'));
            // $clastname = isset($values['lastname']) ? $values['lastname'] : ''; 
            $cfirstname = isset($values['firstname']) ? $values['firstname'] : ''; 
            $cphone = isset($values['phone']) ? $values['phone'] : '';
            $cemail = isset($values['email']) ? $values['email'] : '';
            $caddress = isset($values['address']) ? $values['address'] : '';
            // $ccity = $values['city'];
            $ccity = isset($values['city']) ? $values['city'] : 0 ;
            $cdistrict = isset($values['district']) ? $values['district'] : 0 ;
            $cward = isset($values['ward']) ? $values['ward'] : '';
        }
        $district = District::find($cookie_district);
        if ($district and Cart::total() < $district->min_price) {
            Session::flash('response', ['status' => 'error', 'message' => trans('user.message.area.alert', ['num' => format_money($district->min_price)])]);
            if (str_contains(url()->previous(), 'thuc-don')) return back();
            return redirect()->action('GroupController@index');
        }

        /*=TODO*/

        //Mảng lưu id extra để query
        $arrayExtras = [];
        foreach (Cart::content() as $item){

            if(isset($item->options->notes['ids']))
                $arrayExtras[$item->options->notes['ids'][0]] = $item->options->notes['ids'][0];
        }
        /*get temp dish for dummy data*/
        $extras = Dish::find($arrayExtras);

        $cities = City::where('delivery',1)->get();
        $districts = [];
        if($ccity != ''){
            $districts = District::where('city_id',$ccity)->where('delivery',1)->get();
        }else{
            if($cities && count($cities) > 0){
                $districts = District::where('city_id',$cities[0]->id)->where('delivery',1)->get();
            }  
        }
        
        return view('site.checkout.normal', compact('extras', 'cookie_city', 'cookie_district','cdistrict','ccity','caddress','cemail','cphone','cfirstname','clastname','cward','cities','districts'));
    }

    protected function checkTimeServe() {
        # Kiểm tra thời gian phục vụ
        $start = Carbon::today()->addMinutes(config('cart.timeCheckout.start') * 60);
        $end = Carbon::today()->addMinutes(config('cart.timeCheckout.end') * 60);

        return Carbon::now()->between($start, $end);
    }

    // protected function callApi($api) {

    //     $value['hash'] = md5($api['customerName'].$api['customerPhone'].'cloudteam');
    //     $value['order_detail'] = $api;
    //     var_dump($api);die;
    //     $value = json_encode($value);

    //     $url = "http://phuclong.cloudteam.vn/api/create_order.php";

    //     ini_set('max_execution_time', 300);
    //     $curl = curl_init();
    //     curl_setopt($curl, CURLOPT_URL, $url);
    //     curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    //     curl_setopt($curl, CURLOPT_POST, true);
    //     curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    //     curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    //     curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    //     if (!empty($value)) {
    //         curl_setopt($curl, CURLOPT_POSTFIELDS, $value);
    //     }
    //     $response = curl_exec($curl);
    //     curl_close($curl);

    //     var_dump($response);die;
        
    //     if ($response->crm_id === '' and $response->error === 3) $this->callApi($params);
    //     return $response;
    // }

    /**
     * Function: Gửi mail đơn hàng
     */
    public function sendMail($values){
        $data = Cart::content()->reverse();
        Mail::send('site.email', array(
                'values'      => $values, 
            ), function($message){
            $message->to('rock313tl@gmail.com', 'PhucLong')->subject('Đơn đặt hàng');
        }); 
        return true;
    }

    /**
     * Function: Kiểm tra ngày cuối tuần
     * Return : true or false
     */
    public function isWeekend() {
        $date = date('Y-mm-dd');
        $weekDay = date("D", strtotime($date));
        if($weekDay == 'Sun'){
            return true;
        }
        return false;
    }

    //send mail after cart complete
    public function sendMailafterComplete($data,$email){
        $maildata =$data;
        $to = [
            'address' => $email,
            'name' => $data->billingfullname
        ];
        $cc = '';//explode(',', $email->cc);
        $bcc = '';//explode(',', $email->bcc);
        $this->sendMailCartComplete($maildata, $to, $cc, $bcc,"Đặt hàng thành công");
    }
    public function sendMailafterComplete2($data,$email,$dishes){
        $maildata = [];
        $maildata['data'] =$data;
        $maildata['dishes'] =$dishes;
        $to = [
            'address' => $email,
            'name' => $data->billingfullname
        ];
        $cc = '';//explode(',', $email->cc);
        $bcc = '';//explode(',', $email->bcc);
        $this->sendMailCartComplete2($maildata, $to, $cc, $bcc,"Đặt hàng thành công");
    }

    public function ajaxDistrict(Request $request){
        $delivery = true;
        if($request->has('delivery')){
            $delivery = $request->delivery;
        }
        // dd($delivery);
        if($request->has('id')){
            if($delivery == 'true'){  
                $districts = District::where('city_id',$request->id)->where('delivery',1)->get();
            }else{
     
                $districts = District::where('city_id',$request->id)->get()->toArray();
            }
            return $districts;
        }
        return;
    }
}