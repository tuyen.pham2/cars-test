<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CouponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $params = $this->route()->parameters();

        return [
            'code' => 'required|max:255|unique:coupons,code' . (array_key_exists('id', $params) ? ", {$params['id']}" : ''),
            'name' => 'required',
            'value' => 'required',
        ];
    }
}
