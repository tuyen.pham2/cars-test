<?php

namespace App\Repositories\Coupon;

use App\Repositories\BaseRepository;

class CouponRepository extends BaseRepository implements CouponRepositoryInterface
{
    public function getModel()
    {
        return \App\Models\Coupon::class;
    }
}
