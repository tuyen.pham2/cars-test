<?php

namespace App\Repositories\AttributeValue;

use App\Repositories\BaseRepository;

class AttributeValueRepository extends BaseRepository implements AttributeValueRepositoryInterface
{
    public function getModel()
    {
        return \App\Models\AttributeValue::class;
    }
}
