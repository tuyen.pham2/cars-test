<?php
namespace App\Repositories\Car;

use App\Repositories\RepositoryInterface;

interface CarRepositoryInterface extends RepositoryInterface
{
    //ví dụ: lấy 5 sản phầm đầu tiên
    public function getCar();
}