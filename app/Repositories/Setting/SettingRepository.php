<?php

namespace App\Repositories\Setting;

use App\Repositories\BaseRepository;

class SettingRepository extends BaseRepository implements SettingRepositoryInterface
{
    public function getModel()
    {
        return \App\Models\Setting::class;
    }
}
