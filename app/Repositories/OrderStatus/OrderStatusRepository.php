<?php

namespace App\Repositories\OrderStatus;

use App\Repositories\BaseRepository;

class OrderStatusRepository extends BaseRepository implements OrderStatusRepositoryInterface
{
    public function getModel()
    {
        return \App\Models\OrderStatus::class;
    }
}
