<?php

namespace App\Mail;

interface MailInterface
{
    const HOST = '127.0.0.1';

    const DRIVER = 'smtp';

    const PORT = '1025';

    const USERNAME = '';

    const PASSWORD = '';

    const ENCRYPTION = '';

    const FROM_ADDRESS = '';

    const FROM_NAME = '';

    const VIEW = 'mails.test';

    const EMAIL = 'test@ceresolutions.com';

    const SUBJECT = 'Test mail';

    public function sendMail($to, $cc = null, $bcc = null, $subject);
}
