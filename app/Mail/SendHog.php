<?php

namespace App\Mail;

use Mail;
// use Illuminate\Mail\Mailable;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

// class SendHog extends Mailable implements MailInterface
class SendHog implements MailInterface
{
    // use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function sendMail($to, $cc = null, $bcc = null, $subject)
    {
        $data = $this->data;

        Mail::send(self::VIEW, compact('data'), function ($message) use ($to, $cc, $bcc, $subject) {
            $message->from(config('mail.from.address'), config('mail.from.name'));

            $message->to($to)->subject($subject);

            if (! empty($cc[0])) {
                $message->cc($cc);
            }
            if (! empty($bcc[0])) {
                $message->bcc($bcc);
            }
        });
    }
}
