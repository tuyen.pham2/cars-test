<?php
use Elasticsearch\ClientBuilder;

if (!function_exists('es_connect')) {

    function es_connect()
    {
        $hosts = [
            'http://localhost:9200' 
        ];
        $client = ClientBuilder::create()->setHosts($hosts)->build();

        return $client;
    }
}

