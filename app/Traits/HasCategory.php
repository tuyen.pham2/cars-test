<?php

namespace App\Traits;

use App\Models\Category;

trait HasCategory
{
    /**
     * The "booting" method of the trait.
     *
     * @return void
     */
    public static function bootHasCategory()
    {
        static::saved(function ($entity) {
            if (request()->has('categories')) {
                $entity->categories()->sync(request()->categories);
            }
        });
    }

    public function categories()
    {
        return $this->morphToMany(Category::class, 'categorizable', 'categorizables', 'item_id', 'categorizable_id');
    }
}
