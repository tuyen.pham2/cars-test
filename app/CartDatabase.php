<?php

namespace App;

use Auth;
use Cache;
use App\Models\Coupon;
use App\Models\CarGroup;
use App\Models\CartItem;
use App\Models\CartCoupon;
use App\Models\CartContent;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Collection;
use Illuminate\Session\SessionManager;
use App\Exceptions\CouponNotExistException;
use Illuminate\Contracts\Events\Dispatcher;

class CartDatabase implements CartInterface
{
    private $session;

    private $events;

    private $instance;

    private $session_id;

    private $user_id;

    private $cart;

    private $coupon;

    public function __construct(SessionManager $session, Dispatcher $events)
    {
        $this->session = $session;
        $this->events = $events;
        $this->session_id = $this->session->getId();
        $this->cart = $this->getCartContent();

        $this->instance('database');
        $this->user_id = null;
        if (Auth::check()) {
            $this->user_id = auth()->user()->id;
        }
    }

    public function instance($instance = null)
    {
        $instance = $instance ?: 'database';

        $this->instance = sprintf('%s.%s', 'cart', $instance);

        return $this;
    }

    private function addProduct($id, $price, $name, $image, $qty, $idcart)
    {
        if ($this->hasItem($idcart)) {
            $this->update($idcart, $qty);
        } else {
            $cart = CartContent::firstOrCreate(['session_id' => $this->session_id], ['user_id' => $this->user_id]);

            $data = [
                'session_id' => $this->session_id,
                'cartname_id' => $idcart,
                'cart_id' => $cart->id,
                'item_id' => $id,
                'price' => $price ?? 0,
                'qty' => $qty ?? 1,
                'name' => $name,
                'image' => $image,
            ];
            CartItem::create($data);
        }
    }

    private function addGroup($id, $price, $name, $image, $qty, $group_id, $idcart)
    {
        if ($this->hasItem($idcart)) {
            $this->update($idcart, $qty);
        } else {
            $group_detail = CarGroup::where('id', $group_id)->first();

            if ($group_detail) {
                $group_detail = $group_detail->toArray();
            } else {
                $group_detail = [];
            }

            $data = [
                'session_id' => $this->session_id,
            ];

            $cart = CartContent::firstOrCreate($data);

            $data = [
                'session_id' => $this->session_id,
                'cartname_id' => $idcart,
                'cart_id' => $cart->id,
                'item_id' => $id,
                'price' => $price ?? 0,
                'qty' => $qty ?? 1,
                'name' => $name,
                'image' => $image,
                'group_id' => $group_id,
                'group_detail' => serialize($group_detail),
            ];
            CartItem::create($data);
        }
    }

    public function add($id, $price, $name, $image, $qty = null, $group_id = null)
    {
        if ($qty === null) {
            $qty = 1;
        }

        $idcart = $this->idItem($id, $group_id);

        if (is_null($group_id)) {
            $this->addProduct($id, $price, $name, $image, $qty, $idcart);
        } else {
            $this->addGroup($id, $price, $name, $image, $qty, $group_id, $idcart);
        }

        $this->updateCache();
    }

    public function content()
    {
        if (is_null($this->cart)) {
            return new Collection;
        }

        $cart = $this->cart->items->toArray();

        $cart = array_map(function ($item) {
            $item['group_detail'] = unserialize($item['group_detail']);

            return $item;
        }, $cart);

        return $cart;
    }

    public function get($id)
    {
    }

    public function update($id, int $qty)
    {
        $cart = $this->cart;

        if ($cart) {
            $cartItem = CartItem::where('cart_id', $cart->id)->where('cartname_id', $id)->first();
            if ($cartItem) {
                $cartItem->qty = $cartItem->qty + $qty;
                $cartItem->save();
                $this->updateCache();
            }
        }
    }

    public function total()
    {
        $total = 0;
        $cart = $this->cart;

        if ($cart) {
            foreach ($cart->items as $item) {
                $total += $item->qty;
            }
        }

        return $total;
    }

    public function destroy()
    {
        CartContent::where('session_id', $this->session_id)->delete();
        Cache::forget('cart_' . $this->session_id);
    }

    public function remove($id)
    {
        try {
            CartItem::where('cart_id', $this->cart->id)->where('cartname_id', $id)->delete();

            // Check total

            foreach ($this->cart->items as $item) {
                if ($item->cartname_id == $id) {
                    $minus = $item->qty;

                    if (($this->total() - $minus) <= 0) {
                        CartContent::where('session_id', $this->session_id)->delete();
                    }

                    break;
                }
            }
            $this->updateCache();
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return 'Remove Success!';
    }

    private function idItem($id, $group_id = null)
    {
        $name = 'product_' . $id;

        if (! is_null($group_id)) {
            $name = $name . '.group_' . $group_id;
        }

        return $name;
    }

    /**
     * return TRUE | FALSE
    */
    private function hasItem($id)
    {
        $cart = CartItem::where('session_id', $this->session_id)->where('cartname_id', $id)->first();
        if (! $cart) {
            return false;
        }

        return true;
    }

    public function getContent()
    {
        $content = $this->session->has('cart')
            ? $this->session->get('cart')
            : new Collection;

        return $content;
    }

    private function checkQty($groupid, $qty)
    {
        if (is_null($groupid)) {
            return true;
        }
        $g = CarGroup::where('id', $groupid)->first();
        if ($g) {
            if ($g->qty >= $qty) {
                return true;
            }
        }

        return false;
    }

    public function subTotal()
    {
        $total = 0;
        if (is_null($this->cart)) {
            return 0;
        }
        foreach ($this->cart->items as $item) {
            $total += ($item->qty * $item->price);
        }

        return $total;
    }

    public function totalPrice()
    {
        $total = $this->subTotal();
        $response = $this->plusCoupon($total);

        return $response;
    }

    private function getCartContent()
    {
        $session_id = $this->session_id;
        $user_id = $this->user_id;

        return Cache::remember('cart_' . $session_id . $user_id, 600, function () use ($session_id, $user_id) {
            $cart = CartContent::where('session_id', $session_id);
            if (! is_null($user_id)) {
                $cart = $cart->orWhere('user_id', $user_id);
            }

            return $cart->first();
        });
    }

    public function isEmpty()
    {
        if (is_null($this->cart)) {
            return true;
        }

        if (count($this->cart->items) <= 0) {
            return true;
        }

        return false;
    }

    public function updateCache()
    {
        $cart = CartContent::where('session_id', $this->session_id);
        if ($this->user_id != null) {
            $cart = $cart->orWhere('user_id', $this->user_id);
        }
        $cart = $cart->first();
        Cache::put('cart_' . $this->session_id, $cart, 600);
    }

    public function addCoupon($code)
    {
        $coupon = Coupon::where('code', $code)->first();
        if (! $coupon) {
            throw new CouponNotExistException;
        }

        $pipes = [
            // \App\Pipes\HasCoupon::class,
            \App\Pipes\CouponDate::class,
            \App\Pipes\CouponWithItem::class,
            // Check already added (later)
            // Check number usage (later)
            // Check limit for user (later)
        ];

        $cart = app(Pipeline::class)
            ->send($coupon)
            ->through($pipes)
            ->then(function ($content) use ($code) {
                $arr = [$code];

                // DO update cart coupon
                CartCoupon::where('cart_id', $this->cart->id)->delete();
                $coupon = Coupon::where('code', $code)->first();

                return CartCoupon::create([
                    'cart_id' => $this->cart->id,
                    'coupon_id' => $coupon->id,
                    'coupon_code' => $code,
                    'coupon_value' => $coupon->value,
                    'coupon_is_percent' => $coupon->is_percent,
                ]);
            });
        $this->updateCache();

        return true;
    }

    public function hasCoupon()
    {
        if (is_null($this->cart)) {
            return false;
        }
        if (count($this->cart->coupons) > 0) {
            return true;
        }

        return false;
    }

    public function getCoupons()
    {
        return '';
    }

    private function plusCoupon($total)
    {
        if (! $this->hasCoupon()) {
            return $total;
        }
        $coupons = $this->cart->coupons;
        foreach ($coupons as $coupon) {
            if ($coupon->coupon_is_percent == 1) {
                $total = $total - (($total * $coupon->coupon_value) / 100);
            } else {
                $total = $total - $coupon->coupon_value;
            }
        }

        return $total;
    }

    public function updateMulti($id, $qty)
    {
        $cart = $this->cart;
        $cartItem = CartItem::where('cart_id', $cart->id)->where('cartname_id', $id)->first();
        if ($cartItem) {
            $cartItem->qty = $qty;
            $cartItem->save();
        }
    }

    public function coupons()
    {
        if (is_null($this->cart)) {
            return [];
        }

        return $this->cart->coupons;
    }
}
