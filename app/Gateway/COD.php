<?php

namespace App\Gateway;

use App\Models\Order;
use Illuminate\Http\Request;

class COD implements GateInterface
{
    public $label;

    public $description;

    public function __construct()
    {
        $this->label = 'COD';
        $this->description = 'COD description';
    }

    public function purchase(Order $order, Request $request)
    {
        return new GatewayResponse();
    }

    public function complete(Order $order)
    {
        return new GatewayResponse();
    }
}
