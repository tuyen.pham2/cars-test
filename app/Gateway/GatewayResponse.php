<?php

namespace App\Gateway;

class GatewayResponse
{
    public function isRedirect()
    {
        return false;
    }

    public function isSuccess()
    {
        return true;
    }

    public function getTransactionReference()
    {
    }
}
