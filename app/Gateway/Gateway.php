<?php

namespace App\Gateway;

use Illuminate\Support\Arr;

class Gateway
{
    private $gates = [];

    public function all()
    {
        return collect($this->gates);
    }

    public function register($name, $driver)
    {
        $this->gates[$name] = is_callable($driver) ? call_user_func($driver) : $driver;

        return $this;
    }

    public function names()
    {
        return $this->array_keys($this->gates);
    }

    public function get($name)
    {
        return $this->array_get($this->gates, $name);
    }

    public function array_get($array, $key, $default = null)
    {
        return Arr::get($array, $key, $default);
    }
}
