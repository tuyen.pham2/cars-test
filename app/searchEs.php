<?php

namespace App;

use Elasticsearch\ClientBuilder;

class searchEs
{
    protected $client;

    protected $table;

    protected $fields;

    protected $keyword;

    public function __construct($table, array $fields, $keyword)
    {
        $this->table = $table;
        $this->fields = $fields;
        $this->keyword = $keyword;
        $hosts = [
            'http://localhost:9200',
        ];
        $this->client = ClientBuilder::create()->setHosts($hosts)->build();
    }

    public function search()
    {
        $params = [
            'body' => [
                'query' => [
                    'query_string' => [
                        'fields' => $this->fields,
                        'query' => $this->keyword . '*',
                    ],
                ],
            ],
        ];

        $results = $this->client->search($params);

        return $results;
    }
}
