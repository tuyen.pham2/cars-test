<?php

namespace App\Pipes;

use Closure;
use App\Models\Coupon;

class CouponWithItem implements PipeInterface
{
    /**
     * Check coupon adapt each items in cart
    */
    public function handle($content, Closure $next)
    {
        // not required
        return  $next($content);
    }
}
