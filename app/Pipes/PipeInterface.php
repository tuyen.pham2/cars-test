<?php

namespace App\Pipes;

use Closure;

interface PipeInterface
{
    public function handle($content, Closure $next);
}
