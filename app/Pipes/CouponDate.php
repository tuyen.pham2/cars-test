<?php

namespace App\Pipes;

use Closure;
use Carbon\Carbon;
use App\Models\Coupon;
use App\Exceptions\CouponExpiredException;

class CouponDate implements PipeInterface
{
    /**
     * Check coupon date start and date end
    */
    public function handle($content, Closure $next)
    {
        $now = Carbon::now();

        $start_date = new Carbon($content->start_date);
        $end_date = new Carbon($content->end_date . ' 23:59:59');

        if ($now->gte($start_date) && $now->lte($end_date)) {
            return  $next($content);
        }

        throw new CouponExpiredException;
    }
}
