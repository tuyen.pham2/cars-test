<?php

namespace App;

use App\Car;
use Session;
use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\Collection;
use PDO;

class Cart
{

    /**
     * Instance of the session manager.
     *
     * @var \Illuminate\Session\SessionManager
     */
    private $session;

    /**
     * Instance of the event dispatcher.
     * 
     * @var \Illuminate\Contracts\Events\Dispatcher
     */
    private $events;

    /**
     * Holds the current cart instance.
     *
     * @var string
     */
    private $instance;

    /**
     * Cart constructor.
     *
     * @param \Illuminate\Session\SessionManager      $session
     * @param \Illuminate\Contracts\Events\Dispatcher $events
     */
    public function __construct(SessionManager $session, Dispatcher $events)
    {
        $this->session = $session;
        $this->events = $events;

        $this->instance('default');
    }

    public function instance($instance = null)
    {
        $instance = $instance ?: 'default';

        $this->instance = sprintf('%s.%s', 'cart', $instance);

        return $this;
    }


    public function currentInstance()
    {
        return str_replace('cart.', '', $this->instance);
    }

    public function add($id, $price,$name,$image)
    {

        if(Session::has('cart')){
        
            $cart = Session::get('cart');
         
            if(isset($cart[$id])){
                $cart[$id]['quantity'] ++;
            }else {
                $cart[$id]['price'] = $price; 
                $cart[$id]['quantity'] = 1;
                $cart[$id]['name'] = $name;
                $cart[$id]['image'] = $image;
            }

            Session::put('cart',$cart);  
        }else{
 
            Session::put('cart',[ $id => 
                [
                    'price' => $price, 
                    'quantity' => 1,
                    'name' => $name,
                    'image' => $image
                ]
            ]);
        }

        return $id;
    }

    protected function getContent()
    {
        $content = $this->session->has('cart')
            ? $this->session->get('cart')
            : new Collection;

        return $content;
    }

    public function content()
    {
        if (is_null($this->session->get('cart'))) {
        
            return new Collection([]);
        }

        return $this->session->get('cart');
    }

    public function get($rowId)
    {
        $content = $this->getContent();

        if ( ! $content->has($rowId))
            throw new InvalidRowIDException("The cart does not contain rowId {$rowId}.");

        return $content->get($rowId);
    }

    public function subTotal(){
        $price = 0;
        if(!Session::has('cart')){ 
            return $price;
        }
        
        foreach(Session::get('cart') as $id => $value) {
            $price += $value['quantity']*$value['price'];
        }

        return $price;
    }

    public function isEmpty(){
        if(!Session::has('cart')){ 
            return true;
        }

        if (count(Session::get('cart')) <= 0 ){
            return true;
        }
        return false;
    }

    public function destroy()
    {
        Session::pull('cart');
    }

    public function remove($id)
    {
        $cart = Session::pull('cart',[]);

        if(isset($cart[$id])) {
            unset($cart[$id]);
        }

        session()->put('cart', $cart);
    }

    public function update($id,$qty) {
        $cart = Session::get('cart');
        if ($cart) {
            if (isset($cart[$id])){
                $cart[$id]['quantity'] = $qty;
            }
        }
        Session::put('cart',$cart);
    }

    public function total(){
        $total = 0;
        foreach(Cart::content() as $id => $value) {
            $total += $value['quantity'];
        }
        return $total;
    }

}
