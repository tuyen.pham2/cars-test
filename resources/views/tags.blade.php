@extends('layouts.app')

@section('content')
<section class="section-main bg padding-top-sm">
    <!-- ========================= SECTION CONTENT ========================= -->
    <section class="section-content padding-y-sm bg">
        <div class="container">

            <header class="section-heading heading-line">
                <h4 class="title-section bg">{{$tag->name}}</h4>
            </header>
            <div class="row">
                @foreach($cars as $car)
                    @include('partials.car')
                @endforeach            
            </div>
        
        </div>
        <!-- container .//  -->
    </section>

@endsection