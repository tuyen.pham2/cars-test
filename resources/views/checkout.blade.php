@extends('layouts.app')

@section('content')
<section class="section-pagetop bg-dark">
        <div class="container clearfix">
            <h2 class="title-page">Checkout</h2>
        </div>
        <!-- container //  -->
    </section>
    <!-- ========================= SECTION INTRO END// ========================= -->
    <section class="section-content bg padding-y">
        <div class="container">
            @if (\Session::has('error'))
                <div class="alert alert-success">
                    <ul>
                        <li>{!! \Session::get('error') !!}</li>
                    </ul>
                </div>
            @endif
            <form method="POST" action="{{route('checkout.post')}}" id="checkout-process">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <header class="card-header">
                            <h4 class="card-title mt-2">Billing Details</h4>
                        </header>
                        <article class="card-body">
                            
                                 {{ csrf_field() }}
                                <div class="form-row">
                                  
                                    <div class="col form-group">
                                        <label>First name</label>
                                        <input type="text" class="form-control" placeholder="First Name" id="first_name" name="first_name" value="{{ $currentUser->name ?? old('first_name') }}">
                                    </div>
                                    
                                    <!-- form-group end.// -->
                                    <div class="col form-group">
                                        <label>Last name</label>
                                        <input type="text" class="form-control" placeholder="Last Name"  id="last_name" name="last_name" value="{{ $currentUser->name ?? old('first_name') }}">
                                    </div>
                                    <!-- form-group end.// -->
                                </div>
                                <!-- form-row end.// -->
                                <div class="form-group">
                                    <label>Address</label>
                                    <input type="text" class="form-control" placeholder="Address" name="address" id="address"  value="{{ $currentUser->address ?? old('address') }}">
                                </div>
                                <!-- form-group end.// -->
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>City</label>
                                        <select id="City" class="form-control" id="city" name="city">
                                            @foreach($cities as $city)
                                                <option value="{{$city}}"  {{ ($currentUser && $currentUser->city == $city) ? 'selected' : '' }}>{{$city}}</option>
                                            @endforeach
                                        </select>
                                        <!-- <input type="text" class="form-control"> -->
                                    </div>
                                    <!-- form-group end.// -->
                                    <div class="form-group col-md-6">
                                        <label>District</label>
                                        <select id="inputState" class="form-control"  id="district" name="district">
                        
                                            @foreach($districts as $district)
                                                <option value="{{$district}}" {{ ($currentUser && $currentUser->district == $district) ? 'selected' : '' }}>{{$district}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <!-- form-group end.// -->
                                </div>
                                
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <input type="text" class="form-control" placeholder="Phone Number" name="phone" id="phone"  value="{{ $currentUser->phone ?? old('phone') }}">
                                </div>
                                <!-- form-group end.// -->
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <input type="email" class="form-control" placeholder="Email" name="email" id="email" value="{{ $currentUser->email ?? old('email') }}">
                                    <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                                </div>
                                <!-- form-group end.// -->
                                <div class="form-group">
                                    <label>Order Notes</label>
                                    <textarea class="form-control" name="notes" rows="6" id="notes">{{ old('notes') }}</textarea>
                                </div>
                                <!-- form-group end.// -->
                              
                        </article>
                    </div>
                    <!-- card.// -->
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <header class="card-header">
                                    <h4 class="card-title mt-2">Your Order</h4>
                                </header>
                                <article class="card-body">
                                
                                    @foreach(Cart::content() as $item)
                                        @include('partials.mini-item')
                                    @endforeach

                                    <dl class="dlist-align">
                                        <dt>Total cost: </dt>
                                        <dd class="text-right h5 b"> USD{{ Cart::totalPrice() }} </dd>
                                    </dl>
                                </article>
                            </div>
                        </div>
                        <div class="col-md-12 mt-4">
                            <div class="card">
                                <header class="card-header">
                                    <h4 class="card-title mt-2">Shipment Type</h4>
                                </header>
                                <article class="card-body">
                                    <label class="form-check">
                                      <input class="form-check-input" type="radio" name="exampleRadio" value="">
                                      <span class="form-check-label">
                                        First hand items
                                      </span>
                                    </label>
                                    <label class="form-check">
                                      <input class="form-check-input" type="radio" name="exampleRadio" value="">
                                      <span class="form-check-label">
                                        Brand new items
                                      </span>
                                    </label>
                                    <label class="form-check">
                                      <input class="form-check-input" type="radio" name="exampleRadio" value="">
                                      <span class="form-check-label">
                                        Some other option
                                      </span>
                                    </label>
                                </article>
                            </div>
                        </div>
                        <div class="col-md-12 mt-4">
                            <div class="card">
                                <header class="card-header">
                                    <h4 class="card-title mt-2">Payment Method</h4>
                                </header>
                                <article class="card-body">
                                    @foreach($gates as $key => $gate)
                                    <label class="form-check">
                                      <input class="form-check-input" type="radio" name="payment_method" value="{{$key}}" checked="checked">
                                      <span class="form-check-label">
                                        {{$gate->label}}
                                      </span>
                                      
                                    </label>
                                    @endforeach
                        
                                </article>
                            </div>
                        </div>
                        <div class="col-md-12 mt-4">
                            <button class="subscribe place-order btn btn-success btn-lg btn-block" type="button">Place Order</button>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </section>
@endsection

@push('scripts')
<script type="text/javascript" src="/js/jquery.validate.min.js"></script>
<script>
    (function($){
        $('.place-order').on('click',function(){
            $('#checkout-process').submit();
        });

        $("#checkout-process").validate({
			rules: {
				first_name: "required",
				last_name: "required",
				address: {
					required: true,
					minlength: 2
				},
                phone: "required",
                eamil: "required",
			},
			messages: {
				first_name: "Required",
				last_name: "Required",
				address: {
					required: "Required",
					minlength: "Length required?"
				},
                phone: "Required",
                eamil: "Required",
			}
		});

        // ajax districts
        $('#City').on('change', function(e) {
            var city = $(this).val();
            $.ajax({
                type: 'GET',
                url: '/ajax/getdistrict',
                data: {
                    city: city
                },
                success: function success(response) {
            
                    $('#inputState').html('');
                    $.each(response,function(index,value){
                    
                        $('#inputState').append('<option value="'+value+'">'+value+'</option>');
                    })
                
                }
            });
        });


    })(jQuery);
</script>
@endpush