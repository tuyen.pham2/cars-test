@extends('layouts.app')

@section('content')
<section class="section-main bg padding-top-sm">
        <div class="container">
            <div class="row">
                @include('accounts.partials.sidebar')

                <div class="col-lg-9">
                    <!-- Dashboard -->
                    <div class="card">
                        <header class="card-header">
                            <h4 class="card-title mt-2">Order {{$order->order_id}}</h4> 
                        </header>
                        <article class="card-body">
                        @if ($order->status)
                        <p>Status: {{$order->status->title}}</p>
                        @endif
                            <!-- Information -->
                                <div class="row">

                                    @foreach($order->get_info() as $label => $info)
                                        <div class="col-lg-6">{{$label}}: {{$info}}</div>
                                    @endforeach
                                </div>
                             
                   
                        </article>
                        <header class="card-header">
                            <h4 class="card-title mt-2">Products</h4>
                        </header>
                        <article class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <td>Product</td>
                                    <td>qty</td>
                                    <td>Price</td>
                                </thead>
                                <tbody>
                                @foreach($order->items as $item)
                                    <tr>
                                        <td>{{$item->car->name}}</td>
                                        <td>{{$item->qty}}</td>
                                        <td>{{$item->unit_price}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <table class="float-right"><tbody>
                                <tr>
                                    <td>Total:</td>
                                    <td>{{$order->total_price}}</td>
                                </tr>
                            </tbody></table>
                        </div>
                        </article>
                    </div>

                </div>
            </div>
        </div>
</section> 
@endsection

