@extends('layouts.app')

@section('content')
<section class="section-main bg padding-top-sm">
        <div class="container">
            <div class="row">
                @include('accounts.partials.sidebar')

                <div class="col-lg-9">
                    <!-- Dashboard -->
                    <div class="card">
                        <header class="card-header">
                            <h4 class="card-title mt-2">Orders</h4>
                        </header>
                        <article class="card-body">
                        
                   
                            <table class="table table-hover shopping-cart-wrap">
                                <thead class="text-muted">
                                    <tr>
                                        <th scope="col" width="50">ID</th>
                                        <th scope="col" width="120">Phone</th>
                                        <th scope="col" width="120">Email</th>
                                        <th scope="col" width="120">Total</th>
                                        <!-- <th scope="col" width="120">Line Total</th> -->
                                        <th scope="col" width="120">Total Price</th>
                                        <th scope="col" width="120">Status</th>
                                        <th scope="col" width="120"></th>
                               
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($orders as $order)
                                    <tr>
                                        <td>
                                            <figure class="media">
                                                <div class="img-wrap">
                                                    
                                                </div>
                                                <figcaption class="media-body">
                                                    <h6 class="title text-truncate">{{$order->order_id}} </h6>
                                        
                                                </figcaption>
                                            </figure>
                                        </td>
                                        <td>
                                                {{$order->phone}}
                                        </td>
                                        <td>
                                                {{$order->email}}
                                        </td>
                                        <td>
                                                {{$order->total}}
                                        </td>
                                    
                                        <td>
                                            <div class="price-wrap">
                                                <var class="price">USD {{$order->total_price}}</var>   
                                            </div>
                                            <!-- price-wrap .// -->
                                        </td>
                                        <td>
                                            @if($order->status)
                                                {{$order->status->title}}
                                            @endif
                                        </td>
                                        <td><a href="{{route('account.orders.detail',$order->id)}}" class="btn btn-secondary">Detail</a></td>
                                  
                                    </tr>
                                    @endforeach
                                    
                                </tbody>
                            </table>
                        </article>
                    </div>

                </div>
            </div>
        </div>
</section> 
@endsection

