@extends('layouts.app')

@section('content')
<section class="section-main bg padding-top-sm">
        <div class="container">
            <div class="row">
                @include('accounts.partials.sidebar')

                <div class="col-lg-9">
                    <!-- Dashboard -->
                    <div class="card">
                        <header class="card-header">
                            <h4 class="card-title mt-2">Account detail</h4>
                        </header>
                        <article class="card-body">
                        <form method="POST" action="{{route('account.update')}}">
                          {!! csrf_field() !!}
                          <input type="hidden" name="redirect" value="{{$_GET['redirect'] ?? '' }}">
                  
                            <div id="AccountDetail">
                                <profile-template :userid="{{ $user->id }}"></profile-template>
                            </div>
                            <!-- form-group end.// -->
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block"> Update </button>
                            </div>
                            <!-- form-group// -->
                            <small class="text-muted">By clicking the 'Sign Up' button, you confirm that you accept our <br> Terms of use and Privacy Policy.</small>
                        </form>
                        </article>
                    </div>

                </div>
            </div>
        </div>
</section> 
@endsection

@push('scripts')
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.18/vue.min.js"></script> -->
<link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css">
<script src="{{asset('js/app.js')}}"></script>
@endpush