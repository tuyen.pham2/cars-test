<div class="col-lg-3">
    <div class="card">
        <header class="card-header">
            <h4 class="card-title mt-2">Sidebar</h4>
        </header>
        <article class="card-body">
            <ul class="list-group list-group-flush">
                <li class="list-group-item "><a href="{{route('account')}}">Dashboard</a></li>
                <li class="list-group-item "><a href="{{route('account.detail')}}">Detail</a></li>
                <li class="list-group-item"><a href="{{route('account.orders')}}">Orders</a></li>
                <li class="list-group-item">
                    <form action="{{ route('logout_frontend') }}" method="POST" role="form" style="min-width:auto">
                        {{ csrf_field() }}
                        <button style="padding: 0;cursor: pointer;color: #00a7ff;border:0;background:none" type="submit" style="border:0;background:0;"> Logout</button>
                    </form>
                </li>
            </ul>
        </article>
    </div>
    <!-- Sidebar -->
</div>