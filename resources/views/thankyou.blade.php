@extends('layouts.app')

@section('content')
<section class="section-pagetop bg-dark">
        <div class="container clearfix">
            <h2 class="title-page">Login</h2>
        </div> <!-- container //  -->
    </section>
    <!-- ========================= SECTION INTRO END// ========================= -->
    <!-- ========================= SECTION CONTENT END// ========================= -->
    <section class="section-content bg padding-y">
        <div class="container">
            <h2>THANK YOU</h2>
            <p><a href="{{route('home')}}">Back to homepage</a></p>
        </div>
    </section>  
@endsection
