<div class="col-md-4">
    <figure class="card card-product">
        <a href="{{route('cars.show',$car->slug)}}">
        <?php
            $image = '/images/no-image.png';
            if ($car->image_url) {
                $image = '/uploads/car/' . $car->image_url->name;
            }
        ?>
        
        <div class="img-wrap"><img src="{{$image}}"></div>
        </a>
        <figcaption class="info-wrap">
            <h4 class="title"><a href="{{route('cars.show',$car->slug)}}">{{$car->name}}</a></h4>
            <p class="desc">Some small description goes here</p>
            <div class="rating-wrap">
                <!-- <ul class="rating-stars">
                    <li style="width:80%" class="stars-active">
                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                    </li>
                    <li>
                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                    </li>
                </ul> -->
                <div class="label-rating">Engine size: {{$car->engine_size}}</div>
                <!-- <div class="label-rating">154 orders </div> -->
            </div>
            <!-- rating-wrap.// -->
        </figcaption>
        <div class="bottom-wrap">
            <form method="POST" action="{{route('add_to_cart',$car->id)}}">
            {!! csrf_field() !!}
            <button name="submit" value="normal" class="btn btn-sm btn-primary float-right">Add To Cart</button>
            </form>
            <div class="price-wrap h5">
                <span class="price-new">${{ $car->price }}</span> 
                <!-- <del class="price-old">$1980</del> -->
            </div>
            <!-- price-wrap.// -->
        </div>
        <!-- bottom-wrap.// -->
    </figure>
</div>