<tr>
    <td>
        <figure class="media">
            <div class="img-wrap">
              
                <img src="{{$item['image']}}" class="img-thumbnail img-sm">
            </div>
            <figcaption class="media-body">
                <h6 class="title text-truncate">{{$item['name']}}</h6>
                @if (isset($item['group_detail']['attributes']))
                    @foreach($item['group_detail']['attributes'] as $attribute)
                    <dl class="dlist-inline small">
                    
                        <dt>{{$attribute['attribute']['name']}}: </dt>
                        <dd>{{$attribute['attribute_value']['name']}}</dd>
                    </dl>
                    @endforeach
                @endif
                <!-- <dl class="dlist-inline small">
                    <dt>Size: </dt>
                    <dd>XXL</dd>
                </dl>
                <dl class="dlist-inline small">
                    <dt>Color: </dt>
                    <dd>Orange color</dd>
                </dl> -->
            </figcaption>
        </figure>
    </td>
    <td>
        <input type="number" name="product[{{$item['cartname_id']}}]" class="form-control" value="{{$item['quantity'] ?? $item['qty']}}">

    </td>
    <td>
        <div class="price-wrap">
            <var class="price">USD {{$item['price']}}</var>
            <small class="text-muted">(USD5 each)</small>
        </div>
        <!-- price-wrap .// -->
    </td>

    <td class="text-right">
        <!-- <a data-original-title="Save to Wishlist" title="" href="" class="btn btn-outline-success" data-toggle="tooltip"> <i class="fa fa-heart"></i></a> -->
        <a href="{{route('cart.items.remove',$item['cartname_id'])}}" class="btn btn-outline-danger"> × Remove</a>
    </td>
</tr>