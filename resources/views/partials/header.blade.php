<header class="section-header">
        <section class="header-main">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-3">
                        <div class="brand-wrap">
                            <a href="/">
                                <img class="logo" src="/cars/images/logo-dark.png">
                                <h2 class="logo-text">LOGO</h2>
                            </a>
                        </div>
                        <!-- brand-wrap.// -->
                    </div>
                    <div class="col-lg-6 col-sm-6">
                        <form action="{{route('search')}}" method="GET" class="search-wrap">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search" name="keyword" value="{{$_GET['keyword'] ?? ''}}">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!-- search-wrap .end// -->
                    </div>
                    <!-- col.// -->
                    <div class="col-lg-3 col-sm-6">
                        <div class="widgets-wrap d-flex justify-content-end">
                            @include('partials.minicart')
                            <!-- widget .// -->
                            <div class="widget-header dropdown">
                                <a href="#" class="ml-3 icontext" data-toggle="dropdown" data-offset="20,10">
                                    <div class="icon-wrap icon-xs bg2 round text-secondary"><i class="fa fa-user"></i></div>
                                    <div class="text-wrap">
                                        <small>Hello.@if ($currentUser) {{ $currentUser->name }} @endif</small>
                                        @if (!$currentUser)
                                    
                                        <span>Login <i class="fa fa-caret-down"></i></span>
                                  
                                        @else
                            
                                            <button class="" type="submit" style="border:0;background:0;"> Menu <i class="fa fa-caret-down"></i></button>
                                  
                                        @endif
                                    </div>
                                </a>
                                @if (!$currentUser)
                                <div class="dropdown-menu dropdown-menu-right">
                                    <form class="px-4 py-3" action="{{ route('login_frontend.post') }}" method="POST">
                                    {{ csrf_field() }}
                                        <div class="form-group">
                                            <label>Email address</label>
                                            <input type="email" name="email" class="form-control" placeholder="email@example.com">
                                        </div>
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input type="password" name="password" class="form-control" placeholder="Password">
                                        </div>
                                        <button type="submit" class="btn btn-primary">Sign in</button>
                                    </form>
                                    <hr class="dropdown-divider">
                                    <a class="dropdown-item" href="{{route('register')}}">Have account? Sign up</a>
                                    <!-- <a class="dropdown-item" href="#">Forgot password?</a> -->
                                </div>
                                <!--  dropdown-menu .// -->
                                @else
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="{{route('account')}}">Dashboard</a>
                                    <form action="{{ route('logout_frontend') }}" method="POST" role="form" style="min-width:auto">
                                        {{ csrf_field() }}
                                        <button class="dropdown-item" type="submit" style="border:0;background:0;"> Logout <i class="fa fa-caret-down"></i></button>
                                    </form>
                                    
                                </div>
                                @endif
                            </div>
                            <!-- widget  dropdown.// -->
                        </div>
                        <!-- widgets-wrap.// -->
                    </div>
                    <!-- col.// -->
                </div>
                <!-- row.// -->
            </div>
            <!-- container.// -->
        </section>
        <!-- header-main .// -->

        <nav class="navbar navbar-expand-lg navbar-dark bg-secondary">
            <div class="container">

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav" aria-controls="main_nav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="main_nav">
                    <ul class="navbar-nav">
                        @foreach($menu as $m)
                            <li class="nav-item">
                                <a class="nav-link pl-0" href="{{route('tags', $m->slug)}}"> <strong>{{$m->name}}</strong></a>
                            </li>
                        @endforeach

                    </ul>
                </div>
                <!-- collapse .// -->
            </div>
            <!-- container .// -->
        </nav>

    </header>
    <!-- section-header.// -->
