<div id="PleaseLogin" class="modalshow modal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <form action="{{ route('login',['to'=>'checkout']) }}" method="POST" role="form" id="form_login" class="ng-pristine ng-valid">
        {{ csrf_field() }}
        <div class="top_form">
          <div class="row">
            <div class="form-group col-md-12">
              <label for="mst" class="active">@lang('user.email_number') <span class="error-require">(*)</span></label>
              <input type="text" class="form-control" id="login" name="login" value="" placeholder="@lang('user.nhap_email_so_the')">
            </div> 
            <div class="clearfix"></div>
            <div class="form-group col-md-12">
              <label for="mst" class="active">@lang('user.password') <span class="error-require">(*)</span></label>
              <input id="password" type="password" name="password" class="form-control" placeholder="@lang('user.nhap_mat_khau')">
            </div>
            <div class="form-group col-md-12">@lang('user.forgot_pass_click')<a href="{{url('password/reset')}}"><strong>@lang('user.here')</strong></a></div>
          </div>
          <div class="clearfix"></div>
          <button type="submit" class="btn btn-lg btn-custom login-button" style="margin:0">@lang('user.login')</button>

          <div class="clearfix"></div>
          <div class="row w426" style="max-width:400px;display:inline-block;margin-top:20px"><p>@lang('user.no_account')<a href="{{url('register')}}" style="color: #0d713d">@lang('user.register')</a> @lang('user.no_account_2')</p></div>
      </div>  
      </form>
        
      </div>
    </div>

  </div>
</div>
