<dl class="dlist-align">
    <dt>Parameter: </dt>
    <dd class="text-right">{{$item['name']}} x {{$item['quantity'] ?? $item['qty']}} </dd>
</dl>

@if (isset($item['group_detail']['attributes']))
    @foreach($item['group_detail']['attributes'] as $attribute)
        <dl class="dlist-align">
            <dt>{{$attribute['attribute']['name']}}: </dt>
            <dd class="text-right">{{$attribute['attribute_value']['name']}}</dd>
        </dl>
    @endforeach
@endif