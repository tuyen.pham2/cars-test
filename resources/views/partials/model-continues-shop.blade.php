<div id="PleaseLogin" class="modalshow modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <p>Bạn chưa có tài khoản? <a href="{{url('register')}}" style="color: #0d713d">Đăng ký</a> ngay để tận hưởng những dịch vụ tốt hơn từ chúng tôi.</p>
      </div>
    </div>

  </div>
</div>