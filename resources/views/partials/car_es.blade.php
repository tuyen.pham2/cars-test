<div class="col-md-4">
    <figure class="card card-product">
        <?php 
            $image = '/images/no-image.png';
            if ($car['_source']['image'] != '') {
                if ($car->image_url) {
                    $image = '/uploads/car/'.$car->image_url->name;
                }
            }
            
        ?>
        <div class="img-wrap"><img src="{{$image}}"></div>
        <figcaption class="info-wrap">
            <h4 class="title">{{ $car['_source']['name']}}</h4>
            <p class="desc">Some small description goes here</p>
            <div class="rating-wrap">
                <!-- <ul class="rating-stars">
                    <li style="width:80%" class="stars-active">
                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                    </li>
                    <li>
                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                    </li>
                </ul> -->
                <div class="label-rating">Engine Size: {{$car['_source']['engine_size']}}</div>
                <!-- <div class="label-rating">154 orders </div> -->
            </div>
            <!-- rating-wrap.// -->
        </figcaption>
        <div class="bottom-wrap">
            <a href="{{route('add_to_cart',$car['_source']['id'])}}" class="btn btn-sm btn-primary float-right">Add To Cart</a>
            <div class="price-wrap h5">
                <span class="price-new">${{ $car['_source']['price'] }}</span> 
                <!-- <del class="price-old">$1980</del> -->
            </div>
            <!-- price-wrap.// -->
        </div>
        <!-- bottom-wrap.// -->
    </figure>
</div>