<input type="checkbox" name="btn-toggle" id="btn-toggle" class="hidden">

<header class="header-pl background-primary">
    <div class="h_top">
        <div class="container">
            <div class="clearfix">
                <div class="h_delivered">
            
                </div>
                <div class="h_logo">

                    @include('site.logo')
                </div>
        
                <div class="h_right">
                    @if(!Route::is('checkout'))
                       @include('site.order.minicart',['cart' => $cart])
                    @endif
                    <div class="visible-lg visible-md hidden-sm hidden-xs">
                        <div class="lang-wrapper">
   
                        </div>
                        @include('auth.accountmenu')
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="h_menu">
        <div class="container">
            <button class="h_menu__btn">
                <span>-</span>
                <span>-</span>
                <span>-</span>
            </button>
            <span class="h_menu__overlay"></span>

            <div class="h_menu__box">
                <ul class="h_menu__main">
                    <li class=""><a href="{{ route('home') }}"  >@lang('menu.page.home')</a></li>
          
                    <li id="ca-phe" class="has-width arrow_bottom">
                     <a href="" class="">@lang('menu.coffee')</a>
                        <div class="h_menu__sub">
                            <ul class=" level1">
                                <li sryle="width: 30%;">
                                    <ul class="list-multi level2 none-fl">
                                        <li><a href="">@lang('menu.hanh_trinh_tach_ca_phe_dam_vi')</a></li>
                                        <li><a  href="">@lang('menu.hat_ca_phe_phuc_long')</a></li>
                                         <li><a href="">@lang('menu.nghe_thuat_pha_che')</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div> 
                    </li>
                

              
                </ul>
                <div class="hidden-lg hidden-md  visible-sm visible-xs">
                    @include('auth.accountmenu')
                </div>
           
            </div>
        </div>
    </div>
</header>
<style type="text/css">
    .menu-membership .list-multi li {
        width: 100%;
    }
    .has-width {position: relative;}
    .h_menu .container .h_menu__main > li.has-width .h_menu__sub {width: 250px}
    .h_menu .container .h_menu__main > li.has-width .h_menu__sub>ul>li {width: 100%!important}
    .h_menu .container .h_menu__main > li.has-width .h_menu__sub .list-multi > li {width: 100%}
    /*.h_menu .container .h_menu__main > li#the-menu .h_menu__sub {width: 66.667%;left: auto;right: 0}*/
    .inactiveLink{pointer-events: none;cursor: default;}
</style>
