@extends('layouts.app')

@section('content')
	<div class="main-full">

		<?php
		$currentGroup = ($group->children()->first()!= null)? $group->children()->first() : $group;
		?>

		<section id="breadcrumbs-wrapper" style='min-height:100px;background-image: url("{{ $parent->getImage('full') }}")'>
			<!-- <div class="container">
				<ul class="breadcrumb" aria-label="breadcrumbs">
					<li><a href="{{ route('home') }}">@lang('menu.page.home')</a></li>
					<li class="active"><a href="#">@lang('page.product.drink')</a></li>
				</ul>

				<h1>@lang('page.product.menu')</h1>
				<div class="separator-icon"></div>

				<ul class="menu-category-horizontal">
					@foreach ($groups as $main_group)
						<li class="{{ $main_group->slug === $currentGroup->slug ? ' active' : '' }}">
							<a href="{{ url('category/'.$main_group->slug )}}"
							   class="dish text-uppercase">{{ $main_group->title }}</a>
						</li>
					@endforeach
				</ul>
			</div> -->
		</section>
        <section class="page-heading">
            <div class="container">
                <ul class="breadcrumb" aria-label="breadcrumbs">
                    <li><a href="{{ route('home') }}">@lang('menu.page.home')</a></li>
                    <li class="active"><a href="#">@lang('page.product.drink')</a></li>
                </ul>
                <h1>{{$group->title}}</h1>
                <div class="separator-icon gray"></div>    
                @if (count($parentDrinks) > 0)
                <ul class="menu">
                    @foreach($parentDrinks as $myParentDrink)
                        <li class="active" ><a style="border-top:0" href="{{$myParentDrink->getUrl()}}">{{$myParentDrink->title}}</a></li>
                    @endforeach
                </ul>      
                @endif              
            </div>
        </section>
		{{--@include('site.category.toolbar',['groups' => $groups])--}}
			@include('site.category.toolbar',['groups' => $groups,'value'=>$value,'hideCatSearch'=>$hideCatSearch])
		<?php $group = $currentGroup ?>

		<section class="mobile-order-dishes category-drink">
			@foreach ($group->dishes as $dish)
				<div class="mobile-dish">
					@if($dish->bestseller)
						<span class="badge best">@lang('admin.field.bestseller')</span>
					@endif
					@if ($dish->is_new)
						<span class="badge new">@lang('admin.field.new')</span>
					@endif
					@if ($dish->discount > 0)
						<i class="item-discount">-{{ $dish->discount }}%</i>
					@endif
					<div class="dish-img">
						<img src="{{ $dish->getImage('full') }}" alt="{{ $dish->title }}" class="img-responsive center-block">
					</div>
					<div class="dish-info">
						<h4 class="text-uppercase text-center">{{ $dish->title }}</h4>
						<p class="dish-price text-center">{{ format_money($dish->price) }}</p>
					</div>

						<button class="btn add-to-cart btn-default" data-id="{{ $dish->id }}" data-price="{{$dish->price}}">@lang('user.button.add to cart')</button>
						@if ($combo = $dish->getCombos())
							<div class="hidden combo-info">
								@foreach ($sides as $s)
									@if (in_array($s->id, $combo) )
										<div class="combo-item" data-title="{{ $s->title }}"
											 data-img="{{ $s->getImage('full') }}"
											 data-price="{{$s->price}}"
											 data-extraid="{{ $s->id }}"></div>
									@endif
								@endforeach
							</div>
						@endif


				</div>
			@endforeach
		</section>

		<!-- <section class="mobile-order-purchase"  ng-controller="CartController">
			<a href="{{ action('CartController@checkout') }}" class="btn btn-default" role="button">
				<i class="icon icon-pli-shopping-cart"></i>  <span class="number">@{{ total().count }}</span>   @lang('user.button.checkout')</a>
			<span class="total"></span>
			<div class="clearfix"></div>
		</section> -->
	</div>
@endsection

@push('scripts')
	<!-- <script type="text/javascript" src="/js/order/mobile.js"></script> -->
@endpush

