<div class="@if($dish->sticky == 1 && $first ) sticky col-sm-6 col-xs-12 @else col-sm-3 col-xs-6 @endif product-item ">
    <a class="item-wrapper">
        @if($dish->bestseller)
            <span class="badge best">@lang('admin.field.bestseller')</span>
        @endif
        @if ($dish->is_new)
            <span class="badge new">@lang('admin.field.new')</span>
        @endif
        @if ($dish->discount > 0)
            <i class="item-discount">-{{ $dish->discount }}%</i>
        @endif
        <img class="item-img img-responsive center-block lazy" data-original="{{ timthumb($dish->getImage('full'),300,300,80) }}" alt="{{ $dish->title }}">

        <div class="item-info">
            <div class="item-name">{{ $dish->title }}</div>

            <div class="item-desc">
            @if($dish->sticky == 1)
               {!! str_limit($dish->excerpt, 80, ' ...') !!}
                @else
                {!! str_limit($dish->excerpt,80, '...') !!}
            @endif
            </div>
            <div class="item-price">{{ format_money($dish->price) }}</div>
            @if($dish->hide_addtocart == 1)
                <button class="btn btn-default">@lang('user.button.lien_he')</button>
            @else
                <button class="btn btn-default add-to-cart" data-id="{{ $dish->id }}"
                    data-name="{{ $dish->title }}"
                    data-price="{{ $dish->price }}">@lang('user.button.add to cart')</button>
            @endif
            @if ($combo = $dish->getCombos())
                <div class="hidden combo-info">

                    @foreach ($sides as $s)

                        @if (in_array($s->id, $combo) )

                            <div class="combo-item" data-title="{{ $s->title }}"
                                 data-img="{{ $s->getImage('full') }}"
                                 data-price="{{$s->price}}"
                                 data-extraid="{{ $s->id }}"></div>
                        @endif
                    @endforeach

                </div>
            @endif
        </div>
    </a>
</div>

