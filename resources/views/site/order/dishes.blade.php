<div class="grid-item category-drink">
	<div class="container">
		@if(count($dishes)>0)
			<div class="row">
				@if($sticky != null && !isset($value['hideSticky']))
					@include('site.order.dish', ['dish' => $sticky, 'first' => 1])
				@endif

				@foreach ($dishes as $dish)
					@if( !isset($sticky) || ($dish->id != $sticky->id) )
						@include('site.order.dish', ['dish' => $dish, 'first' => 0])
					@endif
				@endforeach
			</div>

		@endif
	</div>
</div>
