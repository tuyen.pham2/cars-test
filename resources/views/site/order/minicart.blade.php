
<div class="h_cart visible-lg visible-md hidden-sm hidden-xs" ng-controller="CartController">
<div class="cart-icon btn">Cart</div>
<div id="mini-cart" class="mini-cart" >
    <div class="mini-cart__content">
        <div class="minicart">
        <div id="success-message" class="success-message hidden" style="padding:10px 20px; font-weight: 700">
            <i class="icon-tick color-primary" ></i>&nbsp;  Successfully added to cart
        </div>
        <div class="total">
            Subtotal : {!!
            $cart->subTotal() !!}
        </div>
        <h4 class="title-cart hidden-xs">My Cart</h4>
        <ul class="list-mini-product">
            @if ($cart->isEmpty())
            <li>
                <i class="icon icon-cart" aria-hidden="true"></i>
                <h5 class="empty-cart">Your Cart is empty</h5>
            </li>
            @else
   
            @foreach ($cart->content() as $id => $cartItem)
        
            <li class="item mini-cart-details ">
                <div class="row grid-space-20">
                    <div class="col-7">
                        <a href=""
                            class="title">{{ 'test' }}</a>
                        <div class="number">{{ $cartItem['quantity'] }} *</div>
                        <div class="price"> {!! $cartItem['price'] !!}</div>
                    </div>
                    <form method="POST" action="{{ route('cart.items.remove', $id) }}"
                        onsubmit="return confirm('Are you sure?');">
                        {{ csrf_field() }}
                        {{ method_field('delete') }}

                        <button type="submit" class="btn-close" data-toggle="tooltip"
                            data-placement="right'"
                            title="Remove Cart">
                            Remove
                        </button>
                    </form>
                </div>
            </li>
            @endforeach
            @endif
        </ul>
        @unless ($cart->isEmpty())
        <div class="groupbtn">
            <p><a href="{{ route('cart.index') }}"
                    class="btn btn1 noshadow">Cart</a></p>
            <p><a href="{{ route('home') }}" class="btn btn1 noshadow hidden-xs">
                    Continue shopping</a></p>
        </div>
        @endunless
    </div>

    </div>
</div>
</div>