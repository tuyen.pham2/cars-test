@extends('layouts.app')

@section('meta')
	<title>{{ $group->title }} - Phúc Long Coffee & Tea House</title>
    <meta name="description" content="{{ $group->description }}">
    <meta property="og:title" content="{{ $group->title }}" />
    <meta property="og:image" content="{{ $group->getImage('full') }}" />
@endsection

@section('content')

	<div class="main-full">

		<?php $currentGroup = ($group->children()->first()!= null)? $group->children()->orderBy('updated_at','desc')->first() : $group;		?>

{{-- 		<section id="breadcrumbs-wrapper" style='background-image: url("{{ $parent->getImage('full') }}")'>
			<div class="container">
				<ul class="breadcrumb" aria-label="breadcrumbs">
					<li><a href="{{ route('home') }}">@lang('menu.page.home')</a></li>
					<li class="active"><a href="#">{{$group->title}}</a></li>
				</ul>

				<h1>@lang('page.product.menu')</h1>
				<div class="separator-icon"></div>

				<ul class="menu-category-horizontal" >
					@foreach ($groups as $main_group)
						<li class="{{ $main_group->slug === $currentGroup->slug  && $group->slug != 'thuc-uong' ? ' active' : '' }}">
							<!-- <a href="{{ url('category/'.$main_group->slug )}}" class="dish text-uppercase">{{ $main_group->title }}</a> -->
							<a href="{{ action('CategoryController@show',$main_group->slug )}}" class="dish text-uppercase">{{ $main_group->title }}</a>
						</li>
					@endforeach
				</ul>
			</div>
		</section> --}}

 		<section id="page-banner">
            <div class="thumbCover_30">
                <img class="lazy lazy-loaded" data-lazy-type="image" data-lazy-src="{{ $parent->resource_id !== NULL ? $parent->getImage('full') : asset('/images/background/bg_menu.jpg') }}" alt="" 
                src="{{ $parent->resource_id !== NULL ? $parent->getImage('full') : asset('/images/background/bg_menu.jpg') }}">
            </div>
        </section>

        <section class="page-heading">
            <div class="container">
                <ul class="breadcrumb" aria-label="breadcrumbs">
                    <li><a href="{{ route('home') }}">@lang('menu.page.home')</a></li>
                    <li class="active"><a href="#">{{$group->title}}</a></li>
                </ul>
                <h1>{{$group->title}}</h1>
                <div class="separator-icon gray"></div>    
                @if (count($parentDrinks) > 0)
                <ul class="menu">
                    @foreach($parentDrinks as $myParentDrink)
                        <li class="<?php if($myParentDrink->slug == $slug){echo 'active';}?>">
                        	<!-- <a href="{{$myParentDrink->getUrl()}}">{{$myParentDrink->title}}</a> -->
                        	<a href="{{ action('CategoryController@show',$myParentDrink->slug )}}" class="dish text-uppercase">{{ $myParentDrink->title }}</a>
                        </li>
                        				
                    @endforeach
                </ul>      
                @endif              
            </div>
        </section>
		@include('site.category.toolbar',['groups' => $groups,'value'=>$value,'hideCatSearch'=>$hideCatSearch])
		@include('site.order.dishes', ['dishes' => $dishes])
		{{--@include('site.order.category-drink', ['group' => $group])--}}
		<div class="item-menu-right">

		</div>
	</div>
@endsection
@push('scripts')
<script type="text/javascript">
    $("#thuc-uong").addClass('active');
</script>
@endpush
@push('styles')
<style>
	.category-toolbar{border:0;}
	.category-toolbar > .container{border:1px solid #DCDCDC;}
	.category-toolbar .row > form > div:nth-child(3){border-right:0;}
	.page-heading ul.menu li:hover a{color: #0C713D;border-color: #0C713D;}
	.grid-item .product-item > .item-wrapper .item-price{font-weight: 900;color: #0C713D !important;font-size: 18px;font-family: Tahoma;}
</style>
@endpush