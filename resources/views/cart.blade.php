@push('styles')
<style>
    .cart table,.cart table th ,.cart table td {
        margin-top: 40px;
        width:100%;
        border:1px solid #ddd;
        border-collapse: collapse;
    }
    .cart a.btn {
        border:1px solid #ddd;
        background: #ddd;
    }
</style>
@endpush

@push('scripts')
<script>
    (function($){
        $('.plus').on('click', function(e){
            e.preventDefault();
            var parent = $(this).parent();
            var value_qty = parent.find('input').val();
            value_qty++;
            parent.find('input').val(value_qty);
        });
        $('.minus').on('click', function(e){
            e.preventDefault();
            var parent = $(this).parent();
            var value_qty = parent.find('input').val();
            value_qty--;
            if (value_qty<=1){
                value_qty = 1;
            }
            parent.find('input').val(value_qty);
        });
    })(jQuery)
</script>
@endpush
@extends('layouts.app')

@section('content')

<section class="section-pagetop bg-dark">
        <div class="container clearfix">
            <h2 class="title-page">Cart</h2>
        </div> <!-- container //  -->
    </section>
    <!-- ========================= SECTION INTRO END// ========================= -->
    <!-- ========================= SECTION CONTENT ========================= -->
    <section class="section-content bg padding-y border-top">
        <div class="container">
        <form method="POST" action="{{route('cart.update')}}">
            <div class="row">
                <div class="col-md-12">
                    @if(session('errors'))
                        <div class="alert alert-danger">{{session('errors')}}</div>
                    @endif
                    @if(session('success'))
                        <div class="alert alert-success">{{session('success')}}</div>
                    @endif
                </div>
                <main class="col-sm-9">
                {!! csrf_field() !!}
                    <div class="card">
                        <table class="table table-hover shopping-cart-wrap">
                            <thead class="text-muted">
                                <tr>
                                    <th scope="col">Product</th>
                                    <th scope="col" width="120">Quantity</th>
                                    <th scope="col" width="120">Price</th>
                                    <th scope="col" class="text-right" width="200">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                          
                            @if(count($cart)>0) 
                                @foreach($cart as $id => $item)
                                
                                    @include('partials.item')
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        
                    </div>
                    <!-- card.// -->
                    
                    <button style="float:right;margin-top:15px" class="btn btn-primary">Update Cart</button>
                </main>
                
                <!-- col.// -->
                <aside class="col-sm-3">
                    <p class="alert alert-success">Add USD 5.00 of eligible items to your order to qualify for FREE Shipping. </p>
                    <dl class="dlist-align">
                        <dt>Total price: </dt>
                        <dd class="text-right">USD {{Cart::subTotal()}}</dd>
                    </dl>
             
                    @if (count(Cart::coupons())>0)
                        @foreach(Cart::coupons() as $coupon)
                        <dl class="dlist-align">
                            <dt>Discount:</dt>
                            <dd class="text-right">{{ ($coupon['coupon_is_percent']) ? $coupon['coupon_value'].'%' : 'USD '.$coupon['coupon_value']}} ({{$coupon['coupon_code']}})</dd>
                        </dl>
                        @endforeach
                    
                    @endif
                    <dl class="dlist-align h4">
                        <dt>Total:</dt>
                        <dd class="text-right"><strong>USD {{Cart::totalPrice()}}</strong></dd>
                    </dl>
                    <hr>
                    <figure class="itemside mb-3">
                        <aside class="aside"><img src="/cars/images/icons/pay-visa.png"></aside>
                        <div class="text-wrap small text-muted">
                            Pay 84.78 AED ( Save 14.97 AED ) By using ADCB Cards
                        </div>
                    </figure>
                    <figure class="itemside mb-3">
                        <aside class="aside"> <img src="/cars/images/icons/pay-mastercard.png"> </aside>
                        <div class="text-wrap small text-muted">
                            Pay by MasterCard and Save 40%.
                            <br> Lorem ipsum dolor
                        </div>
                    </figure>
                    <a href="{{route('checkout')}}" class="btn btn-success btn-lg btn-block">Proceed To Checkout</a>
                </aside>
                <!-- col.// -->
            </div>
            </form>
            @if (Cart::hasCoupon())

            <p>Coupon: {{Cart::getCoupons()}}<p>
            @else
            <form method="POST" action="{{route('cart.coupon')}}">
                {!! csrf_field() !!}
                <div class="row">
                <div class="col-md-3">
                <input type="text" name="code" value="" placeholder=" Coupon code here" class="form-control">
                </div>
                <div class="col-md-3">
                <button class="btn btn-primary">Add Coupon</button>
                </div>
                </div>
            </form>
            @endif
        </div>
        <!-- container .//  -->
    </section>
@endsection

