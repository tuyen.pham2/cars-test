@extends('layouts.admin')

@section('content')
{!! Form::open(['route' => ['order_status.update', $entity->id], 'method' => 'PUT', 'id' => 'form-tag', 'class' => 'row']) !!}
    @include('admin.order_status.form')
{!! Form::close() !!}
@endsection
