@extends('layouts.admin')

@section('content')
<div class="app-title">
<div>
    <h1><i class="fa fa-th-list"></i> Cars</h1>
    <p>Table to display analytical data effectively</p>
</div>
@include('admin.partials.breadcrumb')
</div>
<a class="btn btn-primary" href="{{route('order_status.create')}}">Create</a>
<div class="row">
<div class="col-md-12">
    <div class="tile">
    <div class="tile-body">
        <table class="table table-hover table-bordered" id="sampleTable">
        <thead>
            <tr>
            <th>Name</th>
            <th>Key</th>
            <th></th>
            </tr>
        </thead>
        <tbody>
        @foreach ($entities as $entity)
            <tr>
    
            <td><a href="{{ route('order_status.edit',$entity->id) }}">{{ $entity->title }}</a></td>
            <td>{{ $entity->key }}</td>

            <td>
                <a class="waves-effect waves-light btn btn-sm btn-secondary" href="{{ route('order_status.edit',$entity->id) }}">Edit</a>
            </td>
            </tr>
        @endforeach
      
        </tbody>
        </table>
    </div>
    </div>
</div>
</div>

@endsection

@push('scripts')
<script type="text/javascript" src="/admin/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/admin/js/plugins/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">$('#sampleTable').DataTable();</script>
@endpush