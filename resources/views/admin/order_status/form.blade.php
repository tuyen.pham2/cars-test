@if(Session::has('response'))
<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('response')['message'] }}</p>
@endif
<div class="tile" style="width:100%;">
<div class="row">
    <div class="col-lg-9">
        <div class="form-group">
        <label for="Title">Title</label>
        <input class="form-control" id="Title" type="text" aria-describedby="emailHelp" placeholder="Enter Title" name="title" value="{{$entity->title}}">
        </div>

        <div class="form-group">
        <label for="Key">Key</label>
        <input class="form-control" id="Key" type="text" aria-describedby="emailHelp" placeholder="Enter Key" name="key" value="{{$entity->key}}">
        </div>
        <!-- VueJS -->
        <div class="tab-pane" id="app">
            <attribute-values :attributeid="{{ $entity->id }}"></attribute-values>
        </div>
    
    </div>
    <div class="col-lg-3">
       
        <div class="tile-footer">
              <button class="btn btn-primary" type="submit">Submit</button>
        </div>
    </div>
</div>
</div>
