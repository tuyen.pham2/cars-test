@extends('layouts.admin')

@section('content')
@if($type == 'post')
	<?php $action = 'Admin\PostController@store'?>
@else
	<?php $action = 'Admin\CoffeeController@store'?>
@endif

{!! Form::open(['action' => $action, 'method' => 'POST', 'id' => 'form-post', 'class' => 'row']) !!}
	{{ method_field('POST') }}
    @include('admin.post.form')
{!! Form::close() !!}
@endsection
