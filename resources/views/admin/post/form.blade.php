<div class="primary col s9">
    <h1 class="text-capitalize">
        @if ($post->id)
            @lang('admin.title.edit', ['object' => trans('admin.object.'.$type)])
            @if(isset($type))
                <a href="{{ action('Admin\CoffeeController@create',['type'=>$type]) }}" class="page-title-action btn waves-effect waves-light cyan">@lang('admin.title.create raw')</a>
            @else
                <a href="{{ action('Admin\PostController@create') }}" class="page-title-action btn waves-effect waves-light cyan">@lang('admin.title.create raw')</a>
            @endif
        @else
            @if(isset($type) && $type == 'coffee')
                @lang('admin.title.create_coffee')
            @elseif(isset($type) && $type == 'tea')
                @lang('admin.title.create_tea')
            @else
                @lang('admin.title.create', ['object' => trans('admin.object.post')])
            @endif          
        @endif
    </h1>
    <div class="input-field">
        @include('partials.lang_input', ['type' => 'text', 'model' => 'post', 'attr' => 'title', 'title' => trans('admin.field.title')])
    </div>
    <div class="input-field">
        <label class="active" for="slug">@lang('admin.field.create_time')</label>
        {{ Form::text('create_time', $post->create_time, ['id' => 'create_time', 'placeholder' => '']) }}
    </div>

    <div class="input-field">
        <label class="active" for="slug">@lang('admin.field.slug')</label>
        {{ Form::text('slug', $post->slug, ['id' => 'slug', 'placeholder' => '']) }}
    </div>

    <div class="input-field">
        <label class="active" for="priority_link">@lang('admin.field.priority_link')</label>
        {{ Form::text('priority_link', $post->priority_link, ['id' => 'priority_link', 'placeholder' => '']) }}
    </div>

    <div class="input-field">
        @include('partials.lang_input', ['type' => 'textarea', 'attr' => "description", 'model' => 'post', 'class' => 'description'])
    </div>
    <div class="input-field">
        @include('partials.lang_input', ['type' => 'textarea', 'attr' => "excerpt", 'model' => 'post', 'class' => 'materialize-textarea', 'title' => trans('admin.field.excerpt')])
    </div>
</div>
<div class="side col s3">
    <div class="cat-top card hoverable">
        <h1 class="text-capitalize">@lang('admin.button.publish')</h1>
        <div class="divider"></div>
        <button type="button" class="btn-preview btn btn-sm text-sm waves-effect waves-light cyan">@lang('admin.button.preview')</button>
        <div class="status">
            <p>@lang('admin.field.status'):
                {{ Form::radio('active', 1, $post->active, ['id' => 'status-publish', 'class' => 'with-gap']) }}<label for="status-publish">@lang('admin.status.publish')</label>
                {{ Form::radio('active', 0, !$post->active, ['id' => 'status-draft', 'class' => 'with-gap']) }}<label for="status-draft">@lang('admin.status.draft')</label>
            </p>
            <p>@lang('admin.field.sticky'): {{ Form::checkbox('sticky', 1, $post->sticky, ['id' => 'sticky']) }}<label for="sticky"><span class="non-visib">1</span></label></p>
            <p>@lang('admin.field.hotpromotion'): {{ Form::checkbox('hotpromotion', 1, $post->hotpromotion, ['id' => 'hotpromotion']) }}<label for="hotpromotion"><span class="non-visib">1</span></label></p>
            @if ($post->id)
            <p>@lang('admin.field.created at'): {{ $post->created_at }}</p>
            <p>@lang('admin.field.updated at'): {{ $post->updated_at }}</p>
            @endif
        </div>
        @if ($post->id)
            <button type="button" class="btn-delete btn btn-sm left waves-light waves-effect red darken-4">@lang('admin.button.delete')</button>
            <button type="submit" class="btn btn-sm right waves-light waves-effect green accent-4">@lang('admin.button.update')</button>
        @else
            <button type="submit" class="btn btn-sm waves-light waves-effect green accent-4">@lang('admin.button.publish')</button>
        @endif
        <div class="clearfix"></div>
    </div>
    @if(isset($settings) && !empty($settings))
    <div class="cat-middle card hoverable">
        <h1 class="text-capitalize">Loại tuyển dụng</h1>
        <div class="divider"></div>
        @if(count($settings)>0)
            @foreach($settings as $setting)
            <p>
                {{ Form::radio('recruitment_type', $setting, ($post->recruitment_type == $setting) ? true : false , ['id' => 'tuyendung-' . $loop->index]) }}
                <label for="tuyendung-{{ $loop->index }}">{{ $setting }}</label>
            </p>
            @endforeach
        @endif
    </div>
    @endif
    <div class="cat-middle card hoverable">
        <h1 class="text-capitalize">@lang('admin.field.category')</h1>
        <div class="divider"></div>
            @foreach ($categories as $category)
            <p>
                {{ Form::checkbox('categories[]', $category->id, in_array($category->id, $post->categories), ['id' => 'category-' . $loop->index]) }}
                <label for="category-{{ $loop->index }}">{{ $category->title }}</label>
            </p>
            @endforeach
    </div>
    <div class="cat-bottom card hoverable">
        <h1 class="text-capitalize">@lang('admin.field.image')</h1>
        <div class="divider"></div>
        <div class="postimagediv">
            <img src="{{ $post->image }}" class="responsive-img" alt="">
            <input type="hidden" name="resource_id" value="{{ $post->resource_id }}">
            <div class="clearfix"></div>
            <a class="btn waves-effect waves-light cyan {{ $post->resource_id ? 'hide' : '' }} set-image modal-trigger" href="#modal-upload">@lang('admin.button.set image')</a>
            <a class="btn waves-effect waves-light cyan {{ $post->resource_id ? '' : 'hide' }} remove-image">@lang('admin.button.remove image')</a>
        </div>
    </div>

    <div class="cat-bottom card hoverable">
        <h1 class="text-capitalize">Hình Banner nổi bật</h1>
        <div class="divider"></div>
        <div class="postimagediv2">
            <img src="{{$feature ? '/uploads/'.$type.'/'.$feature->name : ''}}" class="responsive-img" alt="">
            <input type="hidden" name="feature" value="{{ $post->feature }}">
            <div class="clearfix"></div>
            <a class="btn waves-effect waves-light cyan {{ $post->feature ? 'hide' : '' }} set-image modal-trigger" href="#modal-upload2">@lang('admin.button.set image')</a>
            <a class="btn waves-effect waves-light cyan {{ $post->feature ? '' : 'hide' }} remove-image">@lang('admin.button.remove image')</a>
        </div>
    </div>
</div>
<input type="hidden" name="type" value="@if($type=='coffee') 4 @elseif($type=='tea') 5 @else 1 @endif"/>

@push('scripts')
    <!-- <script type="text/javascript" src="/ckeditor/ckeditor.js"></script> -->
    <script type="text/javascript" src="/ckfinder/ckfinder.js"></script>
    <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var editoren = CKEDITOR.replace( 'CKeditoren',{
                toolbar : 'MyToolbar',
                filebrowserBrowseUrl: '{{URL::to('/')}}/ckfinder/ckfinder.html',
                filebrowserUploadUrl: '{{URL::to('/')}}/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'
            });
            CKFinder.setupCKEditor( editoren );
            var editorvn = CKEDITOR.replace( 'CKeditorvi',{
                toolbar : 'MyToolbar',
                filebrowserBrowseUrl: '{{URL::to('/')}}/ckfinder/ckfinder.html',
                filebrowserUploadUrl: '{{URL::to('/')}}/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'
            });
            CKFinder.setupCKEditor( editorvn );
            var $form = $('#form-post'),
                $form_method = $form.children('input[name="_method"]'),
                form_action = $form.attr('action'),
                form_method = $form_method.val();

            $form.validate({
                rules: {
                    'title[vi]': {
                        required: true,
                        minlength: 3
                    },
                    'title[en]': {
                        minlength: 3
                    },
                    slug: {
                        required: true,
                        minlength: 3,
                        pattern: /^[\w\-\/]+[a-zA-Z\d]$/
                    },
                    'categories[]': {
                        required: true,
                        minlength: 1
                    }
                },
                errorElement : 'div',
                errorPlacement: function(error, element) {
                    var placement = $(element).data('error');
                    if (placement) $(placement).append(error)
                    else if ($(element).attr('type') === 'checkbox') error.insertBefore(element);
                    else error.insertAfter(element);
                }
            })

            $('.btn-preview').click(function(event) {
                event.preventDefault();

                $form.attr({
                    action: '{{ action('Admin\PostController@preview') }}',
                    target: '{{ config('app.name') }}'
                });
                $form_method.val('POST');
                $form.submit();

                $form.attr({
                    action: form_action,
                    target: '_self'
                });
                $form_method.val(form_method);
            });
        });

    </script>
@endpush
@push('styles')
<style>
    .cke_browser_webkit input {height:auto;}
    .cke_dialog_image_url .cke_dialog_ui_hbox td.cke_dialog_ui_hbox_last{vertical-align:middle !important;}
    .cke_dialog_image_url .cke_dialog_ui_hbox td.cke_dialog_ui_hbox_last a{margin-top:10px !important;}
</style>
@endpush