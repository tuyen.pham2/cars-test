@extends('layouts.admin')

@push('styles')
    <style type="text/css">
        td > .post-category {
            display: block;
        }
    </style>
@endpush

@section('content')
<div class="row">
    <h1 class="col s12 head-2">
        @if($type=='coffee')
            @lang('admin.object.coffee')
        @elseif($type=='tea')
            @lang('admin.object.tea')
        @else
            @lang('admin.object.post')
        @endif
        @can('news-create',User::class)
            @if($type=='coffee')
            <a href="{{ action('Admin\CoffeeController@create') }}" class="page-title-action btn waves-effect waves-light cyan">@lang('admin.title.create raw')</a>
            @elseif($type=='tea')
            <a href="{{ action('Admin\CoffeeController@create',['type'=>$type]) }}" class="page-title-action btn waves-effect waves-light cyan">@lang('admin.title.create raw')</a>
            @else
                 <a href="{{ action('Admin\PostController@create') }}" class="page-title-action btn waves-effect waves-light cyan">@lang('admin.title.create raw')</a>
            @endif
        @endcan
       
       
         <a href="#" id="bulk_sort_order" class="page-title-action btn waves-effect waves-light cyan">@lang('admin.title.update_bulk_action')</a>
        <img src="/images/loading.gif" class="loading-gif">
    </h1>
    <div class="clearfix"></div>
    <div class="table-actions"></div>
        <div class="col s2">
            <div class="select-wrapper">
                {{ Form::select('category', $categories, Request::get('category', NULL), ['class' => 'category-filter browser-default', 'placeholder' => trans('admin.title.index', ['object' => trans('admin.object.category')])]) }}
            </div>
        </div>
        {{-- <button type="button" class="btn waves-effect waves-light cyan">Filter</button> --}}
    </div>
    <div class="clearfix"></div>
    <div class="col s12">
        <table class="bordered highlight posts datatable responsive-table display" cellspacing="0">
            <thead class="cyan white-text">
                <th class="no-sort no-padding"></th>
                <th class="no-sort" @if($type=='post') width="50" @endif></th>
                <th>@lang('admin.field.title')</th>
                <th>@lang('admin.field.draft')</th>
                <th>@lang('admin.field.sticky')</th>
                <th>@lang('admin.field.hotpromotion')</th>
                <th class="no-sort">@lang('admin.field.category')</th>
                <th class="no-sort">@lang('admin.field.author')</th>
                <th>@lang('admin.field.date')</th>
                <th class="no-sort"></th>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            var type = '{{$type}}';
            if(type != 'coffee' && type != 'tea'){
                var urltable = '{{ action('Admin\PostController@index',['type' => $type ]) }}';
            }else{
                var urltable = '{{ action('Admin\CoffeeController@index',['type' => $type ]) }}';
            }
            
            var table = $('table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": {
                    url: urltable,
                    data: function(d) {
                        d.category = $('.category-filter').val();
                    }
                },
                "columnDefs": [
                    { orderable: false, targets: 'no-sort' },
                    { className: 'no-padding', targets: 0 }
                ],
            });

            $('.category-filter').change(function(event) {
                table.ajax.reload();
            });
            $(document).on('click','#bulk_sort_order',function(){
                var arr = [];
                $('.sort-order').each(function(){
                    var val = $(this).val();
                    var id = $(this).data('id');
                    arr.push(id+':'+val);
                });
                $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
                });
                $.ajax({
                    type: 'POST',
                    url: "{{route('order_post')}}",
                    data: {'data':arr},
                    beforeSend: function(){
                        $('.loading-gif').show();
                    },
                    success: function(data){
                        $('.loading-gif').hide();
                        console.log('done');
                        table.ajax.reload();
                    },
                })
            });
        } );
    </script>
    <meta name="_token" content="{{ csrf_token() }}">
@endpush
@push('styles')
<style>
    .loading-gif{display:none;width: 100px;
    height: 36px;
    object-fit: cover;
    vertical-align: -8px;}    

</style>
@endpush
