<div class="tile" style="width:100%">
    <div class="row">
        <div class="col-lg-12">

            <div id="User">
                <user-template :userid="{{$entity->id}}"></user-template>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block"> Update </button>
            </div>
        </div>
    </div>
</div>


@push('scripts')
<link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css">
<script src="{{asset('js/app.js')}}"></script>
@endpush