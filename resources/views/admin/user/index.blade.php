@extends('layouts.admin')

@section('content')
<div class="app-title">
<div>
    <h1><i class="fa fa-th-list"></i> Data Table</h1>
    <p>Table to display analytical data effectively</p>
</div>
@include('admin.partials.breadcrumb')
</div>
<div class="row">
<div class="col-md-12">
    <div class="tile">
    <div class="tile-body">
        <table class="table table-hover table-bordered" id="sampleTable">
        <thead>
            <tr>
            <th>Name</th>
            <th>Email</th>
            <!-- <th>Office</th> -->
            <th></th>

            </tr>
        </thead>
        <tbody>
        @foreach($entities as $user)
            <tr>
            <td><a href="{{ route('users.edit', $user->id) }}">{{$user->name}}</a></td>
            <td>{{$user->email}}</td>
            <!-- <td></td> -->
            <td><a class="btn btn-secondary" href="{{ route('users.edit', $user->id) }}">Edit</a></td>

            </tr>
        @endforeach
      
        </tbody>
        </table>
    </div>
    </div>
</div>
</div>

@endsection

@push('scripts')
<script type="text/javascript" src="/admin/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/admin/js/plugins/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">$('#sampleTable').DataTable();</script>
@endpush