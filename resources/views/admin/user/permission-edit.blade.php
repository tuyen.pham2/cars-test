@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="primary col s6">
        {!! Form::open(['action' => ['Admin\UserController@updatePermission',$role->id], 'method' => 'POST', 'id' => 'form-post', 'class' => 'row']) !!}
            {{ method_field('POST') }}
            <div class="input-field">
                <label class="active" for="role">Role</label>
                {{ Form::text('role', $role->role, ['id' => 'name', 'placeholder' => '']) }}
            </div>
            <?php $i = 1;?>
            @foreach($permission as $key=>$per)
            <div class="col s4" style="margin-bottom:40px">
            	{{$per['title']}}
            	<?php 
            	foreach($per['permission'] as $value){ 
                    $pername ='';
                    if(isset($per['type'])){
                        foreach($per['type'] as $morekey => $more){
                            $pername .= $morekey.'-'.$value.$more;
                        }
                    }
                 ?>
            		<p>
                        <input id="{{$pername}}" <?php if(in_array($pername, $perOfRole)){echo 'checked="checked"';}?> name="permission[]" type="checkbox" value="{{$pername}}">
                        <label for="{{$pername}}">{{$per['title']}} {{$value}}</label>
                    </p>
            	<?php }
            	?>
            </div>
            <?php 
                if($i%3 == 0){
                    echo '<div class="clearfix"></div>';
                }
            ?>
            <?php $i++;?>
            @endforeach
            <div class="clearfix"></div>
            <button type="submit" class="btn btn-sm left waves-light waves-effect green accent-4">@lang('admin.button.edit')</button>

            <button type="button" class="btn-delete btn btn-sm left waves-light waves-effect red darken-4" value="{{$role->id}}">@lang('admin.button.delete')</button>
        {!! Form::close() !!}
    </div>
</div>
@endsection

@push('scripts')
    <script type="text/javascript">
        var form = $('#form-post');
        $('.btn-delete').click(function(event) {
            if (!confirm('{{ trans('admin.message.confirm') }}')) return;
            var idValue = $(this).val();
            $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
                type: 'POST',
                url: "{{action('Admin\UserController@permissionDestroyer')}}",
                data: {'id':idValue},
                success: function(data){
                    window.location.href = "{{action('Admin\UserController@permissionCustom')}}";
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            })
        });
    </script>
    <meta name="_token" content="{{ csrf_token() }}">
@endpush