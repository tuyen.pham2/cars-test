@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="primary col s6">
        {!! Form::open(['action' => 'Admin\UserController@storePermission', 'method' => 'POST', 'id' => 'form-user', 'class' => 'row']) !!}
            {{ method_field('POST') }}
            <div class="input-field">
                <label class="active" for="role">Role</label>
                {{ Form::text('role', '', ['id' => 'name', 'placeholder' => '']) }}
            </div>
            <?php $i = 1;?>
            @foreach($permission as $key=>$per)
            <div class="col s4" style="margin-bottom:40px">
                {{$per['title']}}
                <?php 
                foreach($per['permission'] as $value){ 
                    $pername ='';
                    if(isset($per['type'])){
                        foreach($per['type'] as $morekey => $more){
                            $pername .= $morekey.'-'.$value.$more;
                        }
                    }
                 ?>
                    <p>
                        <input id="{{$pername}}" name="permission[]" type="checkbox" value="{{$pername}}">
                        <label for="{{$pername}}">{{$per['title']}} {{$value}}</label>
                    </p>
                <?php }
                ?>
            </div>
            <?php 
                if($i%3 == 0){
                    echo '<div class="clearfix"></div>';
                }
            ?>
            <?php $i++;?>
            @endforeach
            <div class="clearfix"></div>
            <button type="submit" class="btn btn-sm left waves-light waves-effect green accent-4">@lang('admin.button.create')</button>
        {!! Form::close() !!}
    </div>
</div>
@endsection