@if(Session::has('response'))
<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('response')['message'] }}</p>
@endif
<div class="tile" style="width:100%;">
<div class="row">
    <div class="col-lg-9">
        <div class="form-group">
        <label for="Name">Name</label>
        <input class="form-control" id="Name" type="text" aria-describedby="emailHelp" placeholder="Enter name" name="name" value="{{$car->name}}">
        </div>

        <div class="form-group">
        <label for="Slug">SLug</label>
        <input class="form-control" id="Slug" type="text" aria-describedby="emailHelp" placeholder="Enter slug" name="slug" value="{{$car->slug}}">
        </div>

        <div class="form-group">
        <label for="description">Description</label>
        <textarea class="form-control" id="description" rows="3" name="description">{{$car->description}}</textarea>
        </div>

        <div class="form-group">
        <label for="Model">Model</label>
        <input name="model" class="form-control" id="Model" type="text" aria-describedby="emailHelp" placeholder="Enter Model" value="{{$car->model}}">
        </div>

        <div class="form-group">
        <label for="Engine">Engine Size</label>
        <input class="form-control" id="Engine" type="text" aria-describedby="emailHelp" placeholder="Enter Engine size" name="engine_size" value="{{$car->engine_size}}">
        </div>

        <div class="form-group">
        <label for="Make">Make</label>
        <input class="form-control" id="Make" type="text" aria-describedby="emailHelp" placeholder="Enter Make" name="make" value="{{$car->make}}">
        </div>

        <div class="form-group">
        <label for="Registration">Registration</label>
        <input class="form-control" id="Registration" type="text" aria-describedby="emailHelp" placeholder="Enter Registration" name="registration" value="{{$car->registration}}">
        </div>

        <div class="form-group">
        <label for="Price">Price</label>
        <input class="form-control" id="Price" type="number" aria-describedby="emailHelp" placeholder="Enter Price" name="price"  value="{{$car->price}}">
        </div>
        <div class="form-group">
            <label for="image">File input</label>
            <input class="form-control-file" id="image" type="file" name="upload">
            @if($car->image)
            <div class="image"><img  style="max-width:250px" src="/uploads/car/{{$car->image_url->name}}"></div>
            @endif
        </div>

        <div id="Attributes">
            <attributes-table :carid="{{$car->id}}"></attributes-table>
        </div>
    </div>
    <div class="col-lg-3">
        <fieldset class="form-group">
            <legend>Active</legend>
            <div class="form-check">
                <label class="form-check-label">
                <input class="form-check-input" id="optionsRadios1" type="radio" value="0" name="active" {{$car->active == 0 ? 'checked=""' : ''}}>Draft
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                <input class="form-check-input" id="optionsRadios2" type="radio" value="1"  name="active" {{$car->active == 1 ? 'checked=""' : ''}}>Active
                </label>
            </div>
    
        </fieldset>
        <fieldset class="form-group">
            <legend>Tags</legend>
        @if(count($tags)>0)
            <?php
                $car_tags = [];
                if (count($car->categories) > 0) {
                    foreach ($car->categories as $t) {
                        $car_tags[] = $t->id;
                    }
                }
            ?>
            @foreach ($tags as $tag)
        
      
            <div class="form-check">
                <label class="form-check-label">
                    <input class="form-check-input" name="categories[]" type="checkbox" value="{{$tag->id}}" {{ in_array($tag->id, $car_tags) ? 'checked="checked"' : ''}}>{{ $tag->name }}
                </label>
            </div>
          
            @endforeach
            
        @endif
        </fieldset>

      
        <div class="tile-footer">
              <button class="btn btn-primary" type="submit">Submit</button>
            </div>
    </div>
</div>
</div>
<div class="hidden list-select-attribute" style="display:none">
    <select class="form-control">
        @foreach($attributes as $attr)
        <option value="{{$attr->id}}">{{$attr->name}}</option>
        @endforeach
    </select>
</div>

<div class="hidden list-select-attribute-values" style="display:none">
    <select class="form-control">
        

    </select>
</div>

@push('scripts')
<link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css">
<script src="{{asset('js/app.js')}}"></script>
<!-- <script src="/admin/js/attributes.js"></sccript> -->
<script>
    function slug_render(str) {
        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();


        var from = "ạếãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
        var to   = "aeaaaaaeeeeeiiiiooooouuuunc------";
        for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '_') // collapse whitespace and replace by -
            .replace(/-+/g, '_'); // collapse dashes

        return str;
    };

    $("#Name").keyup(function(){
        var Text = $(this).val();
        Text = slug_render(Text);
        $("#Slug").val(Text);    
    });
</script>
@endpush