@extends('layouts.admin')

@section('content')
<div class="app-title">
    <div>
        <h1><i class="fa fa-edit"></i> Form Components</h1>
        <p>Bootstrap default form components</p>
    </div>
    <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
        <li class="breadcrumb-item">Forms</li>
        <li class="breadcrumb-item"><a href="#">Form Components</a></li>
    </ul>
</div>
<div class="row">
	<div class="col-md-12">
		{!! Form::open(['action' => ['App\Http\Controllers\Admin\CarController@update', $car->id], 'method' => 'PUT', 'id' => 'form-car', 'class' => 'row','enctype'=>'multipart/form-data']) !!}
			@include('admin.car.form')
		{!! Form::close() !!}	
	</div>
</div>
@endsection
