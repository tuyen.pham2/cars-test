@if(Session::has('response'))
<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('response')['message'] }}</p>
@endif
<div class="tile" style="width:100%;">
<div class="row">
    <div class="col-lg-9">
        <div class="form-group">
        <label for="ID">ID</label>
        <input class="form-control" readonly id="ID" type="text" value="{{$entity->id}}">
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                <label for="last_name">Last Name</label>
                <input class="form-control" id="last_name" type="text" name="last_name" value="{{$entity->last_name}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="first_name">First Name</label>
                    <input class="form-control" id="first_name" type="text" name="first_name" value="{{$entity->first_name}}">
                </div>
            
            </div>
        </div>
        <div class="form-group">
        <label for="email">Email</label>
        <input class="form-control" id="email" type="text" name="eamil" value="{{$entity->email}}">
        </div>
        <div class="form-group">
        <label for="phone">Phone</label>
        <input class="form-control" id="phone" type="text" name="phone" value="{{$entity->phone}}">
        </div>
        <div class="form-group">
        <label for="address">Address</label>
        <input class="form-control" id="address" type="text" name="address" value="{{$entity->address}}">
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                <label for="city">City</label>
                <input class="form-control" id="city" type="text" name="city" value="{{$entity->city}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                <label for="city">Distirct</label>
                <input class="form-control" id="city" type="text" name="city" value="{{$entity->city}}">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="Product">Products</label>
            <table class='table' id="product">
                <thead>
                    <th>Name</th>
                    <th>Price </th>
                    <th>Qty </th>
                    <th>Line Total </th>
                </thead>
                <tbody >
                    @foreach($entity->items as $item)
                        <tr>
                            <td>
                                {{$item->car->name}}
                            </td>
                            <td>
                                {{number_format($item->unit_price,2)}}
                            </td>
                            <td>
                                {{$item->qty}}
                            </td>
                            <td>
                            {{number_format($item->unit_price * $item->qty,2)}}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <table class="float-right">
                <tr>
                    <td><b>Subtotal:</b> </td>
                    <td>{{number_format($entity->total_price,2)}}</td>
                </tr>
                <tr>
                    <td><b>Total:</b> </td>
                    <td>{{number_format($entity->total_price,2)}}</td>
                </tr>
               
            </table>
            </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <label for="status">Status</label>
            <select class="form-control" name="order_status_id">
                @foreach($order_statuses as $status)
                <option value="{{$status->id}}" {{ $entity->order_status_id == $status->id ? 'selected' : '' }}>{{$status->title}}</option>
                @endforeach
            </select>
        </div>
        Created at: {{$entity->created_at }}
        <div class="tile-footer">
              <button class="btn btn-primary" type="submit">Submit</button>
        </div>
    </div>
</div>
</div>
