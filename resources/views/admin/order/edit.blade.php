@extends('layouts.admin')

@section('content')
{!! Form::open(['route' => ['orders.update', $entity->id], 'method' => 'PUT', 'id' => 'form-tag', 'class' => 'row']) !!}
    @include('admin.order.form')
{!! Form::close() !!}
@endsection
