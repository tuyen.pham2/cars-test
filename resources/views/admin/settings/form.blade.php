@if(Session::has('response'))
<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('response')['message'] }}</p>
@endif
<div class="tile" style="width:100%;">
<div class="row">
    <div class="col-lg-9">
        <div class="h2">
            Order
        </div>

        <div class="form-group">
            <label for="Name">Order Status Processing</label>
            <select name="values[order_processing]" class="form-control">
                @foreach($order_statuses as $status)
                <option value="{{$status->id}}" {{ $settings["order_processing"] == $status->id ? 'selected' : '' }}>{{$status->title}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="Slug">Order Status Completed</label>
            <select name="values[order_completed]" class="form-control">
            @foreach($order_statuses as $status)
                <option value="{{$status->id}}" {{ $settings['order_completed'] == $status->id ? 'selected' : '' }}>{{$status->title}}</option>
                @endforeach
            </select>
        </div>

        <div class="h2">Email</div>
        <div class="form-group">
            <label for="Slug">HOST</label>
            <input type="text" name="values[email_host]" class="form-control" value="{{$settings['email_host'] ?? ''}}">
        </div>
        <div class="form-group">
            <label for="Slug">PORT</label>
            <input type="text" name="values[email_port]" class="form-control" value="{{$settings['email_port'] ?? ''}}">
        </div>
        <div class="form-group">
            <label for="Slug">USERNAME</label>
            <input type="text" name="values[email_username]" class="form-control" value="{{$settings['email_username'] ?? ''}}">
        </div>
        <div class="form-group">
            <label for="Slug">PASSWORD</label>
            <input type="text" name="values[email_password]" class="form-control" value="{{$settings['email_password'] ?? ''}}">
        </div>

        <div id="EmailTest">
            <email-settings></email-settings>
        </div>
        
    </div>
    <div class="col-lg-3">
       
        <div class="tile-footer">
              <button class="btn btn-primary" type="submit">Submit</button>
        </div>
    </div>
</div>
</div>
@push('scripts')
<link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css">
<script src="{{asset('js/app.js')}}"></script>
@endpush