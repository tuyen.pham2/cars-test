@extends('layouts.admin')

@section('content')
{!! Form::open(['route' => ['settings.store'], 'method' => 'POST', 'id' => 'form-tag', 'class' => 'row']) !!}
    @include('admin.settings.form')
{!! Form::close() !!}
@endsection
