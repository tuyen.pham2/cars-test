@if(Session::has('response'))
<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('response')['message'] }}</p>
@endif
<div class="tile" style="width:100%;">
<div class="row">
    <div class="col-lg-9">
        <div class="form-group">
        <label for="Name">Name</label>
        <input class="form-control" id="Name" type="text" aria-describedby="emailHelp" placeholder="Enter name" name="name" value="{{$entity->name}}">
        </div>

        <div class="form-group">
        <label for="Slug">SLug</label>
        <input class="form-control" id="Slug" type="text" aria-describedby="emailHelp" placeholder="Enter slug" name="slug" value="{{$entity->slug}}">
        </div>
        <!-- VueJS -->
        <div class="tab-pane" id="app">
            <attribute-values :attributeid="{{ $entity->id }}"></attribute-values>
        </div>
    
    </div>
    <div class="col-lg-3">
       
        <div class="tile-footer">
              <button class="btn btn-primary" type="submit">Submit</button>
        </div>
    </div>
</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.18/vue.min.js"></script>
<link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css">
<script src="{{asset('js/app.js')}}"></script>
@push('scripts')
    <script>
    function slug_render(str) {
        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();


        var from = "ạếãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
        var to   = "aeaaaaaeeeeeiiiiooooouuuunc------";
        for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '_') // collapse whitespace and replace by -
            .replace(/-+/g, '_'); // collapse dashes

        return str;
    };
</script>
<style>
    .test-list {padding:0;list-style: none;}
    .test-list--item {padding-right:50px;position: relative;margin-bottom: 10px;}
    .test-list--item button {position: absolute;top:0;right:0}
</style>
@endpush
