@extends('layouts.admin')

@section('content')
{!! Form::open(['route' => ['attributes.update', $entity->id], 'method' => 'PUT', 'id' => 'form-tag', 'class' => 'row']) !!}
    @include('admin.attributevalue.form')
{!! Form::close() !!}
@endsection
