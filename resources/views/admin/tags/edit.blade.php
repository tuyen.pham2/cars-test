@extends('layouts.admin')

@section('content')
{!! Form::open(['route' => ['tags.update', $tag->id], 'method' => 'PUT', 'id' => 'form-tag', 'class' => 'row']) !!}
    @include('admin.tags.form')
{!! Form::close() !!}
@endsection

@push('scripts')
	<script type="text/javascript">
		var form = $('#form-tag');
		$('.btn-delete').click(function(event) {
			if (!confirm('{{ trans('admin.message.confirm') }}')) return;
			form.children('input[name="_method"]').val('DELETE');
			form.submit();
		});
	</script>
@endpush
