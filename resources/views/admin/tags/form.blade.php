@push('scripts')
    <script>
    function slug_render(str) {
        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();


        var from = "ạếãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
        var to   = "aeaaaaaeeeeeiiiiooooouuuunc------";
        for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '_') // collapse whitespace and replace by -
            .replace(/-+/g, '_'); // collapse dashes

        return str;
    };

    $("#Name").keyup(function(){
        var Text = $(this).val();
        Text = slug_render(Text);
        $("#Slug").val(Text);    
    });
</script>
@endpush


@if(Session::has('response'))
<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('response')['message'] }}</p>
@endif
<div class="tile" style="width:100%;">
<div class="row">
    <div class="col-lg-9">
        <div class="form-group">
        <label for="Name">Name</label>
        <input class="form-control" id="Name" type="text" aria-describedby="emailHelp" placeholder="Enter name" name="name" value="{{$tag->name}}">
        </div>

        <div class="form-group">
        <label for="Slug">SLug</label>
        <input class="form-control" id="Slug" type="text" aria-describedby="emailHelp" placeholder="Enter slug" name="slug" value="{{$tag->slug}}">
        </div>

    </div>
    <div class="col-lg-3">
       
        <div class="tile-footer">
              <button class="btn btn-primary" type="submit">Submit</button>
        </div>
    </div>
</div>
</div>
