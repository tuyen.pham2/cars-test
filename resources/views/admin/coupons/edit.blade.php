@extends('layouts.admin')

@section('content')
{!! Form::open(['route' => ['coupons.update', $entity->id], 'method' => 'PUT', 'id' => 'form-tag', 'class' => 'row']) !!}
    @include('admin.coupons.form')
{!! Form::close() !!}
@endsection
