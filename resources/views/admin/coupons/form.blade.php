
@foreach ($errors->all() as $message) 
<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{$message}}</p>
@endforeach
@if(Session::has('response'))
<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('response')['message'] }}</p>
@endif
<div class="tile" style="width:100%;">
<div class="row">
    <div class="col-lg-9">
        <div class="form-group">
        <label for="Name">Name</label>
        <input class="form-control" id="Name" type="text" aria-describedby="emailHelp" placeholder="Enter name" name="name" value="{{$entity->name}}">
        </div>

        <div class="form-group">
        <label for="Coded">CODE</label>
        <input class="form-control" id="Code" type="text" aria-describedby="emailHelp" placeholder="Enter code" name="code" value="{{$entity->code}}">
        </div>
     
        <div class="form-check">
            <label class="form-check-label">
            <input class="form-check-input" id="Percent" type="checkbox" name="is_percent" value="1" {{ $entity->is_percent == 1 ? 'checked="checked"' : '' }}> Is Percent
            </label>
        </div>
        <br>
        <div class="form-group">
            <label for="Value">Value</label>
            <input class="form-control" id="Value" type="text" placeholder="Enter Value" name="value" value="{{$entity->value}}">
        </div>
        <div class="form-group">
            <label for="Start_date">Start Date</label>
            <input class="form-control" id="Start_date" type="text" placeholder="Select Date" name="start_date" value="{{$entity->start_date}}">
        </div>
        <div class="form-group">
            <label for="End_date">End Date</label>
            <input class="form-control" id="End_date" type="text" placeholder="Enter Date" name="end_date" value="{{$entity->end_date}}">
        </div>

    </div>
    <div class="col-lg-3">
        <fieldset class="form-group">
            <legend>Active</legend>
            <div class="form-check">
                <label class="form-check-label">
                <input class="form-check-input" id="optionsRadios2" type="radio" value="1"  name="active" {{$entity->active == 1 ? 'checked=""' : ''}}>Active
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                <input class="form-check-input" id="optionsRadios1" type="radio" value="0" name="active" {{$entity->active == 0 ? 'checked=""' : ''}}>Draft
                </label>
            </div>
            
    
        </fieldset>
        <div class="tile-footer">
              <button class="btn btn-primary" type="submit">Submit</button>
        </div>
    </div>
</div>
</div>

@push('scripts')
<script type="text/javascript" src="/admin/js/plugins/bootstrap-datepicker.min.js"></script>
<script>
    (function($){
        $('#Start_date,#End_date').datepicker({
            format: "yyyy-mm-dd",

            autoclose: true,
            todayHighlight: true
        });
    })(jQuery);
    
</script>
@endpush