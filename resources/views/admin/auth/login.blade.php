@extends('layouts.app')

@section('title', 'Login')

@section('content')

<section class="section-pagetop bg-dark">
        <div class="container clearfix">
            <h2 class="title-page">Login</h2>
        </div> <!-- container //  -->
    </section>
    <!-- ========================= SECTION INTRO END// ========================= -->
    <!-- ========================= SECTION CONTENT END// ========================= -->
    <section class="section-content bg padding-y">
        <div class="container">

          @if($errors->any())
          <h4>{{$errors->first()}}</h4>
          @endif
            <div class="col-md-6 mx-auto">
                <div class="card">
                    <header class="card-header">
                        <h4 class="card-title mt-2">Login</h4>
                    </header>
                    <article class="card-body">
                        <form method="POST" action="{{ route('login_admin') }}">
                          {{ csrf_field() }}
                        
                          <div class="form-group">
                              <label>Email address</label>
                              <input type="email" class="form-control" placeholder="" name="email">
                              <!-- <small class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                          </div>
                    
                          <!-- form-row.// -->
                          <div class="form-group">
                              <label>Password</label>
                              <input class="form-control" type="password" name="password">
                          </div>
                          <!-- form-group end.// -->
                          <div class="form-group">
                              <button type="submit" class="btn btn-primary btn-block"> Login </button>
                          </div>
                            <!-- form-group// -->
                            <!-- <small class="text-muted">By clicking the 'Sign Up' button, you confirm that you accept our <br> Terms of use and Privacy Policy.</small> -->
                        </form>
                    </article>
                    <!-- card-body end .// -->
                    <!-- <div class="border-top card-body text-center">Have an account? <a href="{{route('login_frontend')}}">Log In</a></div> -->
                </div>
                <!-- card.// -->
            </div>
        </div>
    </section>  

<!-- <form action="{{ route('login_admin') }}" method="POST" role="form" class="login-form">
    {{ csrf_field() }}
    <div class="row">
        <div class="input-field col s12 center">
            <img src="/images/logo.png" alt="" class="responsive-img valign profile-image-login">
            <p class="center login-form-text">Admin Area</p>
        </div>
    </div>
    <div class="row margin">
        <div class="input-field col s12">
            <i class="mdi-social-person-outline prefix"></i>
            <input id="email" type="email" name="email" value="{{ old('email') }}" autofocus>
            <label for="email" class="center-align">@lang('admin.field.email')</label>
        </div>
    </div>
    <div class="row margin">
        <div class="input-field col s12">
            <i class="mdi-action-lock-outline prefix"></i>
            <input id="password" type="password" name="password">
            <label for="password">@lang('admin.field.password')</label>
        </div>
    </div>
    <div class="row">
        <div class="input-field col s12 m12 l12  login-text">
            <input type="checkbox" id="remember-me" name="remember" />
            <label for="remember-me">Remember me</label>
        </div>
    </div>
    <div class="row">
        <div class="input-field col s12">
            <button type="submit" class="btn btn-default waves-effect waves-light col s12">@lang('admin.button.login')</button>
        </div>
    </div>
    <!-- <div class="row">
        <div class="input-field col s6 m6 l6">
            <p class="margin medium-small"><a href="page-register.html">Register Now!</a></p>
        </div>
        <div class="input-field col s6 m6 l6">
            <p class="margin right-align medium-small"><a href="#">Forgot password ?</a></p>
        </div>
    </div> -->
</form> -->
@endsection
