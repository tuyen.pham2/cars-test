@extends('layouts.app')

<!-- Main Content -->
@section('content')

<div id="register_card" class="forgot_password information">
    <div class="container">
      <h1>Quên mật khẩu</h1>
      <p class="center">Vui lòng nhập đúng email đã đăng ký, chúng tôi sẽ gởi mật khẩu qua email của bạn.</p>
      <form id="form_login" role="form" method="POST" action="{{ url('/password/email') }}">
        {{ csrf_field() }}

        <div class="top_form">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
          <div class="row">
            <div class="form-group col-md-12">
              <label for="mst">@lang('passwords.email_or_membershipid') <span class="error-require">(*)</span>: </label>
              <input id="email"  class="form-control" name="email" placeholder="Nhập Email" value="{{ old('email') }}">
              <!-- type="email" -->
            </div>
          </div>
          <div class="clearfix"></div>
          <button type="submit" class="btn btn-lg btn-custom login-button">Gởi</button>
      </div>  
      </form>
    </div>
  </div>
@endsection
