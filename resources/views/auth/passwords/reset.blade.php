@extends('layouts.app')

@section('content')

<div id="register_card" class="new_password information">
    <div class="container">
      <h1>@lang('user.forgot_pass')</h1>
      <p class="center">@lang('user.password_to_login')</p>
        <form id="form_login" role="form" method="POST" action="{{ url('/password/reset') }}">
        {{ csrf_field() }}
        <input type="hidden" name="token" value="{{ $token }}">
         <input id="email" type="hidden" class="form-control" name="email" value="{{ old('email', $email) }}" required autofocus />
        <div class="top_form">
          <div class="row">
            <div class="form-group col-md-12">
              <label for="mst">@lang('user.new_password') <span class="error-require">(*)</span>: </label>
              <input type="password" class="form-control" name="password" placeholder="@lang('user.input_new_pass')">
              @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="clearfix"></div>
            <div class="form-group col-md-12">
              <label for="mst">@lang('user.input_new_pass_a') <span class="error-require">(*)</span>: </label>
              <input type="password" class="form-control" name="password_confirmation" placeholder="@lang('user.input_new_pass_a')">
              @if ($errors->has('password_confirmation'))
                <span class="help-block">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
            @endif
            </div>
          </div>
          <div class="clearfix"></div>
          <button type="submit" class="btn btn-lg btn-custom login-button">@lang('user.submit')</button>
      </div>  
      </form>
    </div>
  </div>
<!-- <div class="modal fade" id="modal-reset-password">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-body text-center">
                <form action="{{ action('Auth\ResetPasswordController@reset') }}" method="POST" class="form-reset" role="form">
                    {{ csrf_field() }}
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="form-top">
                        <img class="" src="/images/logo.png">
                        <h4 class="text-uppercase text-white">@lang('admin.title.reset password')</h4>
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="@lang('admin.field.email')" required="">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" value="" placeholder="@lang('admin.field.password')" required="">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password_confirmation" value="" placeholder="@lang('admin.field.password_confirmation')" required="">
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group"><button type="submit" class="btn btn-color text-uppercase">@lang('admin.title.reset password')</button></div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div> -->

@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#modal-reset-password').modal({
                backdrop: 'static',
                keyboard: false
            }).modal('show');
        });
    </script>
@endpush
