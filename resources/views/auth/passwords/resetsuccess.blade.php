@extends('layouts.app')

@section('content')
<div id="password_changed" class="information">
    <div class="container">
      <p class="center">Mật khẩu đã được thay đổi, vui lòng sử dụng mật khẩu mới để <a href="{{url('login')}}">Đăng nhập</a><br>vào Phúc Long.</p>
    </div>
</div>
@endsection