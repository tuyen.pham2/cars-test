@extends('layouts.app')

@section('title', 'Đăng nhập')

@section('content')
<section class="section-pagetop bg-dark">
        <div class="container clearfix">
            <h2 class="title-page">Login</h2>
        </div> <!-- container //  -->
    </section>
    <!-- ========================= SECTION INTRO END// ========================= -->
    <!-- ========================= SECTION CONTENT END// ========================= -->
    <section class="section-content bg padding-y">
        <div class="container">

          @if($errors->any())
          <h4>{{$errors->first()}}</h4>
          @endif
            <div class="col-md-6 mx-auto">
                <div class="card">
                    <header class="card-header">
                        <h4 class="card-title mt-2">Login</h4>
                    </header>
                    <article class="card-body">
                        <form method="POST" action="{{ route('login_frontend.post') }}">
                          {{ csrf_field() }}
                          <input type="hidden" name="redirect" value="{{$_GET['redirect'] ?? '' }}">
                          <div class="form-group">
                              <label>Email address</label>
                              <input type="email" class="form-control" placeholder="" name="email">
                              <!-- <small class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                          </div>
                    
                          <!-- form-row.// -->
                          <div class="form-group">
                              <label>Password</label>
                              <input class="form-control" type="password" name="password">
                          </div>
                          <!-- form-group end.// -->
                          <div class="form-group">
                              <button type="submit" class="btn btn-primary btn-block"> Login </button>
                          </div>
                            <!-- form-group// -->
                            <!-- <small class="text-muted">By clicking the 'Sign Up' button, you confirm that you accept our <br> Terms of use and Privacy Policy.</small> -->
                        </form>
                    </article>
                    <!-- card-body end .// -->
                    <!-- <div class="border-top card-body text-center">Have an account? <a href="{{route('login_frontend')}}">Log In</a></div> -->
                </div>
                <!-- card.// -->
            </div>
        </div>
    </section>  
@endsection
