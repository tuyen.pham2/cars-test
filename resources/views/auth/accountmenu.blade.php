@if ($currentUser)
<div class="h_account islogin clearfix">


    <ul class="h_menu__main">
         <li id="" class="arrow_bottom menu-acc">
                <a href="">Hi, {{ $currentUser->name }}</a>
                <div class="h_menu__sub">
                    <ul class=" level1">
                        <li>
                            <ul class="list-multi level2">
                                <li><a href="{{-- action('MembershipController@show') --}}"> @lang('user.button.change pass')</a></li>
                    
                                <li class="menu-item-user-logout">
                                    <form action="{{ route('logout_frontend') }}" method="POST" class="form-inline" role="form">
                                        {{ csrf_field() }}
                                        <button class="" type="submit"> Logout</button>
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
        </li>
    </ul>

</div>

@else

     <ul class="h_menu__main">
        <li id="" class="acc-login">
             <a href="{{ route('login_frontend') }}" class=" text btn btn-primary"><span>Login</span></a>
        </li>
        {{--<li id="" class="acc-register">
            <a href="@if($current_locale == 'en'){{'/en/dang-ky-the'}}@else{{url('dang-ky-the')}}@endif" class=" text"><span>@lang('admin.button.id_register')</span></a>
        </li>--}}
    </ul>

@endif