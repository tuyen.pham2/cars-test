@extends('layouts.app')

@section('content')
<section class="section-main bg padding-top-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <!-- ================= main slide ================= -->
                    <div class="owl-init slider-main owl-carousel" data-items="1" data-dots="false" data-nav="true">
                        <div class="item-slide">
                            <img src="/cars/images/banners/slide1.jpg">
                        </div>
                        <div class="item-slide rounded">
                            <img src="/cars/images/banners/slide2.jpg">
                        </div>
                        <div class="item-slide rounded">
                            <img src="/cars/images/banners/slide3.jpg">
                        </div>
                    </div>
                    <!-- ============== main slidesow .end // ============= -->
                </div>
                <!-- col.// -->
                <div class="col-md-3">
                    @php($i = 1)
                    @foreach($cars as $car)
                    @if($i <=4)
                    <div class="card mt-2 mb-2">
                        <figure class="itemside">
                            <div class="aside">
                                <div class="img-wrap img-sm border-right"><img src="/cars/images/items/3.jpg"></div>
                            </div>
                            <figcaption class="p-3">
                                <h6 class="title"><a href="#">{{$car->name}}</a></h6>
                                <div class="price-wrap">
                                    <span class="price-new b">${{$car->price}}</span>
                                    <!-- <del class="price-old text-muted">$1980</del> -->
                                </div>
                                <!-- price-wrap.// -->
                            </figcaption>
                        </figure>
                    </div>
                    @endif
                    @php ($i++)
                    @endforeach
                </div>
                <!-- col.// -->
            </div>
        </div>
        <!-- container .//  -->
    </section>
    <!-- ========================= SECTION MAIN END// ========================= -->
    <!-- ========================= Blog ========================= -->
    <section class="section-content padding-y-sm bg">
        <div class="container">
            <header class="section-heading heading-line">
                <h4 class="title-section bg">Featured Categories</h4>
            </header>
            <div class="row">
                @foreach($tags as $tag)
                <div class="col-md-4">
                    <div class="card-banner" style="height:250px; background-image: url('/cars/images/posts/1.jpg');">
                        <article class="overlay overlay-cover d-flex align-items-center justify-content-center">
                            <div class="text-center">
                                <h5 class="card-title">{{$tag->name}}</h5>
                                <a href="{{route('tags',$tag->slug)}}" class="btn btn-warning btn-sm"> View All </a>
                            </div>
                        </article>
                    </div>
                    <!-- card.// -->
                </div>
                @endforeach

            </div>
        </div>
    </section>
    <!-- ========================= Blog .END// ========================= -->

    <!-- ========================= SECTION CONTENT ========================= -->
    <section class="section-content padding-y-sm bg">
        <div class="container">

            <header class="section-heading heading-line">
                <h4 class="title-section bg">FEATURED PRODUCTS</h4>
            </header>
            <div class="row">
                @foreach($cars as $car)
                    @include('partials.car')
                @endforeach            
            </div>
        
        </div>
        <!-- container .//  -->
    </section>

@endsection