@extends('layouts.app')

@section('content')
<section class="section-main bg padding-top-sm">
    <section class="section-content padding-y-sm bg">
        <div class="container">

            <header class="section-heading heading-line">
                <h4 class="title-section bg">RESULTS OF KEYWORD "{{$keyword}}"</h4>
            </header>
            <div class="row">
                @foreach($cars as $car)
                    @include('partials.car_es')
                @endforeach            
            </div>
        
        </div>
    </section>
@endsection