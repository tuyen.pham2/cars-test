<!DOCTYPE html>
<html lang="" ng-app="Car">

<head>
    <meta charset="utf-8">
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <meta name="_token" content="{{ csrf_token() }}">

    <meta name="robots" content="index, follow">
    <meta property="og:url" content="{{ url()->current() }}">
    <meta property="og:type" content="article">
    <meta property="og:site_name" content="Car">

    <title>Car</title>
    <meta name="description" content="">
    <meta property="og:title" content="Car">
    <meta property="og:description" content="">

    <link rel="stylesheet" type="text/css" href="/css/vendor.css">
    <link rel="stylesheet" type="text/css" href="/css/app.css?rf=1">

    @stack('styles')
    <style>
        #news-detail .info-detail h1{font-family: 'Lora';font-size: 36px}
        .category-product .cat-item .cat-content .description{line-height: 34px}
        .page-heading h1{font-family: 'Lora'}
        .h_menu .container .h_menu__main > li a{padding:10px 15px;}
        .h_logo__img section {
          position: relative;
          height: 70px;
        }
        .h_logo__img section a > img {
          position: absolute !important;
          left: 50%;
          transform: translateX(-50%);
          padding: 1px 5px;
        }
        .h_logo__img section a > img.top {
          animation-name: fade;
          animation-timing-function: ease-in-out;
          animation-iteration-count: infinite;
          animation-duration: 7s;
          animation-direction: alternate;
          background: white;
          
        }
        #news-detail .info-detail h1{line-height: 50px}
        .header-pl .lang-wrapper {margin-top:7px;float:right;}
        .header-pl .lang-wrapper a{line-height: 19px;}

        .h_logo__img section a > img.middle {
          animation-name: fade2;
          animation-timing-function: ease-in-out;
          animation-iteration-count: infinite;
          animation-duration: 7s;
          animation-direction: alternate;
          background: white;
        }

        img.bottom {
            background: white;
        }
        .h_menu .container .h_menu__main > li a{
            font-size: 15px
        }
        a.lang-item:hover{color:#07a252;}
        @keyframes fade {
          0% {
            opacity: 1;
          }
          20% {
            opacity: 1;
          }
          40% {
            opacity: 0;
          }
          60% {
            opacity: 0;
          }
          80% {
            opacity: 0;
          }
          100% {
            opacity: 0;
          }
        }

        @keyframes fade2 {
          0% {
            opacity: 0;
          }
          20% {
            opacity: 0;
          }
          40% {
            opacity: 1;
          }
          60% {
            opacity: 1;
          }
          80% {
            opacity: 0;
          }
          100% {
            opacity: 0;
          }
        }
        .lang-wrapper >a.active{
            color: #00713d;
            font-weight: bold;  
        }       

        .mg-right{padding-right: 50px;}
        .mg-left{padding-left: 50px}

        @media screen and (max-width: 767px){

            .h_menu .container .h_menu__main > li:hover .h_menu__sub {display: none }
            .h_menu .container .h_menu__main > li.active:hover .h_menu__sub {display: block }
            .category-drink .product-item.sticky .item-info{
                text-align: left;
                bottom: 15px;
                width: calc(100% - 80px);
                position: absolute;
                left: 40px;
                right: auto;
                top: 50px;
                height: auto;
                -webkit-transform: translateX(0%);
                -ms-transform: translateX(0%);
                transform: translateX(0%);
            }
            .category-drink .product-item.sticky .item-wrapper > .img-responsive{
                width: auto;
                float: none;
                margin: 0 auto;
                padding-top: 250px;
                padding-bottom: 20px;
            }
            .grid-item .sticky.product-item{
                max-height: none
            }
            .category-drink .product-item.sticky .item-desc{
                max-height: 52px;overflow: hidden;height:52px;
            }
            .col-xs-12, .col-xs-6:nth-child(2n+1), .col-xs-4:nth-child(3n+1), .col-xs-3:nth-child(4n+1), .col-xs-2:nth-child(6n+1){
                clear:none;
            }
            .ss-1 .row-2 {display: block!important; width: 100%;margin: 0;}
            .ss-1 .row-2 .col-sm-6{
                    display: block !important;
                    vertical-align: middle !important;
                    width: 100%;
            }
            .ss-1 .row-2 .img{width: 100% !important}
            .intro_info img {height:auto !important;}
        }
        .header-pl .lang-wrapper{margin-right:10px;}
        .header-pl .h_account{margin-right:0;}
        #about-us .h_menu__sub {
            width: 60%;
            left: auto;
            right: 0;
                min-width: 600px;
        }
        .h_menu .container .h_menu__main > li {
            margin: 0 10px 0 10px;
        }
        .h_menu .container .h_menu__main > li .h_menu__sub ul li ul li a{font-size: 16px;font-weight: 500}
        #home_story svg{
            height:80px;
        }
        .content_fields>div>div h2{font-size: 24px;}
        .content_fields h2{font-family: sans-serif;}
        .grid-item .product-item > .item-wrapper .item-name{
                -webkit-line-clamp: initial !important;
        }
        .h_menu .container .h_menu__box{
            height: calc(100% - 70px);
        }
        .mini-cart{
          margin:0
        }
        #mini-cart .groupbtn p{
          display: inline-block;
        }
        #mini-cart .groupbtn p a {
          color:#999;
          border:1px solid #999
        }
        #mini-cart .groupbtn p a:hover {
          color: #0C713D;
    border-color: #0C713D;
    background-color: transparent;
        }
        .header-pl{
          position: relative;
        }
        .main-full{
          margin-top:20px
        }
        .cart-icon {
        
          background:#ddd;
        }
        .header-pl .h_logo img {
          max-height: 60px;
          max-width: 200px !important;
        }
        .h_right .h_menu__main {
          float:right
        }
        .item-info {
          text-align: center;
        }
        .product-item .item-wrapper img {
          border: 1px solid #ddd;
        }
    </style>
</head>

<body class="">
@include('site.menu', ['cart' => $cart->content()])
    @yield('content')

    @include('partials.modal-register')
    {{-- @include('partials.modal-pleaselogin') --}}
    {{-- @include('site.footer') --}}


    <script type="text/javascript" src="/js/plugins/jquery-3.3.1.min.js"></script>



    <!-- <script type="text/javascript" src="/js/plugins.js"></script> -->
    <!-- <script type="text/javascript" src="/js/ng.js"></script> -->
    <!-- <script type="text/javascript" src="/js/moment.js"></script> -->
    <!-- <script type="text/javascript" src="/js/datepicker/bootstrap-datetimepicker.min.js"></script> -->
    <!-- <script type='text/javascript' src='/js/owl.carousel.min.js'></script>  -->
    <!-- <script type='text/javascript' src='/js/plugins/owl.carousel2.thumbs.min.js'></script>  -->
    <!-- <script type='text/javascript' src='/js/imagesloaded.pkgd.min.js'></script>  -->
    <script type="text/javascript" src="/js/app.js?v=11"></script>
    <script type="text/javascript" src="/js/site/custom-script.js"></script>




    @stack('scripts')
    <link rel="stylesheet" type="text/css" href="/custom-css/603gs.css">
    <!-- Start Quick Call Buttons -->
    <!-- <div class='quick-call-button'></div>
    <div class='call-now-button' id='draggable'>
        <div><p class='call-text'> <b>Hotline: 1800 6779</b> </p>
            <a href='tel:18006779' id='quickcallbutton' ' title='Call Now' >
            <div class='quick-alo-ph-circle active'></div>
            <div class='quick-alo-ph-circle-fill active'></div>
            <div class='quick-alo-ph-img-circle shake'></div>
            </a>
        </div>
    </div> -->
    <style>
        @media screen and (max-width: 1600px) {
            .call-now-button { display: flex !important; background: #1a1919; }
            .quick-call-button { display: block !important; }
        }
        @media screen and (min-width: px) {
            .call-now-button .call-text { display: none !important; }
        }
        @media screen and (max-width: px) {
            .call-now-button .call-text { display: none !important; }
        }
        @media screen and (max-width: 767px){
            .call-now-button{z-index: 10}
        }
        .call-now-button { top: 80%; }
        .call-now-button { left: 5%; }
        .call-now-button { background: #1a1919; }
        .call-now-button div a .quick-alo-ph-img-circle, .call-now-button div a .quick-alo-phone-img-circle { background-color: #0c3; }
        .call-now-button .call-text { color: #fff; }
    </style>
    <!-- /End Quick Call Buttons -->
</body>

</html>

//////////////////////////////////////////////////////////

<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Bootstrap-ecommerce by Vosidiy">

    <title>Home Page - Shopper</title>

    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">

    <!-- jQuery -->
    <script src="js/jquery-2.0.0.min.js" type="text/javascript"></script>

    <!-- Bootstrap4 files-->
    <script src="js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <link href="css/bootstrap.css?v=1.0" rel="stylesheet" type="text/css" />

    <!-- Font awesome 5 -->
    <link href="fonts/fontawesome/css/fontawesome-all.min.css" type="text/css" rel="stylesheet">

    <!-- plugin: fancybox  -->
    <script src="plugins/fancybox/fancybox.min.js" type="text/javascript"></script>
    <link href="plugins/fancybox/fancybox.min.css" type="text/css" rel="stylesheet">

    <!-- plugin: owl carousel  -->
    <link href="plugins/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="plugins/owlcarousel/assets/owl.theme.default.css" rel="stylesheet">
    <script src="plugins/owlcarousel/owl.carousel.min.js"></script>

    <!-- custom style -->
    <link href="css/ui.css?v=1.0" rel="stylesheet" type="text/css" />
    <link href="css/responsive.css" rel="stylesheet" media="only screen and (max-width: 1200px)" />

    <!-- custom javascript -->
    <script src="js/script.js" type="text/javascript"></script>

    <script type="text/javascript">
        /// some script

        // jquery ready start
        $(document).ready(function() {
            // jQuery code

        });
        // jquery end
    </script>

</head>

<body>
    <header class="section-header">
        <section class="header-main">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-3">
                        <div class="brand-wrap">
                            <img class="logo" src="images/logo-dark.png">
                            <h2 class="logo-text">LOGO</h2>
                        </div>
                        <!-- brand-wrap.// -->
                    </div>
                    <div class="col-lg-6 col-sm-6">
                        <form action="#" class="search-wrap">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!-- search-wrap .end// -->
                    </div>
                    <!-- col.// -->
                    <div class="col-lg-3 col-sm-6">
                        <div class="widgets-wrap d-flex justify-content-end">
                            <div class="widget-header">
                                <a href="#" class="icontext">
                                    <div class="icon-wrap icon-xs bg2 round text-secondary"><i class="fa fa-shopping-cart"></i></div>
                                    <div class="text-wrap">
                                        <small>Basket</small>
                                        <span>3 items</span>
                                    </div>
                                </a>
                            </div>
                            <!-- widget .// -->
                            <div class="widget-header dropdown">
                                <a href="#" class="ml-3 icontext" data-toggle="dropdown" data-offset="20,10">
                                    <div class="icon-wrap icon-xs bg2 round text-secondary"><i class="fa fa-user"></i></div>
                                    <div class="text-wrap">
                                        <small>Hello.</small>
                                        <span>Login <i class="fa fa-caret-down"></i></span>
                                    </div>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <form class="px-4 py-3">
                                        <div class="form-group">
                                            <label>Email address</label>
                                            <input type="email" class="form-control" placeholder="email@example.com">
                                        </div>
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input type="password" class="form-control" placeholder="Password">
                                        </div>
                                        <button type="submit" class="btn btn-primary">Sign in</button>
                                    </form>
                                    <hr class="dropdown-divider">
                                    <a class="dropdown-item" href="#">Have account? Sign up</a>
                                    <a class="dropdown-item" href="#">Forgot password?</a>
                                </div>
                                <!--  dropdown-menu .// -->
                            </div>
                            <!-- widget  dropdown.// -->
                        </div>
                        <!-- widgets-wrap.// -->
                    </div>
                    <!-- col.// -->
                </div>
                <!-- row.// -->
            </div>
            <!-- container.// -->
        </section>
        <!-- header-main .// -->

        <nav class="navbar navbar-expand-lg navbar-dark bg-secondary">
            <div class="container">

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav" aria-controls="main_nav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="main_nav">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link pl-0" href="#"> <strong>All category</strong></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Fashion</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Supermarket</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Electronics</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Baby &amp Toys</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Fitness sport</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown07" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More</a>
                            <div class="dropdown-menu" aria-labelledby="dropdown07">
                                <a class="dropdown-item" href="#">Foods and Drink</a>
                                <a class="dropdown-item" href="#">Home interior</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Category 1</a>
                                <a class="dropdown-item" href="#">Category 2</a>
                                <a class="dropdown-item" href="#">Category 3</a>
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- collapse .// -->
            </div>
            <!-- container .// -->
        </nav>

    </header>
    <!-- section-header.// -->

    <!-- ========================= SECTION MAIN ========================= -->
    <section class="section-main bg padding-top-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <!-- ================= main slide ================= -->
                    <div class="owl-init slider-main owl-carousel" data-items="1" data-dots="false" data-nav="true">
                        <div class="item-slide">
                            <img src="images/banners/slide1.jpg">
                        </div>
                        <div class="item-slide rounded">
                            <img src="images/banners/slide2.jpg">
                        </div>
                        <div class="item-slide rounded">
                            <img src="images/banners/slide3.jpg">
                        </div>
                    </div>
                    <!-- ============== main slidesow .end // ============= -->
                </div>
                <!-- col.// -->
                <div class="col-md-3">
                    <div class="card mt-2 mb-2">
                        <figure class="itemside">
                            <div class="aside">
                                <div class="img-wrap img-sm border-right"><img src="images/items/3.jpg"></div>
                            </div>
                            <figcaption class="p-3">
                                <h6 class="title"><a href="#">Some name of item goes here nice</a></h6>
                                <div class="price-wrap">
                                    <span class="price-new b">$1280</span>
                                    <del class="price-old text-muted">$1980</del>
                                </div>
                                <!-- price-wrap.// -->
                            </figcaption>
                        </figure>
                    </div>
                    <!-- card.// -->
                    <div class="card mb-2">
                        <figure class="itemside">
                            <div class="aside">
                                <div class="img-wrap img-sm border-right"><img src="images/items/3.jpg"></div>
                            </div>
                            <figcaption class="p-3">
                                <h6 class="title"><a href="#">Some name of item goes here nice</a></h6>
                                <div class="price-wrap">
                                    <span class="price-new b">$1280</span>
                                    <del class="price-old text-muted">$1980</del>
                                </div>
                                <!-- price-wrap.// -->
                            </figcaption>
                        </figure>
                    </div>
                    <!-- card.// -->
                    <div class="card mb-2">
                        <figure class="itemside">
                            <div class="aside">
                                <div class="img-wrap img-sm border-right"><img src="images/items/3.jpg"></div>
                            </div>
                            <figcaption class="p-3">
                                <h6 class="title"><a href="#">Some name of item goes here nice</a></h6>
                                <div class="price-wrap">
                                    <span class="price-new b">$1280</span>
                                    <del class="price-old text-muted">$1980</del>
                                </div>
                                <!-- price-wrap.// -->
                            </figcaption>
                        </figure>
                    </div>
                    <!-- card.// -->
                </div>
                <!-- col.// -->
            </div>
        </div>
        <!-- container .//  -->
    </section>
    <!-- ========================= SECTION MAIN END// ========================= -->
    <!-- ========================= Blog ========================= -->
    <section class="section-content padding-y-sm bg">
        <div class="container">
            <header class="section-heading heading-line">
                <h4 class="title-section bg">Featured Categories</h4>
            </header>
            <div class="row">
                <div class="col-md-4">
                    <div class="card-banner" style="height:250px; background-image: url('images/posts/1.jpg');">
                        <article class="overlay overlay-cover d-flex align-items-center justify-content-center">
                            <div class="text-center">
                                <h5 class="card-title">Primary text as title</h5>
                                <a href="#" class="btn btn-warning btn-sm"> View All </a>
                            </div>
                        </article>
                    </div>
                    <!-- card.// -->
                </div>
                <div class="col-md-4">
                    <div class="card-banner" style="height:250px; background-image: url('images/posts/2.jpg');">
                        <article class="overlay overlay-cover d-flex align-items-center justify-content-center">
                            <div class="text-center">
                                <h5 class="card-title">Primary text as title</h5>
                                <a href="#" class="btn btn-warning btn-sm"> View All </a>
                            </div>
                        </article>
                    </div>
                    <!-- card.// -->
                </div>
                <div class="col-md-4">
                    <div class="card-banner" style="height:250px; background-image: url('images/posts/3.jpg');">
                        <article class="overlay overlay-cover d-flex align-items-center justify-content-center">
                            <div class="text-center">
                                <h5 class="card-title">Primary text as title</h5>
                                <a href="#" class="btn btn-warning btn-sm"> View All </a>
                            </div>
                        </article>
                    </div>
                    <!-- card.// -->
                </div>

            </div>
        </div>
    </section>
    <!-- ========================= Blog .END// ========================= -->

    <!-- ========================= SECTION CONTENT ========================= -->
    <section class="section-content padding-y-sm bg">
        <div class="container">

            <header class="section-heading heading-line">
                <h4 class="title-section bg">FEATURED PRODUCTS</h4>
            </header>
            <div class="row">
                <div class="col-md-4">
                    <figure class="card card-product">
                        <div class="img-wrap"><img src="images/items/1.jpg"></div>
                        <figcaption class="info-wrap">
                            <h4 class="title">Another name of item</h4>
                            <p class="desc">Some small description goes here</p>
                            <div class="rating-wrap">
                                <ul class="rating-stars">
                                    <li style="width:80%" class="stars-active">
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                    </li>
                                </ul>
                                <div class="label-rating">132 reviews</div>
                                <div class="label-rating">154 orders </div>
                            </div>
                            <!-- rating-wrap.// -->
                        </figcaption>
                        <div class="bottom-wrap">
                            <a href="" class="btn btn-sm btn-primary float-right">Add To Cart</a>
                            <div class="price-wrap h5">
                                <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                            </div>
                            <!-- price-wrap.// -->
                        </div>
                        <!-- bottom-wrap.// -->
                    </figure>
                </div>
                <!-- col // -->
                <div class="col-md-4">
                    <figure class="card card-product">
                        <div class="img-wrap"><img src="images/items/2.jpg"> </div>
                        <figcaption class="info-wrap">
                            <h4 class="title">Good product</h4>
                            <p class="desc">Some small description goes here</p>
                            <div class="rating-wrap">
                                <ul class="rating-stars">
                                    <li style="width:80%" class="stars-active">
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                    </li>
                                </ul>
                                <div class="label-rating">132 reviews</div>
                                <div class="label-rating">154 orders </div>
                            </div>
                            <!-- rating-wrap.// -->
                        </figcaption>
                        <div class="bottom-wrap">
                            <a href="" class="btn btn-sm btn-primary float-right">Add To Cart</a>
                            <div class="price-wrap h5">
                                <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                            </div>
                            <!-- price-wrap.// -->
                        </div>
                        <!-- bottom-wrap.// -->
                    </figure>
                </div>
                <!-- col // -->
                <div class="col-md-4">
                    <figure class="card card-product">
                        <div class="img-wrap"><img src="images/items/3.jpg"></div>
                        <figcaption class="info-wrap">
                            <h4 class="title">Product name goes here</h4>
                            <p class="desc">Some small description goes here</p>
                            <div class="rating-wrap">
                                <ul class="rating-stars">
                                    <li style="width:80%" class="stars-active">
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                    </li>
                                </ul>
                                <div class="label-rating">132 reviews</div>
                                <div class="label-rating">154 orders </div>
                            </div>
                            <!-- rating-wrap.// -->
                        </figcaption>
                        <div class="bottom-wrap">
                            <a href="" class="btn btn-sm btn-primary float-right">Add To Cart</a>
                            <div class="price-wrap h5">
                                <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                            </div>
                            <!-- price-wrap.// -->
                        </div>
                        <!-- bottom-wrap.// -->
                    </figure>
                </div>
                <!-- col // -->
            </div>
            <!-- row.// -->

        </div>
        <!-- container .//  -->
    </section>

    <!-- ========================= SECTION ITEMS ========================= -->
    <section class="section-request bg padding-y-sm">
        <div class="container">
            <header class="section-heading heading-line">
                <h4 class="title-section bg text-uppercase">Recently Added</h4>
            </header>
            <div class="row">
                <div class="col-md-3">
                    <figure class="card card-product">
                        <div class="img-wrap"><img src="images/items/1.jpg"></div>
                        <figcaption class="info-wrap">
                            <h4 class="title">Another name of item</h4>
                            <p class="desc">Some small description goes here</p>
                            <div class="rating-wrap">
                                <ul class="rating-stars">
                                    <li style="width:80%" class="stars-active">
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                    </li>
                                </ul>
                                <div class="label-rating">132 reviews</div>
                                <div class="label-rating">154 orders </div>
                            </div>
                            <!-- rating-wrap.// -->
                        </figcaption>
                        <div class="bottom-wrap">
                            <a href="" class="btn btn-sm btn-primary float-right">Add To Cart</a>
                            <div class="price-wrap h5">
                                <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                            </div>
                            <!-- price-wrap.// -->
                        </div>
                        <!-- bottom-wrap.// -->
                    </figure>
                </div>
                <!-- col // -->
                <div class="col-md-3">
                    <figure class="card card-product">
                        <div class="img-wrap"><img src="images/items/2.jpg"> </div>
                        <figcaption class="info-wrap">
                            <h4 class="title">Good product</h4>
                            <p class="desc">Some small description goes here</p>
                            <div class="rating-wrap">
                                <ul class="rating-stars">
                                    <li style="width:80%" class="stars-active">
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                    </li>
                                </ul>
                                <div class="label-rating">132 reviews</div>
                                <div class="label-rating">154 orders </div>
                            </div>
                            <!-- rating-wrap.// -->
                        </figcaption>
                        <div class="bottom-wrap">
                            <a href="" class="btn btn-sm btn-primary float-right">Add To Cart</a>
                            <div class="price-wrap h5">
                                <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                            </div>
                            <!-- price-wrap.// -->
                        </div>
                        <!-- bottom-wrap.// -->
                    </figure>
                </div>
                <!-- col // -->
                <div class="col-md-3">
                    <figure class="card card-product">
                        <div class="img-wrap"><img src="images/items/3.jpg"></div>
                        <figcaption class="info-wrap">
                            <h4 class="title">Product name goes here</h4>
                            <p class="desc">Some small description goes here</p>
                            <div class="rating-wrap">
                                <ul class="rating-stars">
                                    <li style="width:80%" class="stars-active">
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                    </li>
                                </ul>
                                <div class="label-rating">132 reviews</div>
                                <div class="label-rating">154 orders </div>
                            </div>
                            <!-- rating-wrap.// -->
                        </figcaption>
                        <div class="bottom-wrap">
                            <a href="" class="btn btn-sm btn-primary float-right">Add To Cart</a>
                            <div class="price-wrap h5">
                                <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                            </div>
                            <!-- price-wrap.// -->
                        </div>
                        <!-- bottom-wrap.// -->
                    </figure>
                </div>
                <!-- col // -->
                <!-- col // -->
                <div class="col-md-3">
                    <figure class="card card-product">
                        <div class="img-wrap"><img src="images/items/3.jpg"></div>
                        <figcaption class="info-wrap">
                            <h4 class="title">Product name goes here</h4>
                            <p class="desc">Some small description goes here</p>
                            <div class="rating-wrap">
                                <ul class="rating-stars">
                                    <li style="width:80%" class="stars-active">
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                    </li>
                                </ul>
                                <div class="label-rating">132 reviews</div>
                                <div class="label-rating">154 orders </div>
                            </div>
                            <!-- rating-wrap.// -->
                        </figcaption>
                        <div class="bottom-wrap">
                            <a href="" class="btn btn-sm btn-primary float-right">Add To Cart</a>
                            <div class="price-wrap h5">
                                <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                            </div>
                            <!-- price-wrap.// -->
                        </div>
                        <!-- bottom-wrap.// -->
                    </figure>
                </div>
                <!-- col // -->
            </div>
            <!-- row.// -->
            <div class="row">
                <div class="col-md-3">
                    <figure class="card card-product">
                        <div class="img-wrap"><img src="images/items/1.jpg"></div>
                        <figcaption class="info-wrap">
                            <h4 class="title">Another name of item</h4>
                            <p class="desc">Some small description goes here</p>
                            <div class="rating-wrap">
                                <ul class="rating-stars">
                                    <li style="width:80%" class="stars-active">
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                    </li>
                                </ul>
                                <div class="label-rating">132 reviews</div>
                                <div class="label-rating">154 orders </div>
                            </div>
                            <!-- rating-wrap.// -->
                        </figcaption>
                        <div class="bottom-wrap">
                            <a href="" class="btn btn-sm btn-primary float-right">Add To Cart</a>
                            <div class="price-wrap h5">
                                <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                            </div>
                            <!-- price-wrap.// -->
                        </div>
                        <!-- bottom-wrap.// -->
                    </figure>
                </div>
                <!-- col // -->
                <div class="col-md-3">
                    <figure class="card card-product">
                        <div class="img-wrap"><img src="images/items/2.jpg"> </div>
                        <figcaption class="info-wrap">
                            <h4 class="title">Good product</h4>
                            <p class="desc">Some small description goes here</p>
                            <div class="rating-wrap">
                                <ul class="rating-stars">
                                    <li style="width:80%" class="stars-active">
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                    </li>
                                </ul>
                                <div class="label-rating">132 reviews</div>
                                <div class="label-rating">154 orders </div>
                            </div>
                            <!-- rating-wrap.// -->
                        </figcaption>
                        <div class="bottom-wrap">
                            <a href="" class="btn btn-sm btn-primary float-right">Add To Cart</a>
                            <div class="price-wrap h5">
                                <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                            </div>
                            <!-- price-wrap.// -->
                        </div>
                        <!-- bottom-wrap.// -->
                    </figure>
                </div>
                <!-- col // -->
                <div class="col-md-3">
                    <figure class="card card-product">
                        <div class="img-wrap"><img src="images/items/3.jpg"></div>
                        <figcaption class="info-wrap">
                            <h4 class="title">Product name goes here</h4>
                            <p class="desc">Some small description goes here</p>
                            <div class="rating-wrap">
                                <ul class="rating-stars">
                                    <li style="width:80%" class="stars-active">
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                    </li>
                                </ul>
                                <div class="label-rating">132 reviews</div>
                                <div class="label-rating">154 orders </div>
                            </div>
                            <!-- rating-wrap.// -->
                        </figcaption>
                        <div class="bottom-wrap">
                            <a href="" class="btn btn-sm btn-primary float-right">Add To Cart</a>
                            <div class="price-wrap h5">
                                <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                            </div>
                            <!-- price-wrap.// -->
                        </div>
                        <!-- bottom-wrap.// -->
                    </figure>
                </div>
                <!-- col // -->
                <!-- col // -->
                <div class="col-md-3">
                    <figure class="card card-product">
                        <div class="img-wrap"><img src="images/items/3.jpg"></div>
                        <figcaption class="info-wrap">
                            <h4 class="title">Product name goes here</h4>
                            <p class="desc">Some small description goes here</p>
                            <div class="rating-wrap">
                                <ul class="rating-stars">
                                    <li style="width:80%" class="stars-active">
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                    </li>
                                </ul>
                                <div class="label-rating">132 reviews</div>
                                <div class="label-rating">154 orders </div>
                            </div>
                            <!-- rating-wrap.// -->
                        </figcaption>
                        <div class="bottom-wrap">
                            <a href="" class="btn btn-sm btn-primary float-right">Add To Cart</a>
                            <div class="price-wrap h5">
                                <span class="price-new">$1280</span> <del class="price-old">$1980</del>
                            </div>
                            <!-- price-wrap.// -->
                        </div>
                        <!-- bottom-wrap.// -->
                    </figure>
                </div>
                <!-- col // -->
            </div>
            <!-- row.// -->

        </div>
        <!-- container // -->
    </section>

    <!-- ========================= Subscribe ========================= -->
    <section class="section-subscribe bg-primary padding-y-lg">
        <div class="container">

            <p class="pb-2 text-center white">Delivering the latest product trends and industry news straight to your inbox</p>

            <div class="row justify-content-md-center">
                <div class="col-lg-4 col-sm-6">
                    <form class="row-sm form-noborder">
                        <div class="col-8">
                            <input class="form-control" placeholder="Your Email" type="email">
                        </div>
                        <!-- col.// -->
                        <div class="col-4">
                            <button type="submit" class="btn btn-block btn-warning"> <i class="fa fa-envelope"></i> Subscribe </button>
                        </div>
                        <!-- col.// -->
                    </form>
                    <small class="form-text text-white-50">We’ll never share your email address with a third-party. </small>
                </div>
                <!-- col-md-6.// -->
            </div>

        </div>
        <!-- container // -->
    </section>
    <!-- ========================= Subscribe .END// ========================= -->
    <!-- ========================= FOOTER ========================= -->
    <footer class="section-footer bg-dark white">
        <div class="container">
            <section class="footer-top padding-top">
                <div class="row">
                    <aside class="col-sm-3 col-md-3 white">
                        <h5 class="title">Customer Services</h5>
                        <ul class="list-unstyled">
                            <li> <a href="#">Help center</a></li>
                            <li> <a href="#">Money refund</a></li>
                            <li> <a href="#">Terms and Policy</a></li>
                            <li> <a href="#">Open dispute</a></li>
                        </ul>
                    </aside>
                    <aside class="col-sm-3  col-md-3 white">
                        <h5 class="title">My Account</h5>
                        <ul class="list-unstyled">
                            <li> <a href="#"> User Login </a></li>
                            <li> <a href="#"> User register </a></li>
                            <li> <a href="#"> Account Setting </a></li>
                            <li> <a href="#"> My Orders </a></li>
                            <li> <a href="#"> My Wishlist </a></li>
                        </ul>
                    </aside>
                    <aside class="col-sm-3  col-md-3 white">
                        <h5 class="title">About</h5>
                        <ul class="list-unstyled">
                            <li> <a href="#"> Our history </a></li>
                            <li> <a href="#"> How to buy </a></li>
                            <li> <a href="#"> Delivery and payment </a></li>
                            <li> <a href="#"> Advertice </a></li>
                            <li> <a href="#"> Partnership </a></li>
                        </ul>
                    </aside>
                    <aside class="col-sm-3">
                        <article class="white">
                            <h5 class="title">Contacts</h5>
                            <p>
                                <strong>Phone: </strong> +123456789
                                <br>
                                <strong>Fax:</strong> +123456789
                            </p>

                            <div class="btn-group white">
                                <a class="btn btn-facebook" title="Facebook" target="_blank" href="#"><i class="fab fa-facebook-f  fa-fw"></i></a>
                                <a class="btn btn-instagram" title="Instagram" target="_blank" href="#"><i class="fab fa-instagram  fa-fw"></i></a>
                                <a class="btn btn-youtube" title="Youtube" target="_blank" href="#"><i class="fab fa-youtube  fa-fw"></i></a>
                                <a class="btn btn-twitter" title="Twitter" target="_blank" href="#"><i class="fab fa-twitter  fa-fw"></i></a>
                            </div>
                        </article>
                    </aside>
                </div>
                <!-- row.// -->
                <br>
            </section>
            <section class="footer-bottom row border-top-white">
                <div class="col-sm-6">
                    <p class="text-white-50"> Made with
                        <3 <br> by Vosidiy M.</p>
                </div>
                <div class="col-sm-6">
                    <p class="text-md-right text-white-50">
                        Copyright &copy
                        <br>
                        <a href="http://bootstrap-ecommerce.com" class="text-white-50">Bootstrap-ecommerce UI kit</a>
                    </p>
                </div>
            </section>
            <!-- //footer-top -->
        </div>
        <!-- //container -->
    </footer>
    <!-- ========================= FOOTER END // ========================= -->
        
    @if (session('response'))
        <script type="text/javascript">
            @if (isset(session('response')['showModal']) && session('response')['showModal'] != '')
                var showModal = '{!! session('response')['showModal'] !!}';
                $('#'+showModal).modal('show');
            @elseif(isset(session('response')['showModal']) && session('response')['showModal'] != '')
                var Registered = '{!! session('response')['Registered'] !!}';
                $('#'+Registered).modal('show');
            @else
                var messageAlert = '{!! session('response')['message'] !!}';
            @endif   
        </script>
    @endif
    @if (isset($errors) and count($errors) > 0)
        <script type="text/javascript">
            @if ($errors->any())
                messageAlert = '{{ json_encode($errors->all()) }}';
            @endif
        </script>
    @endif
    <script type="text/javascript" src="/js/plugins/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="/js/app.js?v=11"></script>
    <script type="text/javascript" src="/js/site/custom-script.js"></script>
</body>

</html>