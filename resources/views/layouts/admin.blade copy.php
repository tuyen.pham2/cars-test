<!DOCTYPE html>
<html lang="en_EN">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <meta name="robots" content="noindex">
    <title>Admin - Phuc Long Coffee & Tea House</title>
    <link rel="icon" href="/images/favicon/favicon.png" sizes="32x32">
    <!-- CORE CSS-->
    <link href="/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="/css/style.min.css" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link rel="stylesheet" type="text/css" href="/js/plugins/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="/css/plugins/select.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="/css/plugins/dropzone.min.css">
    <link href="/css/admin.css" type="text/css" rel="stylesheet" media="screen,projection">
    <script type="text/javascript" src="/js/plugins/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="/js/materialize.min.js"></script>
    @stack('styles')
    <style>
        .order-custom-css{position: relative;}
        .order-custom-css span{position: absolute;top:0;line-height: 25px;font-size: 12px;width:20px;height:20px;border-radius:100%;-webkit-border-radius:100%;-moz-border-radius:100%;background: red;
        text-align: center;
        line-height: 20px;
        color: #fff;
        box-shadow: 1px 1px 5px #ddd;
        -webkit-box-shadow: 1px 1px 5px #ddd;
        -moz-box-shadow: 1px 1px 5px #ddd;
        }
        .font-2{font-size: 14px;}
        .font-3{font-size: 12px;}
    </style>
</head>

<body ng-app="jollibee">

    <header id="header" class="page-topbar">
        <div class="navbar-fixed">
            <nav class="navbar-color">
                <div class="nav-wrapper">
                    <ul class="left">
                        <li><h1 class="logo-wrapper"><a href="{{ route('home') }}" class="brand-logo darken-1" target="_blank">Logo</a> <span class="logo-text">Logo</span></h1></li>
                    </ul>
                    <ul class="header-search-wrapper hide-on-med-and-down">
                        <li>@yield('view-page')</li>
                    </ul>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light translation-button" data-activates="translation-dropdown"></a></li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a></li>
                        {{-- <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown"><i class="mdi-social-notifications"><small class="notification-badge">5</small></i>                    </a>                        </li> --}}
                        {{-- <li><a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse"><i class="mdi-communication-chat"></i></a>                        </li> --}}
                    </ul>
      
                </div>
            </nav>
        </div>
    </header>

    <div id="main">
        <div class="wrapper">
            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation">
   
                    <li class="no-padding">
                        <ul class="collapsible collapsible-accordion">
                            <li class="bold">
                                <a class="collapsible-header waves-effect waves-cyan "><i class="mdi-action-account-child"></i> Users</a>
                                <div class="collapsible-body">
                                    <ul>
                                  
                                        <li class=""><a href="{{route('users.index')}}">List</a></li>
                                     
                                      
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="no-padding">
                        <ul class="collapsible collapsible-accordion">
                            <li class="bold">
                                <a class="collapsible-header waves-effect waves-cyan "><i class="mdi-action-account-child"></i> Cars</a>
                                <div class="collapsible-body">
                                    <ul>
                                        <li class=""><a href="{{route('cars.index')}}">List</a></li>
                                      
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="no-padding">
                        <ul class="collapsible collapsible-accordion">
                            <li class="bold">
                                <a class="collapsible-header waves-effect waves-cyan "><i class="mdi-action-account-child"></i> Tags</a>
                                <div class="collapsible-body">
                                    <ul>
                                        <li class=""><a href="{{route('tags.index')}}">List</a></li>
                                      
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
       
                <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only cyan"><i class="mdi-navigation-menu"></i></a>
            </aside>
            <!-- END LEFT SIDEBAR NAV-->
            <section id="content">
                <div class="container">@yield('content')</div>
            </section>
        </div>
    </div>
    <div class="modal" id="modal-upload">
        <div class="modal-content">
            <h4 class="modal-title">@lang('admin.title.upload image')</h4>
            <div id="uploader" class="dropzone"></div>
        </div>
    </div>
    <div class="modal" id="modal-upload2">
        <div class="modal-content">
            <h4 class="modal-title">@lang('admin.title.upload image')</h4>
            <div id="uploader2" class="dropzone"></div>
        </div>
    </div>
    <script type="text/javascript" src="/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script type="text/javascript" src="/js/plugins.min.js"></script>

    <script type="text/javascript" src="/js/plugins/data-tables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/js/plugins/data-tables/js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="/js/plugins/jquery-validation/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/js/plugins/jquery-validation/additional-methods.min.js"></script>
    <script type="text/javascript" src="/js/plugins/dropzone.js"></script>
    <script type="text/javascript" src="/js/plugins/tabs.js"></script>
    <script type="text/javascript">

        $postimagediv       = $('.postimagediv');
        $uploader           = $('#uploader');
        $modalupload        = $('#modal-upload');

        Dropzone.options.uploader = {
            url: "{{ route('resources.store') }}",
            paramName: 'upload',
            thumbnailWidth: 150,
            thumbnailHeight: 150,
            acceptedFiles: 'image/*',
            sending: function(file, xhr, formData) {
                formData.append('type', '{{ str_replace('Controller', '', explode('@', class_basename(Route::currentRouteAction()))[0]) }}');
                formData.append('resize', 1);
            },
            success: function(file, response) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $postimagediv.find('img').attr('src', e.target.result);
                }
                reader.readAsDataURL(file);
                // $postimagediv.find('img').attr('src', response.thumbnail);
                $postimagediv.find('input').val(response.id);
                $postimagediv.find('.set-image').addClass('hide');
                $postimagediv.find('.remove-image').removeClass('hide');
                $modalupload.closeModal();
                this.removeAllFiles();
            }
        };

        $postimagediv.on('click', '.remove-image', function(event) {
            event.preventDefault();
            if (!confirm('{{ trans('admin.message.confirm') }}')) return;
            var $input = $postimagediv.find('input');
            $.post("{{ route('resources.destroy','id') }}".replace('id', $input.val()), {_method: 'DELETE'}, function(data, textStatus, xhr) {
                if (data.status === 'success') {
                    $postimagediv.find('.set-image').removeClass('hide');
                    $postimagediv.find('.remove-image').addClass('hide');
                    $postimagediv.find('img').attr('src', '{{ App\Models\Resource::NO_SRC }}');
                    $input.val('');
                }
            });

        });
    </script>
    @stack('scripts')
    @include('partials.toast')
</body>

</html>
