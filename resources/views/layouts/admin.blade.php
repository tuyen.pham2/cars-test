<!DOCTYPE html>
<html lang="en">
  <head>
    <meta
      name="description"
      content="Car is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular."
    />
    <!-- Twitter meta-->
    <meta property="twitter:card" content="summary_large_image" />
    <meta property="twitter:site" content="@pratikborsadiya" />
    <meta property="twitter:creator" content="@pratikborsadiya" />
    <!-- Open Graph Meta-->
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="Car Admin" />
    <meta property="og:title" content="Car - Free Bootstrap 4 admin theme" />
    <meta
      property="og:url"
      content="http://pratikborsadiya.in/blog/vali-admin"
    />
    <meta property="og:image" content="../blog/vali-admin/hero-social.png" />
    <meta
      property="og:description"
      content="Car is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular."
    />
    <title>Car Admin - Free Bootstrap 4 Admin Template</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="/admin/css/main.css" />
    <!-- Font-icon css-->
    <link
      rel="stylesheet"
      type="text/css"
      href="/admin/css/font-awesome/4.7.0/css/font-awesome.min.css"
    />
</head>


<body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <header class="app-header">
      <a class="app-header__logo" href="index-2.html">Car</a>
      <!-- Sidebar toggle button--><a
        class="app-sidebar__toggle"
        href="#"
        data-toggle="sidebar"
        aria-label="Hide Sidebar"
      ></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
        <li class="app-search">
          <input class="app-search__input" type="search" placeholder="Search" />
          <button class="app-search__button">
            <i class="fa fa-search"></i>
          </button>
        </li>
        <!--Notification Menu-->
        <li class="dropdown">
          <a
            class="app-nav__item"
            href="#"
            data-toggle="dropdown"
            aria-label="Show notifications"
            ><i class="fa fa-bell-o fa-lg"></i
          ></a>
        </li>
        <!-- User Menu-->
        <li class="dropdown">
          <a
            class="app-nav__item"
            href="#"
            data-toggle="dropdown"
            aria-label="Open Profile Menu"
            ><i class="fa fa-user fa-lg"></i
          ></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <!-- <li>
              <a class="dropdown-item" href="page-user.html"
                ><i class="fa fa-cog fa-lg"></i> Settings</a
              >
            </li>
            <li>
              <a class="dropdown-item" href="page-user.html"
                ><i class="fa fa-user fa-lg"></i> Profile</a
              >
            </li> -->
            <li>
              <a class="dropdown-item" href="{{route('logout_admin')}}"
                ><i class="fa fa-sign-out fa-lg"></i> Logout</a
              >
            </li>
          </ul>
        </li>
      </ul>
    </header>
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
      <div class="app-sidebar__user">
        <div>
          <p class="app-sidebar__user-name">{{Auth::user()->name}}</p>
          <!-- <p class="app-sidebar__user-designation">Frontend Developer</p> -->
        </div>
      </div>
      <ul class="app-menu">
        <li>
          <a class="app-menu__item" href="index-2.html"
            ><i class="app-menu__icon fa fa-dashboard"></i
            ><span class="app-menu__label">Dashboard</span></a
          >
        </li>
        <li class="treeview">
          <a class="app-menu__item" href="{{route('users.index')}}" data-toggle="treeview">
              <i class="app-menu__icon fa fa-user"></i>
              <span class="app-menu__label">Users</span>
              <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
          <ul class="treeview-menu">
            <li>
              <a class="treeview-item" href="{{route('users.index')}}"
                ><i class="icon fa fa-circle-o"></i> List</a
              >
            </li>
          </ul>
        </li>
  
        <li class="treeview">
          <a class="app-menu__item" href="#" data-toggle="treeview"
            ><i class="app-menu__icon fa fa-edit"></i
            ><span class="app-menu__label">Car</span
            ><i class="treeview-indicator fa fa-angle-right"></i
          ></a>
          <ul class="treeview-menu">
            <li>
              <a class="treeview-item" href="{{route('cars.index')}}"
                ><i class="icon fa fa-circle-o"></i> List</a
              >
            </li>
            <li>
              <a class="treeview-item" href="{{route('cars.create')}}"
                ><i class="icon fa fa-circle-o"></i> Create new</a
              >
            </li>
          </ul>
        </li>
        <li class="treeview">
          <a class="app-menu__item" href="#" data-toggle="treeview"
            ><i class="app-menu__icon fa fa-th-list"></i
            ><span class="app-menu__label">Tags</span
            ><i class="treeview-indicator fa fa-angle-right"></i
          ></a>
          <ul class="treeview-menu">
            <li>
              <a class="treeview-item" href="{{route('tags.index')}}"
                ><i class="icon fa fa-circle-o"></i> List</a
              >
            </li>
            <li>
              <a class="treeview-item" href="{{route('tags.create')}}"
                ><i class="icon fa fa-circle-o"></i> Create new</a
              >
            </li>
          </ul>
        </li>
        <li class="treeview">
          <a class="app-menu__item" href="#" data-toggle="treeview"
            ><i class="app-menu__icon fa fa-th-list"></i
            ><span class="app-menu__label">Attributes</span
            ><i class="treeview-indicator fa fa-angle-right"></i
          ></a>
          <ul class="treeview-menu">
            <li>
              <a class="treeview-item" href="{{route('attributes.index')}}"
                ><i class="icon fa fa-circle-o"></i> List</a
              >
            </li>
            <li>
              <a class="treeview-item" href="{{route('attributes.create')}}"
                ><i class="icon fa fa-circle-o"></i> Create new</a
              >
            </li>
          </ul>
        </li>
        <li class="treeview">
          <a class="app-menu__item" href="#" data-toggle="treeview"
            ><i class="app-menu__icon fa fa-shopping-cart"></i
            ><span class="app-menu__label">Orders</span
            ><i class="treeview-indicator fa fa-angle-right"></i
          ></a>
          <ul class="treeview-menu">
            <li>
              <a class="treeview-item" href="{{route('orders.index')}}"
                ><i class="icon fa fa-circle-o"></i> List</a
              >
            </li>
            <li>
              <a class="treeview-item" href="{{route('order_status.index')}}"
                ><i class="icon fa fa-circle-o"></i> Order Status</a
              >
            </li>
     
          </ul>
        </li>
        <li class="treeview">
          <a class="app-menu__item {{ Route::currentRouteName() == 'coupons.index' ? 'active' : '' }}" href="{{route('coupons.index')}}"
            ><i class="app-menu__icon fa fa-shopping-cart"></i
            ><span class="app-menu__label">Coupon</span
            ><i class="treeview-indicator fa fa-angle-right"></i
          ></a>
        </li>
        <li class="treeview">
          <a class="app-menu__item" href="{{route('settings.index')}}"
            ><i class="app-menu__icon fa fa-shopping-cart"></i
            ><span class="app-menu__label">Settings</span
            ><i class="treeview-indicator fa fa-angle-right"></i
          ></a>
        </li>
      </ul>
    </aside>
    <main class="app-content">
    @yield('content')
    </main>
    <!-- Essential javascripts for application to work-->
    <script src="/admin/js/jquery-3.2.1.min.js"></script>
    <script src="/admin/js/popper.min.js"></script>
    <script src="/admin/js/bootstrap.min.js"></script>
    <script src="/admin/js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="/admin/js/plugins/pace.min.js"></script>
    @stack('scripts')

</body>

</html>
