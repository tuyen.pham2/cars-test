require('./bootstrap');
import { createApp } from 'vue';
let app = createApp({});
import AttributeValues from "./components/AttributeValues";
app.component('attribute-values', AttributeValues).mount('#app');

import Attributes from "./components/Attributes";
app.component('attributes-table', Attributes).mount('#Attributes');

import ProductPrice from './components/ProductPrice';
app.component('product-price', ProductPrice).mount('#ProductPrice');

import CartItem from './components/CartItems';
app.component('cart-item',CartItem).mount('#CartItem');

import Account from './components/Account';
app.component('profile-template', Account).mount('#AccountDetail');

import User from './components/User';
app.component('user-template', User).mount('#User');

import EmailSetting from './components/EmailSetting';
app.component('email-settings', EmailSetting).mount('#EmailTest');