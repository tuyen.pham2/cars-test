app.controller('CheckoutController', ['$scope', '$http', function($scope, $http) {
	$scope.swiper = {};
	$scope.cities = $scope.districts = $scope.products = [];
/*	$scope.city = cookieCity;
    $scope.district = cookieDistrict;*/

	$scope.selectedDisctrict = $scope.districts[0];
	$scope.selectedCity = $scope.cities[0];

	$scope.extras = extras;
	$scope.tab = 1;

	$http.get('/api/area').success(function(response) {
        $scope.cities = response.cities;
        $scope.districts = response.districts;

		/*$("select[name='city']").trigger("change");
		$("select[name='district']").trigger("change");*/
    });

	$http.get('/cart').success(function(response) {
		$scope.products = response.data;
	});

	$scope.getDistrictById = function (districtId) {
		angular.forEach($scope.districts, function(value, key) {
			if(value.id == districtId){
				return value;
			}
		});
		return null;
	};

	$scope.increase = function(product) {
		product.qty++;
		$http.post('/cart/rowId'.replace('rowId', product.rowId), {qty: product.qty, _method: 'PUT'});
	};

	$scope.decrease = function(product) {
		if (product.qty > 1) {
			product.qty--;
			$http.post('/cart/rowId'.replace('rowId', product.rowId), {qty: product.qty, _method: 'PUT'});
		}
	};

	$scope.delete = function(index, product) {
		$scope.products.splice(index, 1);
		$http.post('/cart/id'.replace('id', product.rowId), {_method: 'DELETE'});
		if (product.extra) {
			$('#extra-' + product.id).prop('checked', false);
			$('#extra-xs-' + product.id).prop('checked', false);
		}
	};

	$scope.calculateProductPrice = function(price, priceSize){
		var p = parseInt(priceSize);
		return (p > 0) ? p : price;
	}

	$scope.subtotal = function(product) {
		var total = 0;

/*		if(product.options.length > 0 && product.options.notes.size && parseInt(product.options.notes.size.price) > 0){
			total = parseInt(product.options.notes.size.price);
		}
		else{
			total = product.price;
		}

		angular.forEach(product.options.extra, function(e, k) {
			total += e.price * e.qty;
		});*/

		total = product.price * product.qty;
		return total;
	};

	$scope.total = function() {
		var count = 0, total = 0;
		angular.forEach($scope.products, function(value, key) {
			count += value.qty;
			total += $scope.subtotal(value);
		});
		return {
			count: count,
			money: total.toMoney()
		};
	};


	$scope.changeExtra = function($event, extra) {
		if ($event.target.checked) {
			$scope.products.push(extra);
			$scope.swiper.slideNext(true, 0);
		} else {
			var index = $scope.products.indexOf(extra);
			if (index !== -1) $scope.products.splice(index, 1);
		}
	};

	$scope.openTab = function(value, isClose) {
		$scope.$apply(function() { 
			$scope.setTab(value); 
		});
	};

	$scope.setTab = function(value) {
		//check if cart is empty or not
		if($scope.total().count> 0){
			$scope.tab = value;
		}

		if(value == 3){
			/*trigger to get data binding*/

		}
	};

	$scope.isTab = function(value) {
		return $scope.tab === value;
	};
}]);

//# sourceMappingURL=normal.js.map
