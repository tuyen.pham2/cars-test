$(document).ready(function() {
	var $orderBar = $('.mobile-order-purchase');

	if ($.cookie('customerFirstCart') === null) $.cookie('customerFirstCart', 'true');

	$('.btn-close').on('click', function(){
		$(this).parents('.mobile-order-min').remove();
	});

	addNameAtt('.dropdown-drink', 'dink');
	addNameAtt('.dropdown-icecream', 'icecream');
	addNameAtt('.dropdown-potato', 'potato');
	// Add name attribute for every dishes to make it different from the others
	function addNameAtt (container, name) {
		var wrapper = $('.order-dishes').toArray();
		for (var i = 0; i < wrapper.length; i++) {
			var value = name + '_' + i;
			$(wrapper[i]).find(container).find('input[type="checkbox"]').not('.left').attr('name', value);
		}
	}
	// Border around order when choosing extra attachment
	$('.mobile-dish').on('click', '*', function() {
		var items = $('.mobile-dish').toArray();
		for (var i = 0; i < items.length; i++) {
			$(items[i]).removeClass('edit-order');
		}
		$(this).parents('.mobile-dish').addClass('edit-order');
	});

	$('.dropdown dt').on('click', function() {
		$(this).parent('.side-group').toggleClass('enable');
	});

	function getCart() {
		$.get('/cart', function(response) {
			if($orderBar != undefined && $orderBar.children('.total') != undefined)
			{
				$orderBar.children('.total').text(parseInt(response.total).toMoney());
			}
		});
	}

	getCart();

	$('.dropdown-option .change-option').on('click', function(event) {
		var $col = $(this).closest('dl.dropdown');
		var $colName = $col.children('dt').children('.col-name');
		$colName.text($(this).parent('.radio').siblings('.combo-title').text());
	});

	$('input.change-size').change(function(event) {
		var value = parseInt($(this).parent('.checkbox').siblings('.text-center').text().replace(/\D/g, '')),
			$target = $(this).closest('.mobile-dish').children('.dish-info').children('.dish-price'),
			targetValue = parseInt($target.text().replace(/\D/g, ''));

		if ($(this).is(':checked')) targetValue += value;
		else targetValue -= value;
		$target.text(targetValue.toMoney());
	});

	$('input.change-col').change(function(event) {
		var $col = $(this).closest('.side-group'),
			$changeSize = $col.find('input.change-size');

		if ($changeSize.is(':checked'))	$changeSize.prop('checked', false).change();

		var value = parseInt($(this).parent('.checkbox').siblings('.text-center').text().replace(/\D/g, '')),
			$target = $(this).closest('.mobile-dish').children('.dish-info').children('.dish-price'),
			targetValue = parseInt($target.text().replace(/\D/g, ''));

		targetValue = ($(this).is(':checked')) ? targetValue + value : targetValue - value;
		$col.toggleClass('disabled enable').next('.side-group').toggleClass('disabled enable');
		$target.text(targetValue.toMoney());
	});

	$('.category-drink .mobile-dish .add-to-cart').click(function(event) {
		event.preventDefault();

		var id = $(this).data('id');
		var info = $(this).siblings( ".hidden.combo-info" );
		
		angular.element($productModal.scope().getProduct(id, info));
		angular.element($productModal[0]).scope().openTab(1,true);
	});

	// thuc-uong modal mobile
	$('.product-modal .mobile.add-to-cart.single').click(function(event) {
		var self = $(this),
			$item = self.closest('.product-item, #product-modal'),
			$target = $('.mobile-order-purchase'),
			ids = [];

		if ($item.hasClass('combo')) {

			$container = $item.find('.item-info__extra');
			$container.find('.list-inline .active').each(function(index, el) {
				var chosen_dish = $(el);
				ids.push(chosen_dish.data('exid'));
			});
		}

		$.ajax({
			url: '/cart',
			type: 'POST',
			dataType: 'json',
			beforeSend: function() {
				self.attr('disabled', true);
			},
				data: {
					// id: self.data('id'),
					id: self.attr('data-id'),
					ids: ids,
					size:  $('input[name=productSize]').val(),
					extra: $('input[name=productExtraSelected]').val(), //main
					extraPrice :  $('input[name=productExtraPrice]').val(),
					extras: $('input[name=productExtras]').val(),
					qty : $('input[name=productQty]').val(),
					orderNotes:  $('input[name=orderNotes]').val(),
					productSizeId : $('input[name=productSizeId]').val()

				}
		})
		.done(function(data) {
			if (data.status === 'success') {
				// getCart();
				angular.element($orderBar[0]).scope().getCart();
			}
		})
		.always(function() {
			self.attr('disabled', false);
		});


		$('#product-modal').modal('hide');
		// var $imgtodrag = $container.children('.dish-img').children('.img-responsive');
		var $imgtodrag = $item.find('.item-img');
		flyToCart($imgtodrag, $target);
	});


	// san pham mobile
	$('.add-to-cart.cart-product.mobile').click(function(event) {
	    event.preventDefault();
	    var self = $(this),
	        $item = self.closest('.product-top'),
	        $target = $('.mobile-order-purchase');


	    

	    $.ajax({
	            url: '/cart',
	            type: 'POST',
	            dataType: 'json',
	            beforeSend: function() {
	                self.attr('disabled', true);
	            },
	            data: {
	                id: self.attr('data-id'),
	                qty : $('input[name=productQty]').val()
	            }
	        })
	        .done(function(data) {
	            if (data.status === 'success') {
	                // angular.element($miniCart[0]).scope().getCart();
	                angular.element($orderBar[0]).scope().getCart();
	                
	            }
	        })
	        .always(function() {
	            self.attr('disabled', false);
	        });
	    var $imgtodrag = $item.find('.item-img .item img');
	    flyToCart($imgtodrag, $target);

	});
});

//# sourceMappingURL=mobile.js.map
