var mapCanvas, bounds_GMapFP,
    infowindow      = [],
    markerImage     = '/images/icons/icon_mark.svg',
    num_open        = 0,
    infowindowLevel = 0,
    marker          = [];

/*pl*/
var curentPos;
var directionsService;
var directionsDisplay;
/*PL*/
var LatLngList = [];


function attachinfowindow(marker, place, i) {
    infowindow[i] = new google.maps.InfoWindow({
        // content: '<div class="store-image"><img width="220" src="' + place.image + '"></div>',
        maxWidth: 197,
        disableAutoPan: 0
    });
    /* google.maps.event.addListener(marker, 'mouseover', function(e) {
     infowindow[num_open].close(mapCanvas, marker);
     infowindow[i].setZIndex(++infowindowLevel);
     infowindow[i].open(mapCanvas, marker);
     num_open = i;
     mapCanvas.setCenter(marker.getPosition());
     });*/
}


function initMap() {

    callbackGeo();

    mapCanvas = new google.maps.Map(document.getElementById('map-canvas'), {
        zoom: 13,
        panControl: false,
        scaleControl: false,
        scrollwheel: false
    });

    bounds_GMapFP = new google.maps.LatLngBounds();


    directionsService = new google.maps.DirectionsService;
    directionsDisplay = new google.maps.DirectionsRenderer({
        suppressMarkers: true,
        polylineOptions: {
            strokeColor: "#0C713D"
        }
    });
    directionsDisplay.setMap(mapCanvas);


    for (var i = 0; i < locations.length; i++) {
        var place = locations[i],
            maLatLng = new google.maps.LatLng(Number(place.lat), Number(place.lng));

        LatLngList.push(new google.maps.LatLng(Number(place.lat), Number(place.lng)));


        bounds_GMapFP.extend(maLatLng);


        marker[i] = new google.maps.Marker({
            map: mapCanvas,
            position: maLatLng,
            //title: place[3],
            icon: markerImage
        });

        attachinfowindow(marker[i], place, i);


    }
    mapCanvas.setCenter(bounds_GMapFP.getCenter());


    // if (navigator.geolocation) {
    //     navigator.geolocation.getCurrentPosition(function(position) {
    //         curentPos = {
    //             lat: position.coords.latitude,
    //             lng: position.coords.longitude
    //         };
    //         var marker = new google.maps.Marker({
    //             position: curentPos,
    //             map: map,
    //             icon: '/images/icons/icon_current.svg'
    //         })
    //         // To add the marker to the map, call setMap();
    //         marker.setMap(mapCanvas);
    //         LatLngList.push(new google.maps.LatLng(curentPos.lat, curentPos.lng));
    //     });
    // }
    // else {
    // }
    /**/
    LatLngList.forEach(function(latLng) {
        bounds_GMapFP.extend(latLng);
    });
}


function callbackGeo(){

    //callback
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            // jQuery('.map_info').hide();
            jQuery('.store-entry li button').removeClass('disabled')

            curentPos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            var marker = new google.maps.Marker({
                position: curentPos,
                map: map,
                icon: './images/icons/icon_current.svg'
            })
            // To add the marker to the map, call setMap();
            marker.setMap(mapCanvas);
            LatLngList.push(new google.maps.LatLng(curentPos.lat, curentPos.lng));
        });
    }
}

function closeAllInfoWindows(infoWindows) {
  for (var i=0;i<infoWindows.length;i++) {
     infoWindows[i].close();
  }
}

app.controller('StoreController', ['$scope', '$http', function($scope, $http) {
    $scope.stores = locations;
    $scope.currentStore = locations[0];
    $scope.cities = [];
    $scope.cities2 = [];
    $scope.districts = [];
    $scope.city = customerCity;
    $scope.district = null;
    var infoWindows = [];
    $scope.storeClick = function(index, store) {
        /*closeAllInfoWindows(infoWindows);
        google.maps.event.trigger(marker[index], 'mouseover');
        $scope.currentStore = store;
        //window.location.hash = store.slug;
        //text window info
        var phoneaf = store.phone.replace(/ /g,'');
        var contentString = '<div id="content">'+
            '<div id="siteNotice" style="float:left;width:35%;padding-right:15px;text-align:center;padding-top:15px"><img width="75px" height="auto" src="'+ store.image +'"></div>'+
            '<div id="bodyContent" style="float:left;width:65%">'+
            '<h3 id="firstHeading" class="firstHeading" style="margin-top:15px;font-size:14px;font-weight:bold">'+store.title+'</h3>'+
            '<p style="margin-bottom:10px"><b>Địa chỉ:</b> '+store.address+'</p>'+
            '<p>Số điện thoại: <b style="color:red"><a href="tel:'+phoneaf+'">'+store.phone+'</a></b></p>'+
            '</div>'+
            '</div>';
        var infowindow = new google.maps.InfoWindow({
          content: contentString,
          maxWidth: 300
        });
        //click hiện thong tin
        infowindow.open(mapCanvas,marker[index]);
        infoWindows.push(infowindow); 
        //end test


        /!*direction*!/
        directionsService.route({
            origin: curentPos,
            destination: {
                lat: Number(store.lat),
                lng: Number(store.lng)
            },
            travelMode: 'DRIVING'
        }, function(response, status) {
            if (status === 'OK') {
                directionsDisplay.setDirections(response);
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });*/
        console.log(store.url);
        window.open(store.url,'_blank');
    };

    $scope.submit = function() {
        if ($scope.district === null) return;
        $http.get('/api/store/' + $scope.district).success(function(response) {
            if (response.length === 0) {
                swal('', messageNoStore);
                return;
            }
            locations = $scope.stores = response;
            $scope.currentStore = $scope.stores[0];
            initMap();


        });
    };

    $scope.onselect = function(){

        /*=edit*/
        if ($scope.city === null) return;
        $http.get('/api/area1/' + $scope.city).success(function(response) {
            if (response.length === 0) {
                swal('', messageNoStore);
                return;
            }
            locations = $scope.stores = response;
            $scope.currentStore = $scope.stores[0];
            initMap();
        });

        setTimeout(function () {
            $("#slb_district").selectpicker('refresh');
        });

    };

    $http.get('/api/area/0').success(function(response) {
        $scope.cities = response.cities;
        $scope.cities2 = response.cities2;
        $scope.districts = response.districts;
    });
}]);


//# sourceMappingURL=store.js.map

//# sourceMappingURL=store.js.map
