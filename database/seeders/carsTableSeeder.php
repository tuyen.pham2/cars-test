<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class carsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = (new \Faker\Factory())::create();
        $faker->addProvider(new \Faker\Provider\Fakecar($faker));
        for ($i = 0; $i < 30; $i++) {
            $car = \App\Models\Car::insert([
                'name' => $faker->name,
                'slug' => $faker->unique()->slug,
                'description' => '',
                'model' => $faker->vehicleModel,
                'registration' => $faker->vehicleRegistration,
                'engine_size' => $faker->vin,
                'make' => $faker->vehicleBrand,
                'price' => rand(150, 1),
                'image' => '',
                'active' => 1,
            ]);
            $arr = $car->toArray();
            $params = [
                'index' => 'car',
                'id' => $arr['id'],
                'body' => $arr,
            ];
            $client = es_connect();
            $response = $client->index($params);
        }
    }
}
