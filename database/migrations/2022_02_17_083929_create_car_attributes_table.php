<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_attributes', function (Blueprint $table) {
            $table->bigIncrements('id')->length(20);
            $table->integer('car_id')->length(11)->unsigned();
            $table->bigInteger('attribute_id')->length(20)->unsigned();

            $table->foreign('car_id')->references('id')->on('cars')->onDelete('cascade');
            $table->foreign('attribute_id')->references('id')->on('attributes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_attributes');
    }
}
