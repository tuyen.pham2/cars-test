<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_attributes', function (Blueprint $table) {
            $table->bigIncrements('id')->length(20);
            $table->bigInteger('group_id')->length(20)->unsigned();
            $table->bigInteger('attribute_id')->length(20)->unsigned();
            $table->bigInteger('attribute_value_id')->length(20)->unsigned();

            $table->foreign('group_id')->references('id')->on('car_groups')->onDelete('cascade');
            $table->foreign('attribute_id')->references('id')->on('attributes')->onDelete('cascade');
            $table->foreign('attribute_value_id')->references('id')->on('attribute_values')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_attributes');
    }
}
