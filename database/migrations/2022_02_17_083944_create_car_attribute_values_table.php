<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarAttributeValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_attribute_values', function (Blueprint $table) {
            $table->bigIncrements('id')->length(20);

            $table->bigInteger('car_attribute_id')->length(20)->unsigned();
            $table->bigInteger('attribute_value_id')->length(20)->unsigned();

            $table->foreign('car_attribute_id')->references('id')->on('car_attributes')->onDelete('cascade');
            $table->foreign('attribute_value_id')->references('id')->on('attribute_values')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_attribute_values');
    }
}
